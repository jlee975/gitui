TEMPLATE = subdirs

SUBDIRS += \
    qt \
    repo \
    test \
    jlee \
    qtility

repo.depends = jlee
qtility.depends = jlee
qt.depends = repo qtility
test.depends = repo
