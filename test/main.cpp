/* Program is largely used for timing the performance of loading large repositories
 */
#include <iostream>
#include <chrono>

#include <unistd.h>

#include <git2.h>

#include "repository.h"

int main()
{
	git_libgit2_init();

	// Open up repo and create topological log
	if ( char* p = ::get_current_dir_name() )
	{
		auto                            t0 = std::chrono::system_clock::now();
		Repository                      repo(p);
		auto                            t1    = std::chrono::system_clock::now();
		std::chrono::duration< double > secs1 = t1 - t0;
		std::cout << "Loading repo took " << secs1.count() << "s" << std::endl;
		const auto                      tmp   = repo.topological_order();
		auto                            t2    = std::chrono::system_clock::now();
		std::chrono::duration< double > secs2 = t2 - t1;
		std::cout << "Topological ordering took " << secs2.count() << "s" << std::endl;

		::free(p);
	}

	git_libgit2_shutdown();

	return 0;
}
