include(../vars.pri)

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt c++11

SOURCES += \
        main.cpp

unix|win32: LIBS += -lgit2

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../jlee/release/ -ljlee
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../jlee/debug/ -ljlee
else:unix: LIBS += -L$$OUT_PWD/../jlee/ -ljlee

INCLUDEPATH += $$PWD/../jlee
DEPENDPATH += $$PWD/../jlee

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../jlee/release/libjlee.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../jlee/debug/libjlee.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../jlee/release/jlee.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../jlee/debug/jlee.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../jlee/libjlee.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../repo/release/ -lrepo
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../repo/debug/ -lrepo
else:unix: LIBS += -L$$OUT_PWD/../repo/ -lrepo

INCLUDEPATH += $$PWD/../repo
DEPENDPATH += $$PWD/../repo

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../repo/release/librepo.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../repo/debug/librepo.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../repo/release/repo.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../repo/debug/repo.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../repo/librepo.a
