#ifndef REPOTYPES_H
#define REPOTYPES_H

#include <string>
#include <unordered_set>
#include <vector>
#include <array>
#include <ctime>
#include "bits/hex.h"
#include "util/index_type.h"
#include "hash/hash.h"

struct branch_id_tag {};

using branch_id = index_type< std::size_t, branch_id_tag >;

const branch_id INVALID_BRANCH(-1);

struct remote_id_tag {};

using remote_id = index_type< std::size_t, remote_id_tag >;

const remote_id INVALID_REMOTE(-1);

struct tag_id_tag {};

using tag_id = index_type< std::size_t, tag_id_tag >;

const tag_id INVALID_TAG(-1);

struct stash_id_tag {};

using stash_id = index_type< std::size_t, stash_id_tag >;

const stash_id INVALID_STASH(-1);

struct status_id_tag {};

using status_id = index_type< std::size_t, status_id_tag >;

const status_id INVALID_STATUS(-1);

struct commit_id_tag {};

using commit_id = index_type< std::size_t, commit_id_tag >;

const commit_id INVALID_COMMIT(-1);

using hash_type = jlee::hash< 160 >;

struct branch_type
{
	std::string name;
	hash_type hash;
};

struct remote_type
{
	std::string name;

	std::string url;

	// (branch name, hash)
	/// @todo idmap< branch_type, branch_id>
	std::vector< branch_type > branches;

	std::string full_name(std::size_t i) const
	{
		return name + "/" + branches.at(i).name;
	}

};

struct contributor_id_tag {};

using contributor_id = index_type< std::size_t, contributor_id_tag >;

const contributor_id INVALID_CONTRIBUTOR(-1);

struct contributor_type
{
	std::string name;
	std::string email;
	std::size_t num_authorships;
	std::size_t num_commits;
	commit_id first_activity;
	commit_id last_activity;
};

/**
 * @todo Performance. String view for committers and emails, store originals in repo object
 * @todo Performance. Custom type for hash. Other types should store commit id rather than hash.
 * @todo Performance. I think both time_abbr and time can be derived from actual_time
 */
struct commit_type
{
	hash_type hashxxx; // string version of oid
	std::string summary;
	std::string message;
	contributor_id committer;
	contributor_id author;
	std::time_t actual_time;
};

struct line_type
{
	char origin;
	int old_lineno;
	int new_lineno;
	std::string content;
};

struct hunk_type
{
	std::string header;
	int old_start;
	int old_lines;
	int new_start;
	int new_lines;
	std::vector< line_type > lines;
};

struct delta_type
{
	bool added;
	bool removed;
	std::string old_name;
	std::string new_name;
	std::vector< hunk_type > hunks;
};

struct diff_type
{
	std::vector< delta_type > deltas;
};

struct tag_type
{
	std::string name;
	hash_type hash;
	std::string user;
	std::string email;
	std::string message;
};

struct stash_type
{
	std::size_t idx;
	std::string message;
	hash_type hash;
};

struct status_type
{
	enum {CURRENT = 1, INDEX_NEW = 2, INDEX_MODIFIED = 4, INDEX_DELETED = 8,
		  INDEX_RENAMED = 16, INDEX_TYPECHANGE = 32, WT_NEW = 64,
		  WT_MODIFIED = 128, WT_DELETED = 256, WT_TYPECHANGE = 512,
		  WT_RENAMED = 1024, WT_UNREADABLE = 2048, IGNORED = 4096,
		  CONFLICTED = 8192, INDEX_ANY = 62};

	std::string path;
	unsigned status;
};

struct reduce_type
{
	commit_id orig;
	std::vector< std::size_t > parents;
};

class reduction
{
public:
	void reserve(std::size_t n)
	{
		nodes.reserve(n);
	}

	void push_back(commit_id i)
	{
		nodes.push_back(reduce_type{ i });
	}

	auto size() const
	{
		return nodes.size();
	}

	reduce_type& operator[](std::size_t i)
	{
		return nodes[i];
	}

	const reduce_type& operator[](std::size_t i) const
	{
		return nodes[i];
	}

	void add_parent(
		std::size_t i,
		std::size_t j
	)
	{
		nodes[i].parents.push_back(j);
	}

private:
	std::vector< reduce_type > nodes;
};

struct blame_type
{
	std::size_t lineno;
	std::string commit;
	contributor_id author;
	std::string line;
};

struct commit_to_type
{
	// cross references

	std::unordered_set< branch_id > branches;

	std::unordered_set< tag_id > tags;

	/// @todo Use set-like
	std::vector< std::pair< remote_id, std::size_t > > remotes;
};

struct reflog_type
{
	hash_type curr;
	hash_type prev;
	std::string name;
	std::string email;
	std::string message;
};

#endif // REPOTYPES_H
