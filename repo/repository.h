#ifndef REPOSITORY_H
#define REPOSITORY_H

#include <vector>
#include <string>
#include <memory>
#include <map>

#include "repotypes.h"

struct RepositoryImpl;

/** @todo Replace pimpl with interface if other types of versioning systems will be supported
 */
class Repository
{
public:
	explicit Repository(const std::string&);
	~Repository();

	void refresh();

	bool is_valid() const;
	std::string path() const;
	const contributor_type& contributor(contributor_id) const;
	std::size_t number_of_contributors() const;

	const status_type& status(status_id) const;
	std::vector< status_id > status_ids() const;
	bool add_to_index(const std::string&);
	bool remove_from_index(const std::vector< status_id >&);
	bool is_index_empty() const;
	bool commit_index(const std::string&);

	const hash_type& head() const;
	const commit_type& commit(commit_id) const;
	std::size_t number_of_commits() const;
	commit_id hash_to_commit(const hash_type&) const;
	commit_id                parent(commit_id, std::size_t) const;
	std::vector< commit_id > parents(commit_id) const;
	std::vector< reflog_type > reflog(const std::string&) const;

	std::vector< branch_id > branch_ids() const;
	const std::unordered_set< branch_id >& branch_ids(commit_id) const;
	const branch_type&                     branch(branch_id) const;
	commit_id                              branch_to_commit(branch_id) const;
	branch_id                              create_branch_from_branch(branch_id, const std::string&);
	branch_id                              create_branch_from_commit(commit_id, const std::string&);
	bool                                   rename_branch(branch_id, const std::string&);
	bool                                   remove_branch(branch_id);
	branch_id find_branch(const std::string&) const;
	bool                       rebase(const hash_type&, branch_id, const std::optional< hash_type >&, bool);
	bool                       move_branch_to_commit(branch_id, commit_id);
	bool                       checkout(branch_id) const;
	std::vector< reflog_type > reflog(branch_id) const;
	std::string                reference(branch_id) const;

	std::vector< remote_id > remote_ids() const;
	const remote_type& remote(remote_id) const;
	bool               rename_remote(remote_id, const std::string&);
	bool               set_url_remote(remote_id, const std::string&);
	remote_id add_remote(const std::string&, const std::string&);
	commit_id remote_branch_to_commit(remote_id, std::size_t) const;
	remote_id find_remote(const std::string&) const;
	std::size_t find_remote_branch(remote_id, const std::string&) const;
	bool        remove_remote_branch(remote_id, std::size_t);
	bool        fetch_remote(remote_id);

	std::vector< tag_id > tag_ids() const;
	const std::unordered_set< tag_id >& tag_ids(commit_id) const;
	const tag_type&                     tag(tag_id) const;
	bool                                remove_tag(tag_id);
	commit_id                           tag_to_commit(tag_id) const;
	bool                                create_tag_from_commit(commit_id, const std::string&);
	bool                                create_tag_from_commit(commit_id, const std::string&, const std::string&);
	bool                                checkout(tag_id) const;
	tag_id find_tag(const std::string&) const;
	bool                       move_tag_to_commit(tag_id, commit_id);
	std::vector< reflog_type > reflog(tag_id) const;
	std::string                reference(tag_id) const;

	const stash_type& stash(stash_id) const;
	std::vector< stash_id > stash_ids() const;
	bool push_stash();
	bool pop_stash(stash_id);
	bool drop_stash(stash_id);
    commit_id stash_to_commit(stash_id) const;

	reduction topological_order() const;
	reduction reduce(commit_id, commit_id) const;
	reduction reduce(std::vector< std::string >&) const;

	diff_type diff_brief(commit_id, commit_id) const;
	diff_type diff_full(commit_id, commit_id) const;
	diff_type diff_with_index(commit_id) const;
	diff_type diff_with_workdir(commit_id) const;

	std::vector< blame_type > blame(const std::string&) const;

	commit_id head_commit() const;

	const std::map< hash_type, commit_to_type >::value_type& relations(const hash_type&) const;
private:
	Repository(const Repository&)            = delete;
	Repository& operator=(const Repository&) = delete;

	std::unique_ptr< RepositoryImpl > pimpl;
};

#endif // REPOSITORY_H
