#include "repository.h"

#include "repositoryimpl.h"

Repository::Repository(const std::string& path_)
	: pimpl(std::make_unique< RepositoryImpl >(path_) )
{
}

void Repository::refresh()
{
	pimpl->refresh();
}

bool Repository::is_valid() const
{
	return pimpl->is_valid();
}

Repository::~Repository() = default;

std::string Repository::path() const
{
	return pimpl->path;
}

const branch_type& Repository::branch(branch_id i) const
{
	return pimpl->branches[i];
}

std::vector< branch_id > Repository::branch_ids() const
{
	std::vector< branch_id > v;

	for ( const auto b : pimpl->branches )
	{
		v.push_back(b.first);
	}

	return v;
}

const std::unordered_set< branch_id >& Repository::branch_ids(commit_id c) const
{
	auto it = pimpl->xrefs.find(pimpl->commits[c.get()].hashxxx);

	if ( it != pimpl->xrefs.end() )
	{
		return it->second.branches;
	}

	static const std::unordered_set< branch_id > fallback;

	return fallback;
}

const remote_type& Repository::remote(remote_id i) const
{
	return pimpl->remotes[i];
}

std::vector< remote_id > Repository::remote_ids() const
{
	std::vector< remote_id > v;

	for ( const auto b : pimpl->remotes )
	{
		v.push_back(b.first);
	}

	return v;
}

const tag_type& Repository::tag(tag_id i) const
{
	return pimpl->tags[i];
}

std::vector< tag_id > Repository::tag_ids() const
{
	std::vector< tag_id > v;

	for ( const auto b : pimpl->tags )
	{
		v.push_back(b.first);
	}

	return v;
}

const std::unordered_set< tag_id >& Repository::tag_ids(commit_id c) const
{
	auto it = pimpl->xrefs.find(pimpl->commits[c.get()].hashxxx);

	if ( it != pimpl->xrefs.end() )
	{
		return it->second.tags;
	}

	static const std::unordered_set< tag_id > fallback;

	return fallback;

}

const commit_type& Repository::commit(commit_id i) const
{
	const auto it = pimpl->commits.find(i.get() );

	if ( it != pimpl->commits.end() )
	{
		return it->second;
	}

	static const commit_type dummy = {};
	return dummy;
}

std::size_t Repository::number_of_commits() const
{
	return pimpl->commits.order();
}

commit_id Repository::parent(
	commit_id   c,
	std::size_t i
) const
{
	return commit_id(pimpl->commits.parents(c.get() )[i]);
}

std::vector< commit_id > Repository::parents(commit_id i) const
{
	/// @todo Unhappy about the copy
	std::vector< commit_id > res;

	for ( const auto p : pimpl->commits.parents(i.get() ) )
	{
		res.emplace_back(p);
	}

	return res;
}

std::vector< reflog_type > Repository::reflog(const std::string& s) const
{
	return pimpl->reflog(s);
}

const stash_type& Repository::stash(stash_id i) const
{
	return pimpl->stashes[i];
}

std::vector< stash_id > Repository::stash_ids() const
{
	std::vector< stash_id > v;

	for ( const auto& p : pimpl->stashes )
	{
		v.push_back(p.first);
	}

	return v;
}

const status_type& Repository::status(status_id i) const
{
	return pimpl->status[i];
}

std::vector< status_id > Repository::status_ids() const
{
	std::vector< status_id > v;

	for ( const auto& p : pimpl->status )
	{
		v.push_back(p.first);
	}

	return v;
}

diff_type Repository::diff_brief(
	commit_id i,
	commit_id j
) const
{
	return pimpl->diff_commits(i, j, true);
}

diff_type Repository::diff_full(
	commit_id i,
	commit_id j
) const
{
	return pimpl->diff_commits(i, j, false);
}

diff_type Repository::diff_with_index(commit_id i) const
{
	return pimpl->diff_with_index(i);
}

diff_type Repository::diff_with_workdir(commit_id i) const
{
	return pimpl->diff_with_workdir(i);
}

commit_id Repository::hash_to_commit(const hash_type& s) const
{
	return pimpl->hash_to_commit(s);
}

bool Repository::rename_branch(
	branch_id          i,
	const std::string& s
)
{
	return pimpl->rename_branch(i, s);
}

bool Repository::remove_branch(branch_id i)
{
	return pimpl->remove_branch(i);
}

bool Repository::rename_remote(
	remote_id          i,
	const std::string& s
)
{
	return pimpl->rename_remote(i, s);
}

bool Repository::fetch_remote(remote_id r)
{
	return pimpl->fetch_remote(r);
}

remote_id Repository::add_remote(
	const std::string& url,
	const std::string& name
)
{
	return pimpl->add_remote(url, name);
}

bool Repository::set_url_remote(
	remote_id          i,
	const std::string& s
)
{
	return pimpl->set_url_remote(i, s);
}

bool Repository::rebase(
	const hash_type&                  onto,
	branch_id                         tip,
	const std::optional< hash_type >& from,
	bool                              recursive
)
{
	return pimpl->rebase(onto, tip, from, recursive);
}

branch_id Repository::find_branch(const std::string& s) const
{
	return pimpl->find_branch(s);
}

commit_id Repository::branch_to_commit(branch_id b) const
{
	return pimpl->branch_to_commit(b);
}

branch_id Repository::create_branch_from_branch(
	branch_id          b,
	const std::string& s
)
{
	return pimpl->create_branch_from_branch(b, s);
}

commit_id Repository::remote_branch_to_commit(
	remote_id   r,
	std::size_t b
) const
{
	return pimpl->remote_branch_to_commit(r, b);
}

branch_id Repository::create_branch_from_commit(
	commit_id          i,
	const std::string& s
)
{
	return pimpl->create_branch_from_commit(i, s);
}

bool Repository::move_branch_to_commit(
	branch_id s,
	commit_id i
)
{
	return pimpl->move_branch_to_commit(s, i);
}

bool Repository::move_tag_to_commit(
	tag_id    s,
	commit_id i
)
{
	return pimpl->move_tag_to_commit(s, i);
}

bool Repository::create_tag_from_commit(
	commit_id          i,
	const std::string& s
)
{
	return pimpl->create_tag_from_commit(i, s);
}

bool Repository::create_tag_from_commit(
	commit_id          i,
	const std::string& s,
	const std::string& m
)
{
	return pimpl->create_tag_from_commit(i, s, m);
}

bool Repository::remove_tag(tag_id i)
{
	return pimpl->remove_tag(i);
}

tag_id Repository::find_tag(const std::string& s) const
{
	return pimpl->find_tag(s);
}

commit_id Repository::tag_to_commit(tag_id i) const
{
	return pimpl->tag_to_commit(i);
}

bool Repository::push_stash()
{
	return pimpl->push_stash();
}

bool Repository::pop_stash(stash_id i)
{
	return pimpl->pop_stash(i);
}

bool Repository::drop_stash(stash_id i)
{
    return pimpl->drop_stash(i);
}

commit_id Repository::stash_to_commit(stash_id i) const
{
    return pimpl->stash_to_commit(i);
}

bool Repository::add_to_index(const std::string& s)
{
	return pimpl->add_to_index(s);
}

bool Repository::remove_from_index(const std::vector< status_id >& s)
{
	return pimpl->remove_from_index(s);
}

bool Repository::is_index_empty() const
{
	for ( const auto& si : pimpl->status )
	{
		if ( ( si.second.status & status_type::INDEX_ANY ) != 0 )
		{
			return false;
		}
	}

	return true;
}

bool Repository::commit_index(const std::string& s)
{
	return pimpl->commit_index(s);
}

reduction Repository::topological_order() const
{
	return pimpl->topological_order();
}

reduction Repository::reduce(
	commit_id from,
	commit_id to
) const
{
	return pimpl->reduce(from, to);
}

/// @bug Not implemented
reduction Repository::reduce(std::vector< std::string >& v) const
{
	return pimpl->reduce(v);
}

const contributor_type& Repository::contributor(contributor_id i) const
{
	return pimpl->contributor(i);
}

std::size_t Repository::number_of_contributors() const
{
	return pimpl->number_of_contributors();
}

std::vector< blame_type > Repository::blame(const std::string& s) const
{
	return pimpl->blame(s);
}

const hash_type& Repository::head() const
{
	return pimpl->head;
}

remote_id Repository::find_remote(const std::string& s) const
{
	return pimpl->find_remote(s);
}

std::size_t Repository::find_remote_branch(
	remote_id          r,
	const std::string& s
) const
{
	return pimpl->find_remote_branch(r, s);
}

bool Repository::remove_remote_branch(
	remote_id   r,
	std::size_t i
)
{
	return pimpl->remove_remote_branch(r, i);
}

commit_id Repository::head_commit() const
{
	return pimpl->hash_to_commit(pimpl->head);
}

bool Repository::checkout(branch_id b) const
{
	return pimpl->checkout(b);
}

bool Repository::checkout(tag_id b) const
{
	return pimpl->checkout(b);
}

std::vector< reflog_type > Repository::reflog(branch_id b) const
{
	return pimpl->reflog(b);
}

std::vector< reflog_type > Repository::reflog(tag_id b) const
{
	return pimpl->reflog(b);
}

const std::map< hash_type, commit_to_type >::value_type& Repository::relations(const hash_type& h) const
{
	return pimpl->relations(h);
}

std::string Repository::reference(branch_id b) const
{
	return pimpl->reference(b);
}

std::string Repository::reference(tag_id b) const
{
	return pimpl->reference(b);
}
