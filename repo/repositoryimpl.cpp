#include "repositoryimpl.h"

#include <cstring>
#include <set>
#include <ctime>
#include <algorithm>
#include <stdexcept>
#include <map>
#include <iostream>
#include <sstream>
#include <fstream>

#include <git2.h>

namespace
{
bool less_oid(
	const git_oid& l,
	const git_oid& r
)
{
	return git_oid_cmp(&l, &r) < 0;
}

bool compare_by_hash(
	const std::pair< commit_type, std::vector< hash_type > >& l,
	const std::pair< commit_type, std::vector< hash_type > >& r
)
{
	return l.first.hashxxx < r.first.hashxxx;
}

struct CompareContributor
{
	const std::vector< contributor_type >& contributors;

	bool operator()(
		std::size_t li,
		const std::pair< const char*, const char* >& r
	) const
	{
		const contributor_type& l   = contributors[li];
		const int               res = l.name.compare(r.first);

		return res < 0 || ( res == 0 && l.email.compare(r.second) < 0 );
	}

};


class find_hash
{
public:
	explicit find_hash(const directed_acyclic_graph< commit_type >& u)
		: unsortedcommits(u)
	{
	}

	bool operator()(
		commit_id        c,
		const hash_type& h
	)
	{
		return unsortedcommits[c.get()].hashxxx < h;
	}

private:
	const directed_acyclic_graph< commit_type >& unsortedcommits;
};

git_oid hash_to_oid(const hash_type& h)
{
	char s[41];

	h.to_string(s);

	git_oid oid;

	if ( git_oid_fromstr(&oid, s) == GIT_OK )
	{
		return oid;
	}

	throw std::logic_error("Bad hash");
}

bool compare_commit_by_date(
	const commit_type& l,
	const commit_type& r
)
{
	return std::difftime(l.actual_time, r.actual_time) > 0;
}

struct SortCommitsByDate
{
	using id_type = typename directed_acyclic_graph< commit_type >::node_id;

	const directed_acyclic_graph< commit_type >& commits;

	bool operator()(
		id_type i,
		id_type j
	) const
	{
		return compare_commit_by_date(commits[i], commits[j]);
	}

};

struct ProxySortCommitsByDate
{
	using proxy_id = typename directed_acyclic_graph< commit_type >::node_id;
	using id_type  = typename directed_acyclic_graph< proxy_id >::node_id;

	const directed_graph< commit_type >& commits;
	const directed_graph< proxy_id >& proxy;

	bool operator()(
		id_type i,
		id_type j
	) const
	{
		return compare_commit_by_date(commits[proxy[i]], commits[proxy[j]]);
	}

};
}


RepositoryImpl::RepositoryImpl(std::string path_)
	: path(std::move(path_) ), repo(nullptr)
{
	if ( git_repository_open(&repo, path.c_str() ) == GIT_OK )
	{
		refresh();
	}
	else
	{
		repo = nullptr;
	}
}

RepositoryImpl::~RepositoryImpl()
{
	if ( repo != nullptr )
	{
		git_repository_free(repo);
	}
}

bool RepositoryImpl::is_valid() const
{
	return repo != nullptr;
}

branch_id RepositoryImpl::create_branch(
	git_oid            oid,
	const std::string& s
)
{
	branch_id b = INVALID_BRANCH;

	if ( repo != nullptr )
	{
		// oid to commit
		git_commit* c;

		if ( git_commit_lookup(&c, repo, &oid) == GIT_OK )
		{
			// create branch
			git_reference* out;

			if ( git_branch_create(&out, repo, s.c_str(), c, 0) == GIT_OK )
			{
				const hash_type h = hash_type::from_array(oid.id);

				b = branches.emplace(s, h);

				auto it = xrefs.find(h);

				if ( it != xrefs.end() )
				{
					it->second.branches.insert(b);
				}
				else
				{
					xrefs.emplace(h, commit_to_type{ { b } });
				}

				git_reference_free(out);
			}

			git_commit_free(c);
		}
	}

	return b;
}

bool RepositoryImpl::rename_branch(
	branch_id          i,
	const std::string& s
)
{
	bool success = false;

	if ( repo != nullptr )
	{
		auto it = branches.find(i);

		if ( it != branches.end() )
		{
			git_reference* orig;

			if ( git_branch_lookup(&orig, repo, it->second.name.c_str(), GIT_BRANCH_LOCAL) == GIT_OK )
			{
				git_reference* out;

				if ( git_branch_move(&out, orig, s.c_str(), 0) == GIT_OK )
				{
					success         = true;
					it->second.name = s;
					git_reference_free(out);
				}

				git_reference_free(orig);
			}
		}
	}

	return success;
}

bool RepositoryImpl::remove_branch(branch_id i)
{
	bool success = false;

	if ( repo != nullptr )
	{
		auto jt = branches.find(i);

		if ( jt != branches.end() )
		{
			git_reference* orig;

			if ( git_branch_lookup(&orig, repo, jt->second.name.c_str(), GIT_BRANCH_LOCAL) == GIT_OK )
			{
				if ( git_branch_delete(orig) == GIT_OK )
				{
					success = true;

					// Erase cross references
					auto it = xrefs.find(jt->second.hash);

					if ( it != xrefs.end() )
					{
						it->second.branches.erase(i);
					}

					// Delete the branch
					branches.erase(jt);
				}

				git_reference_free(orig);
			}
		}
	}

	return success;
}

bool RepositoryImpl::remove_remote_branch(
	remote_id   r,
	std::size_t i
)
{
	bool success = false;

	if ( repo != nullptr )
	{
		auto jt = remotes.find(r);

		if ( jt != remotes.end() )
		{
			git_reference* orig;

			const std::string name = "refs/remotes/" + jt->second.full_name(i);

			if ( git_reference_lookup(&orig, repo, name.c_str() ) == GIT_OK )
			{
				if ( git_branch_delete(orig) == GIT_OK )
				{
					success = true;

					jt->second.branches.erase(jt->second.branches.begin() + i);
				}

				git_reference_free(orig);
			}
		}
	}

	return success;
}

bool RepositoryImpl::fetch_remote(remote_id r)
{
	git_remote* rem;

	bool success = false;

	if ( git_remote_lookup(&rem, repo, remotes[r].name.c_str() ) == GIT_OK )
	{
		git_fetch_options opts = GIT_FETCH_OPTIONS_INIT;

		if ( git_remote_fetch(rem, nullptr, &opts, nullptr) == GIT_OK )
		{
			refresh();
			success = true;
		}

		git_remote_free(rem);
	}

	return success;
}

branch_id RepositoryImpl::find_branch(const std::string& s) const
{
	for ( const auto& b : branches )
	{
		if ( b.second.name == s )
		{
			return b.first;
		}
	}

	return INVALID_BRANCH;
}

commit_id RepositoryImpl::branch_to_commit(branch_id b) const
{
	auto it = branches.find(b);

	if ( it != branches.end() )
	{
		return hash_to_commit(it->second.hash);
	}

	return INVALID_COMMIT;
}

branch_id RepositoryImpl::create_branch_from_branch(
	branch_id          b,
	const std::string& s
)
{
	if ( repo != nullptr )
	{
		const std::string longname = reference(b);

		git_oid oid;

		if ( git_reference_name_to_id(&oid, repo, longname.c_str() ) == GIT_OK )
		{
			return create_branch(oid, s);
		}
	}

	return INVALID_BRANCH;
}

branch_id RepositoryImpl::create_branch_from_commit(
	commit_id          i,
	const std::string& s
)
{
	auto it = commits.find(i.get() );

	if ( it != commits.end() )
	{
		const git_oid oid = hash_to_oid(it->second.hashxxx);

		return create_branch(oid, s);
	}

	return INVALID_BRANCH;
}

remote_id RepositoryImpl::add_remote(
	const std::string& url,
	const std::string& name
)
{
	git_remote* remote;

	remote_id r = INVALID_REMOTE;

	if ( repo != nullptr && git_remote_create(&remote, repo, name.c_str(), url.c_str() ) == GIT_OK )
	{
		r = remotes.emplace(name, url, std::vector< branch_type >{});
		git_remote_free(remote);
	}

	return r;
}

bool RepositoryImpl::rename_remote(
	remote_id          i,
	const std::string& s
)
{
	bool success = false;

	auto it = remotes.find(i);

	if ( it != remotes.end() )
	{
		git_strarray problems;

		if ( git_remote_rename(&problems, repo, it->second.name.c_str(), s.c_str() ) == GIT_OK )
		{
			success         = true;
			it->second.name = s;
			git_strarray_free(&problems);
		}
	}

	return success;
}

bool RepositoryImpl::set_url_remote(
	remote_id          i,
	const std::string& s
)
{
	auto it = remotes.find(i);

	if ( it != remotes.end() )
	{
		if ( git_remote_set_url(repo, it->second.name.c_str(), s.c_str() ) == GIT_OK )
		{
			it->second.url = s;
			return true;
		}
	}

	return false;
}

commit_id RepositoryImpl::hash_to_commit(const hash_type& h) const
{
	const auto first = commits_by_hash.begin();
	const auto last  = commits_by_hash.end();
	const auto it    = std::lower_bound(first, last, h, find_hash(commits) );

	if ( it != last && commits[it->get()].hashxxx == h )
	{
		return *it;
	}

	return INVALID_COMMIT;
}

/// @todo Translate (r,b) to remote_id,std::size_t
commit_id RepositoryImpl::remote_branch_to_commit(
	remote_id   r,
	std::size_t b
) const
{
	auto it = remotes.find(r);

	if ( it != remotes.end() )
	{
		if ( b < it->second.branches.size() )
		{
			return hash_to_commit(it->second.branches[b].hash);
		}
	}

	return INVALID_COMMIT;
}

bool RepositoryImpl::branch_to_oid(
	git_oid*  poid,
	branch_id s
) const
{
	auto it = branches.find(s);

	if ( it != branches.end() )
	{
		*poid = hash_to_oid(it->second.hash);
		return true;
	}

	return false;
}

bool RepositoryImpl::create_tag_from_commit(
	commit_id          i,
	const std::string& s
)
{
	bool success = false;
	auto it      = commits.find(i.get() );

	if ( it != commits.end() )
	{
		git_oid oid = hash_to_oid(it->second.hashxxx);

		git_object* obj;

		if ( git_object_lookup(&obj, repo, &oid, GIT_OBJ_COMMIT) == GIT_OK )
		{
			git_oid out;

			if ( git_tag_create_lightweight(&out, repo, s.c_str(), obj, 0) == GIT_OK )
			{
				success = true;

				const auto t = tags.emplace(s, it->second.hashxxx);

				auto jt = xrefs.find(it->second.hashxxx);

				if ( jt != xrefs.end() )
				{
					jt->second.tags.insert(t);
				}
				else
				{
					xrefs.emplace(it->second.hashxxx, commit_to_type{ {}, { t } });
				}
			}

			git_object_free(obj);
		}
	}

	return success;
}

bool RepositoryImpl::create_tag_from_commit(
	commit_id          i,
	const std::string& s,
	const std::string& m
)
{
	bool success = false;
	auto it      = commits.find(i.get() );

	if ( it != commits.end() )
	{
		git_oid oid = hash_to_oid(it->second.hashxxx);

		git_object* obj;

		if ( git_object_lookup(&obj, repo, &oid, GIT_OBJ_COMMIT) == GIT_OK )
		{
			git_signature* sig;

			if ( git_signature_default(&sig, repo) == GIT_OK )
			{
				git_oid out;

				if ( git_tag_create(&out, repo, s.c_str(), obj, sig, m.c_str(), 0) == GIT_OK )
				{
					success = true;
					const auto t  = tags.emplace(s, it->second.hashxxx, sig->name, sig->email, m);
					auto       jt = xrefs.find(it->second.hashxxx);

					if ( jt != xrefs.end() )
					{
						jt->second.tags.insert(t);
					}
					else
					{
						xrefs.emplace(it->second.hashxxx, commit_to_type{ {}, { t } });
					}
				}

				git_signature_free(sig);
			}

			git_object_free(obj);
		}
	}

	return success;
}

bool RepositoryImpl::remove_tag(tag_id i)
{
	auto it = tags.find(i);

	if ( it != tags.end() )
	{
		if ( git_tag_delete(repo, it->second.name.c_str() ) == GIT_OK )
		{
			// Delete cross-references
			auto jt = xrefs.find(it->second.hash);

			if ( jt != xrefs.end() )
			{
				jt->second.tags.erase(i);
			}

			// Delete the tag
			tags.erase(it);
			return true;
		}
	}

	return false;
}

bool RepositoryImpl::push_stash()
{
	git_signature* sig;

	if ( git_signature_default(&sig, repo) == GIT_OK )
	{
		git_oid oid;

		if ( git_stash_save(&oid, repo, sig, "", GIT_STASH_DEFAULT) == GIT_OK )
		{
			update_stash();
			update_status();
			return true;
		}
	}

	return false;
}

bool RepositoryImpl::pop_stash(stash_id i)
{
	auto it = stashes.find(i);

	if ( it != stashes.end() )
	{
		if ( git_stash_pop(repo, it->second.idx, nullptr) == GIT_OK )
		{
			remove_stash(it);
			update_status();

			return true;
		}
	}

	return false;
}

bool RepositoryImpl::drop_stash(stash_id i)
{
	auto it = stashes.find(i);

	if ( it != stashes.end() )
	{
		if ( git_stash_drop(repo, it->second.idx) == GIT_OK )
		{
			remove_stash(it);

			return true;
		}
	}

    return false;
}

commit_id RepositoryImpl::stash_to_commit(stash_id i) const
{
    auto it = stashes.find(i);

    if ( it != stashes.end() )
    {
        return hash_to_commit(it->second.hash);
    }

    return INVALID_COMMIT;
}

void RepositoryImpl::remove_stash(idmap< stash_type, stash_id >::iterator it)
{
	const std::size_t idx = it->second.idx;

	stashes.erase(it);

	for ( auto jt = stashes.begin(), last = stashes.end(); jt != last; ++jt )
	{
		if ( jt->second.idx > idx )
		{
			jt->second.idx -= 1;
		}
	}
}

bool RepositoryImpl::add_to_index(const std::string& s)
{
	bool       success = false;
	git_index* idx     = nullptr;

	if ( git_repository_index(&idx, repo) == GIT_OK )
	{
		if ( git_index_add_bypath(idx, s.c_str() ) == GIT_OK )
		{
			if ( git_index_write(idx) == GIT_OK )
			{
				success = true;

				update_file_status(s);
			}
		}

		git_index_free(idx);
	}

	return success;
}

/// @bug This removes the file from tracking. Actually supposed to use git_index_add to add back the
/// desired state of the file (i.e., HEAD)
bool RepositoryImpl::remove_from_index(const std::vector< status_id >& v)
{
	bool           success = false;
	git_reference* head_;

	if ( git_repository_head(&head_, repo) == GIT_OK )
	{
		git_object* head_commit;

		if ( git_reference_peel(&head_commit, head_, GIT_OBJ_COMMIT) == GIT_OK )
		{
			std::vector< char* > paths;

			for ( const auto id : v )
			{
				auto it = status.find(id);

				if ( it != status.end() )
				{
					paths.push_back(it->second.path.data() );
				}
				else
				{
					return false;
				}
			}

			git_strarray a = { paths.data(), paths.size() };

			if ( git_reset_default(repo, head_commit, &a) == GIT_OK )
			{
				success = true;
			}

			git_object_free(head_commit);
		}

		git_reference_free(head_);
	}

	return success;
}

/*
   bool RepositoryImpl::untrack(const std::vector< status_id >& v)
   {
    bool       success = true;
    git_index* idx = nullptr;

    if ( git_repository_index(&idx, repo) == GIT_OK )
    {
        bool any = false;

        for (const auto id : v)
        {
            auto it = status.find(id);
            if (it != status.end())
            {
                if ( git_index_remove_bypath(idx, it->second.path.c_str() ) == GIT_OK )
                {
                    any = true;
                }
                else success = false;
            }
            else success = false;
        }

        if (any)
        {
            if ( git_index_write(idx) == GIT_OK )
            {
                update_status();
            }
            else success = false;
        }

        git_index_free(idx);
    }

    return success;
   }
 */

tag_id RepositoryImpl::find_tag(const std::string& s) const
{
	for ( const auto& ti : tags )
	{
		if ( ti.second.name == s )
		{
			return ti.first;
		}
	}

	return INVALID_TAG;
}

std::string RepositoryImpl::reference(tag_id i) const
{
	const auto jt = tags.find(i);

	if ( jt != tags.end() )
	{
		return "refs/tags/" + jt->second.name;
	}

	return {};
}

std::vector< reflog_type > RepositoryImpl::reflog(tag_id i) const
{
	return reflog(reference(i) );
}

bool RepositoryImpl::checkout(tag_id t) const
{
	auto it = tags.find(t);

	if ( it != tags.end() )
	{
		git_object* o;

		if ( git_revparse_single(&o, repo, it->second.name.c_str() ) == GIT_OK )
		{
			git_checkout_options opts = GIT_CHECKOUT_OPTIONS_INIT;
			opts.checkout_strategy = GIT_CHECKOUT_SAFE;

			if ( git_checkout_tree(repo, o, &opts) == GIT_OK )
			{
				if ( git_repository_set_head(repo, reference(t).c_str() ) == GIT_OK )
				{
					return true;
				}
			}

			git_object_free(o);
		}
	}

	return false;
}

commit_id RepositoryImpl::tag_to_commit(tag_id t) const
{
	auto it = tags.find(t);

	if ( it != tags.end() )
	{
		return hash_to_commit(it->second.hash);
	}

	return INVALID_COMMIT;
}

std::string RepositoryImpl::get_remote_url(const char* r) const
{
	std::string url;
	git_remote* rem;

	if ( git_remote_lookup(&rem, repo, r) == GIT_OK )
	{
		if ( const char* p = git_remote_url(rem) )
		{
			url = p;
		}

		git_remote_free(rem);
	}

	return url;
}

const contributor_type& RepositoryImpl::contributor(contributor_id i) const
{
	if ( i != INVALID_CONTRIBUTOR )
	{
		return contributors.at(i.get() );
	}

	static const contributor_type fallback = { {}, {}, 0, 0, INVALID_COMMIT, INVALID_COMMIT };
	return fallback;
}

/// @todo #commits and #authorships per contributor, last commit, last authorship
/// @todo #lines per blame
std::size_t RepositoryImpl::number_of_contributors() const
{
	return contributors.size();
}

remote_id RepositoryImpl::find_remote(const std::string& s) const
{
	for ( const auto& x : remotes )
	{
		if ( x.second.name == s )
		{
			return x.first;
		}
	}

	return INVALID_REMOTE;
}

int RepositoryImpl::stash_callback(
	std::size_t    i,
	const char*    msg,
	const git_oid* oid,
	void*          stashes_
)
{
	idmap< stash_type, stash_id >* stashes = static_cast< idmap< stash_type, stash_id >* >( stashes_ );

	stashes->emplace(i, msg, hash_type::from_array(oid->id) );
	return 0;
}

diff_type RepositoryImpl::diff_commits(
	commit_id i,
	commit_id j,
	bool      brief
) const
{
	diff_type t;

	const auto it = commits.find(i.get() );
	const auto jt = commits.find(j.get() );

	if ( it != commits.end() && jt != commits.end() )
	{
		git_oid oid = hash_to_oid(it->second.hashxxx);

		git_commit* c;

		if ( git_commit_lookup(&c, repo, &oid) == GIT_OK )
		{
			oid = hash_to_oid(jt->second.hashxxx);

			git_commit* parent;

			if ( git_commit_lookup(&parent, repo, &oid) == GIT_OK )
			{
				t = diff_inner(c, parent, brief);
				git_commit_free(parent);
			}

			git_commit_free(c);
		}
	}

	return t;
}

diff_type RepositoryImpl::diff_with_index(commit_id i) const
{
	diff_type t;

	auto it = commits.find(i.get() );

	if ( it != commits.end() )
	{
		git_oid oid = hash_to_oid(it->second.hashxxx);

		git_commit* c;

		if ( git_commit_lookup(&c, repo, &oid) == GIT_OK )
		{
			t = diff_with_index_inner(c, false);

			git_commit_free(c);
		}
	}

	return t;
}

diff_type RepositoryImpl::diff_with_workdir(commit_id i) const
{
	diff_type t;

	const auto it = commits.find(i.get() );

	if ( it != commits.end() )
	{
		git_oid oid = hash_to_oid(it->second.hashxxx);

		git_commit* c;

		if ( git_commit_lookup(&c, repo, &oid) == GIT_OK )
		{
			t = diff_with_workdir_inner(c, false);

			git_commit_free(c);
		}
	}

	return t;
}

diff_type RepositoryImpl::diff_inner(
	const git_commit* from,
	const git_commit* to,
	bool              brief
) const
{
	diff_type t;

	git_tree* from_tree = nullptr;
	git_tree* to_tree   = nullptr;

	if ( git_commit_tree(&from_tree, from) == GIT_OK && git_commit_tree(&to_tree, to) == GIT_OK )
	{
		git_diff* diff = nullptr;

		if ( git_diff_tree_to_tree(&diff, repo, to_tree, from_tree, nullptr) == GIT_OK )
		{
			diff_inner_inner(diff, t, brief);

			git_diff_free(diff);
		}

		git_tree_free(from_tree);
		git_tree_free(to_tree);
	}

	return t;
}

diff_type RepositoryImpl::diff_with_index_inner(
	const git_commit* from,
	bool              brief
) const
{
	diff_type t;

	git_tree* from_tree = nullptr;

	if ( git_commit_tree(&from_tree, from) == GIT_OK )
	{
		git_diff* diff = nullptr;

		if ( git_diff_tree_to_index(&diff, repo, from_tree, nullptr, nullptr) == GIT_OK )
		{
			diff_inner_inner(diff, t, brief);

			git_diff_free(diff);
		}

		git_tree_free(from_tree);
	}

	return t;
}

diff_type RepositoryImpl::diff_with_workdir_inner(
	const git_commit* from,
	bool              brief
) const
{
	diff_type t;

	git_tree* from_tree = nullptr;

	if ( git_commit_tree(&from_tree, from) == GIT_OK )
	{
		git_diff* diff = nullptr;

		if ( git_diff_tree_to_workdir(&diff, repo, from_tree, nullptr) == GIT_OK )
		{
			diff_inner_inner(diff, t, brief);

			git_diff_free(diff);
		}

		git_tree_free(from_tree);
	}

	return t;
}

void RepositoryImpl::diff_inner_inner(
	git_diff*  diff,
	diff_type& t,
	bool       brief
) const
{
	git_diff_find_similar(diff, nullptr);

	if ( brief )
	{
		git_diff_foreach(diff, file_callback, nullptr, nullptr, nullptr, &t);
	}
	else
	{
		git_diff_foreach(diff, file_callback, nullptr, hunk_callback, line_callback, &t);
	}
}

void RepositoryImpl::diff_find_file(
	git_diff* diff,
	std::pair< bool, std::set< std::string > >& p
) const
{
	git_diff_find_similar(diff, nullptr);
	git_diff_foreach(diff, find_file_callback, nullptr, nullptr, nullptr, &p);
}

bool RepositoryImpl::move_branch_to_commit(
	branch_id branch,
	commit_id i
)
{
	auto it = commits.find(i.get() );

	if ( it != commits.end() )
	{
		const git_oid oid = hash_to_oid(it->second.hashxxx);

		return move_branch_to_commit_inner(branch, &oid);
	}

	return false;
}

bool RepositoryImpl::move_branch_to_commit_inner(
	branch_id      bi,
	const git_oid* poid
)
{
	const auto it = branches.find(bi);

	bool success = false;

	if ( it != branches.end() )
	{
		git_reference* bref;

		if ( git_branch_lookup(&bref, repo, it->second.name.c_str(), GIT_BRANCH_LOCAL) == GIT_OK )
		{
			git_reference* out;

			if ( git_reference_set_target(&out, bref, poid, "") == GIT_OK )
			{
				// remote branch from old commit
				auto jt = xrefs.find(it->second.hash);

				if ( jt != xrefs.end() )
				{
					jt->second.branches.erase(bi);
				}

				// update hash of branch
				it->second.hash = hash_type::from_array(poid->id);

				// add branch to new commit
				jt = xrefs.find(it->second.hash);

				if ( jt != xrefs.end() )
				{
					jt->second.branches.insert(bi);
				}
				else
				{
					xrefs.emplace(it->second.hash, commit_to_type{ { bi } });
				}

				success = true;

				git_reference_free(out);
			}

			git_reference_free(bref);
		}
	}

	return success;
}

bool RepositoryImpl::rebase(
	const hash_type&                  onto, // a hash
	branch_id                         tip,  // branch
	const std::optional< hash_type >& from,
	bool                              recursive
)
{
	git_signature* committer = nullptr;

	if ( git_signature_default(&committer, repo) == GIT_OK )
	{
		std::unordered_set< commit_id > all_branch_commits;
		std::unordered_set< commit_id > dont_rebase;

		if ( recursive )
		{
			for ( const auto& bi : branches )
			{
				all_branch_commits.insert(branch_to_commit(bi.first) );
			}

			// don't rebase branches which have "onto" as a descendent
			const auto onto_commit = hash_to_commit(onto);

			for ( const auto& bi : branches )
			{
				const auto id = branch_to_commit(bi.first);

				if ( is_descendant_of(id, onto_commit) )
				{
					dont_rebase.insert(id);
				}
			}
		}

		const bool res = rebase_recursive(committer, onto, tip, from, recursive, all_branch_commits, dont_rebase);
		git_signature_free(committer);
		return res;
	}

	return false;
}

bool RepositoryImpl::rebase_recursive(
	const git_signature*                   committer,
	const hash_type&                       onto, // a hash
	branch_id                              tip,  // branch
	const std::optional< hash_type >&      from,
	bool                                   recursive,
	const std::unordered_set< commit_id >& all_branch_commits,
	const std::unordered_set< commit_id >& dont_rebase
)
{
	auto it = branches.find(tip);

	if ( it != branches.end() )
	{
		// get the old commit of tip, so we can rebase it's children
		const std::optional< hash_type > old_tip = it->second.hash;

		std::vector< branch_id > children;

		if ( recursive )
		{
			// find all branches who have tip as parent commit
			const auto tip_commit = branch_to_commit(tip);

			for ( const auto& [child, info] : branches )
			{
				if ( child != tip )
				{
					// If child has tip as an ancestor, with no intervening branches
					auto c = branch_to_commit(child);

					if ( dont_rebase.count(c) == 0 &&
					     ( c == tip_commit || is_direct_child_of(all_branch_commits, tip_commit, c) ) )
					{
						// coincident with tip
						children.push_back(child);
					}
				}
			}
		}

		const git_oid onto_oid = hash_to_oid(onto);

		bool res = false;

		if ( from )
		{
			const git_oid from_oid = hash_to_oid(*from);

			res = rebase_inner(committer, &onto_oid, tip, &from_oid);
		}
		else
		{
			res = rebase_inner(committer, &onto_oid, tip, nullptr);
		}

		if ( res )
		{
			const auto new_tip = branches[tip].hash;

			for ( const branch_id child : children )
			{
				if ( !rebase_recursive(committer, new_tip, child, old_tip, true, all_branch_commits, dont_rebase) )
				{
					res = false;
				}
			}
		}

		return res;
	}

	return false;
}

// precondition: child is not parent
bool RepositoryImpl::is_direct_child_of(
	const std::unordered_set< commit_id >& all_branch_commits,
	commit_id                              parent,
	commit_id                              child
) const
{
	const auto& parents = commits.parents(child.get() );

	// if parents is empty, return false
	if ( parents.empty() )
	{
		return false;
	}

	// if any parent is a branch, and not the given parent, return false
	for ( const auto p_ : parents )
	{
		const commit_id p(p_);

		if ( p != parent && all_branch_commits.count(p) != 0 )
		{
			return false;
		}
	}

	// if the node has exactly parent as a parent, return true
	if ( parents.size() == 1 && parents[0] == parent.get() )
	{
		return true;
	}

	// else if all parents are direct children of given parent, return true
	for ( const auto p_ : parents )
	{
		const commit_id p(p_);

		if ( !is_direct_child_of(all_branch_commits, parent, p) )
		{
			return false;
		}
	}

	return true;
}

bool RepositoryImpl::is_descendant_of(
	commit_id parent,
	commit_id child
) const
{
	if ( child == parent )
	{
		return true;
	}

	for ( const auto pi : commits.parents(child.get() ) )
	{
		if ( is_descendant_of(parent, commit_id(pi) ) )
		{
			return true;
		}
	}

	return false;
}

std::size_t RepositoryImpl::find_remote_branch(
	remote_id          ri,
	const std::string& s
) const
{
	const auto it = remotes.find(ri);

	if ( it != remotes.end() )
	{
		for ( std::size_t i = 0, n = it->second.branches.size(); i < n; ++i )
		{
			if ( it->second.branches[i].name == s )
			{
				return i;
			}
		}
	}

	return -1;
}

/// @bug Use git_tag_create with the force option
bool RepositoryImpl::move_tag_to_commit(
	tag_id    ti,
	commit_id i
)
{
	bool success = false;

	auto it = commits.find(i.get() );

	if ( it != commits.end() )
	{
		auto jt = tags.find(ti);

		if ( jt != tags.end() )
		{
			git_oid oid = hash_to_oid(commits[i.get()].hashxxx);

			git_object* obj;

			if ( git_object_lookup(&obj, repo, &oid, GIT_OBJ_COMMIT) == GIT_OK )
			{
				git_oid tag_oid;

				if ( git_reference_name_to_id(&tag_oid, repo, reference(ti).c_str() ) == GIT_OK )
				{
					git_tag* orig = nullptr;

					if ( git_tag_lookup(&orig, repo, &tag_oid) == GIT_OK )
					{
						git_oid new_oid;

						if ( git_tag_create(&new_oid, repo, jt->second.name.c_str(), obj, git_tag_tagger(orig),
							git_tag_message(orig), true) == GIT_OK )
						{
							success = true;
						}

						git_tag_free(orig);
					}
					else
					{
						git_oid new_oid;

						if ( git_tag_create_lightweight(&new_oid, repo, jt->second.name.c_str(), obj, true) == GIT_OK )
						{
							success = true;
						}
					}

					if ( success )
					{
						auto kt = xrefs.find(jt->second.hash);

						if ( kt != xrefs.end() )
						{
							kt->second.tags.erase(ti);
						}

						jt->second.hash = it->second.hashxxx;

						kt = xrefs.find(it->second.hashxxx);

						if ( kt != xrefs.end() )
						{
							kt->second.tags.insert(ti);
						}
						else
						{
							xrefs.emplace(it->second.hashxxx, commit_to_type{ {}, { ti } });
						}
					}
				}

				git_object_free(obj);
			}
		}
	}

	return success;
}

void RepositoryImpl::update_file_status(const std::string&)
{
	/// @todo Just update the one file
	update_status();
}

void RepositoryImpl::update_status()
{
	idmap< status_type, status_id > t;

	git_status_foreach(repo, status_callback, &t);
	t.swap(status);
}

int RepositoryImpl::status_callback(
	const char* path,
	unsigned    value,
	void*       status_
)
{
	idmap< status_type, status_id >* status = static_cast< idmap< status_type, status_id >* >( status_ );

	unsigned x = 0;

	if ( ( value & GIT_STATUS_CURRENT ) != 0 )
	{
		x |= status_type::CURRENT;
	}

	if ( ( value & GIT_STATUS_INDEX_NEW ) != 0 )
	{
		x |= status_type::INDEX_NEW;
	}

	if ( ( value & GIT_STATUS_INDEX_MODIFIED ) != 0 )
	{
		x |= status_type::INDEX_MODIFIED;
	}

	if ( ( value & GIT_STATUS_INDEX_DELETED ) != 0 )
	{
		x |= status_type::INDEX_DELETED;
	}

	if ( ( value & GIT_STATUS_INDEX_RENAMED ) != 0 )
	{
		x |= status_type::INDEX_RENAMED;
	}

	if ( ( value & GIT_STATUS_WT_NEW ) != 0 )
	{
		x |= status_type::WT_NEW;
	}

	if ( ( value & GIT_STATUS_WT_MODIFIED ) != 0 )
	{
		x |= status_type::WT_MODIFIED;
	}

	if ( ( value & GIT_STATUS_WT_DELETED ) != 0 )
	{
		x |= status_type::WT_DELETED;
	}

	if ( ( value & GIT_STATUS_WT_RENAMED ) != 0 )
	{
		x |= status_type::WT_RENAMED;
	}

	if ( ( value & GIT_STATUS_WT_UNREADABLE ) != 0 )
	{
		x |= status_type::WT_UNREADABLE;
	}

	if ( ( value & GIT_STATUS_IGNORED ) != 0 )
	{
		x |= status_type::IGNORED;
	}

	if ( ( value & GIT_STATUS_CONFLICTED ) != 0 )
	{
		x |= status_type::CONFLICTED;
	}

	status->emplace(path, x);
	return 0;
}

void RepositoryImpl::update_stash()
{
	idmap< stash_type, stash_id > t;

	git_stash_foreach(repo, stash_callback, &t);
	stashes.swap(t);
}

int RepositoryImpl::file_callback(
	const git_diff_delta* delta,
	float,
	void*                 t_
)
{
	diff_type* t       = static_cast< diff_type* >( t_ );
	const bool added   = ( delta->status & GIT_DELTA_ADDED ) != 0;
	const bool removed = ( delta->status & GIT_DELTA_DELETED ) != 0;

	delta_type u = {
		added&& !removed,
		removed&& !added,
		delta->old_file.path, delta->new_file.path
	};

	t->deltas.push_back(u);
	return 0;
}

int RepositoryImpl::find_file_callback(
	const git_diff_delta* delta,
	float,
	void*                 p_
)
{
	std::pair< bool, std::set< std::string > >* p = static_cast< std::pair< bool, std::set< std::string > >* >( p_ );

	if ( p->second.count(delta->old_file.path) != 0 || p->second.count(delta->new_file.path) != 0 )
	{
		p->first = true;
		return 1;
	}

	return 0;
}

int RepositoryImpl::hunk_callback(
	const git_diff_delta*,
	const git_diff_hunk* h,
	void*                t_
)
{
	diff_type* t = static_cast< diff_type* >( t_ );

	hunk_type x =
	{
		std::string(h->header, h->header_len),
		h->old_start,
		h->old_lines,
		h->new_start,
		h->new_lines
	};

	t->deltas.back().hunks.push_back(x);

	return 0;
}

int RepositoryImpl::line_callback(
	const git_diff_delta*,
	const git_diff_hunk*,
	const git_diff_line* l,
	void*                t_
)
{
	diff_type* t = static_cast< diff_type* >( t_ );

	line_type x =
	{
		l->origin,
		l->old_lineno,
		l->new_lineno,
		std::string(l->content, l->content_len)
	};

	t->deltas.back().hunks.back().lines.push_back(x);
	return 0;
}

// Audited above this line ===================================================================================
/// @bug Update commits, head, current branch, status
bool RepositoryImpl::commit_index(const std::string& msg)
{
	bool res = false;

	if ( repo != nullptr )
	{
		git_index* index;

		if ( git_repository_index(&index, repo) == GIT_OK )
		{
			git_oid tree_id;

			if ( git_index_write_tree(&tree_id, index) == GIT_OK )
			{
				git_tree* tree;

				if ( git_tree_lookup(&tree, repo, &tree_id) == GIT_OK )
				{
					git_signature* sig;

					if ( git_signature_default(&sig, repo) == GIT_OK )
					{
						git_buf buf = {};

						if ( git_message_prettify(&buf, msg.c_str(), 0, '\0') == GIT_OK )
						{
							git_oid parent_id;

							if ( git_reference_name_to_id(&parent_id, repo, "HEAD") == GIT_OK )
							{
								git_commit* parent;

								if ( git_commit_lookup(&parent, repo, &parent_id) == GIT_OK )
								{
									git_oid c;

									if ( git_commit_create_v(&c, repo, "HEAD", sig, sig, NULL, msg.c_str(), tree, 1,
										parent) == GIT_OK )
									{
										res = true;
									}

									git_commit_free(parent);
								}
							}
							else
							{
								// Headless means initial commit
								git_oid c;

								if ( git_commit_create(&c, repo, "HEAD", sig, sig, NULL, msg.c_str(), tree, 0,
									nullptr) == GIT_OK )
								{
									res = true;
								}
							}

							git_buf_free(&buf);
						}

						git_signature_free(sig);
					}

					git_tree_free(tree);
				}
			}

			git_index_free(index);
		}
	}

	return res;
}

/// @bug Lots of commits will have changed on the rebased branch
bool RepositoryImpl::rebase_inner(
	const git_signature* committer,
	const git_oid*       onto_oid,
	branch_id            tip,
	const git_oid*       from_oid
)
{
	bool success = false;

	git_oid tip_oid;

	if ( branch_to_oid(&tip_oid, tip) )
	{
		git_annotated_commit* onto_commit;

		if ( git_annotated_commit_lookup(&onto_commit, repo, onto_oid) == GIT_OK )
		{
			git_annotated_commit* tip_commit;

			if ( git_annotated_commit_lookup(&tip_commit, repo, &tip_oid) == GIT_OK )
			{
				git_annotated_commit* from_commit = nullptr;

				if ( from_oid == nullptr || ( git_annotated_commit_lookup(&from_commit, repo, from_oid) == GIT_OK ) )
				{
					git_rebase* re;

					if ( git_rebase_init(&re, repo, tip_commit, from_commit, onto_commit, nullptr) == GIT_OK )
					{
						git_oid newid = *onto_oid;

						while ( true )
						{
							git_rebase_operation* op;
							const int             error = git_rebase_next(&op, re);

							if ( error == GIT_ITEROVER )
							{
								// Rebase succeeded
								git_rebase_finish(re, nullptr);

								// point branch to last commit
								move_branch_to_commit_inner(tip, &newid);

								success = true;
								break;
							}

							/// @todo Handle any action that op indicates
							if ( error != GIT_OK )
							{
								// Rebase failed for some other reason -- abort
								git_rebase_abort(re);
								break;
							}

							if ( op->type == GIT_REBASE_OPERATION_PICK )
							{
								git_oid   id;
								const int e = git_rebase_commit(&id, re, nullptr, committer, nullptr, nullptr);

								if ( e != GIT_OK && e != GIT_EAPPLIED )
								{
									// Commit failed
									git_rebase_abort(re);
									break;
								}

								if ( e != GIT_EAPPLIED )
								{
									newid = id;
								}
							}
							else
							{
								// Unsupported operation
								/// @todo Make sure all operations are supported
								git_rebase_abort(re);
								break;
							}
						}

						git_rebase_free(re);
					}

					git_annotated_commit_free(from_commit);
				}

				git_annotated_commit_free(tip_commit);
			}

			git_annotated_commit_free(onto_commit);
		}
	}

	return success;
}

reduction RepositoryImpl::topological_order() const
{
	using node_id = typename directed_acyclic_graph< commit_type >::node_id;
	const std::size_t n = commits.order();

	/// @todo Rather than return nodes, pass a visitor function to build the reduction as we traverse
	const auto v0 = derp_topo_sort(commits, SortCommitsByDate{ commits });

	reduction v;
	v.reserve(n);
	std::unordered_map< node_id, std::size_t > unmap;

	for ( std::size_t i = 0; i < v0.size(); ++i )
	{
		const auto k = v0[i];
		unmap[k] = i;
		v.push_back(commit_id(k) );
	}

	// Fixup parents to reflect where they are in the sub graph
	for ( std::size_t i = 0; i < v.size(); ++i )
	{
		for ( const node_id j : commits.parents(v0[i]) )
		{
			v.add_parent(i, unmap[j]);
		}
	}

	return v;
}

/* Generate a subgraph of the commits, using only the nodes indicated by keep. Edges still reflect the
 * ancestor relationships. Commits are roughly arranged by date (as much as possible)
 */
/// @todo Optimizations if keep is ordered
/// @todo Store commits in topological order so when we only want to keep a few, we already know
/// what order they should be in, relative to one another
reduction RepositoryImpl::topo_graph(const std::vector< typename directed_acyclic_graph< commit_type >::node_id >& keep)
const
{
	using node_id = typename directed_acyclic_graph< commit_type >::node_id;
	const std::size_t n = commits.order();

	// reduce parents to ancestors in keep
	const auto g = commits.derp(keep);

	const auto v0 = derp_topo_sort(g, ProxySortCommitsByDate{ commits, g });

	reduction v;
	v.reserve(n);
	std::unordered_map< node_id, std::size_t > unmap;

	for ( std::size_t i = 0; i < v0.size(); ++i )
	{
		const auto k = g[v0[i]];
		unmap[k] = i;
		v.push_back(commit_id(k) );
	}

	// Fixup parents to reflect where they are in the sub graph
	for ( std::size_t i = 0; i < v.size(); ++i )
	{
		for ( const node_id j : g.parents(v0[i]) )
		{
			v.add_parent(i, unmap[g[j]]);
		}
	}

	return v;
}

/** @bug Only partially implemented. Parents are not implemented.
 * @todo Determining if diff involves a file could be done at a lower level
 */
reduction RepositoryImpl::reduce(const std::vector< std::string >& files_) const
{
	/// @todo flat_set< std::string_view >
	const std::set< std::string > files(files_.begin(), files_.end() );

	std::vector< typename directed_acyclic_graph< commit_type >::node_id > keep;

	for ( const auto&[i, z] : commits )
	{
		if ( commits.parents(i).empty() )
		{
			// No parent commit -- check if the file exists in the tree
			const git_oid id = hash_to_oid(commits[i].hashxxx);

			git_commit* commit;

			if ( git_commit_lookup(&commit, repo, &id) == GIT_OK )
			{
				git_tree* tree;

				if ( git_commit_tree(&tree, commit) == GIT_OK )
				{
					for ( const auto& s : files )
					{
						if ( git_tree_entry_byname(tree, s.c_str() ) != nullptr )
						{
							keep.push_back(i);
							break;
						}
					}

					git_tree_free(tree);
				}

				git_commit_free(commit);
			}
		}
		else
		{
			// Check diffs against parents to see if the file was modified
			for ( const auto p : commits.parents(i) )
			{
				if ( modifies_files(commit_id(i), commit_id(p), files) )
				{
					keep.push_back(i);
					break;
				}
			}
		}
	}

	return topo_graph(keep);
}

bool RepositoryImpl::modifies_files(
	commit_id                      i,
	commit_id                      p,
	const std::set< std::string >& files
) const
{
	/// @todo Only need to do the diff enough to see the file. Write a custom file_callback
	const diff_type d = diff_commits(i, p, true);

	for ( const auto& delta : d.deltas )
	{
		if ( files.count(delta.old_name) != 0 || files.count(delta.new_name) != 0 )
		{
			return true;
		}
	}

	return false;
}

reduction RepositoryImpl::reduce(
	commit_id from,
	commit_id to
) const
{
	return topo_graph(commits.common(from.get(), to.get() ) );
}

void RepositoryImpl::refresh()
{
	branches.clear();
	remotes.clear();
	tags.clear();
	commits.clear();
	xrefs.clear();
	commits_by_hash.clear();
	stashes.clear();
	status.clear();
	contributors.clear();
	contributor_index.clear();
	head = hash_type();

	{
		git_strarray out;

		if ( git_remote_list(&out, repo) == GIT_OK )
		{
			for ( std::size_t i = 0; i < out.count; ++i )
			{
				remotes.emplace(out.strings[i], get_remote_url(out.strings[i]), std::vector< branch_type >{});
			}

			git_strarray_free(&out);
		}
	}

	std::vector< git_oid > oids;
	{
		git_reference_iterator* it;

		if ( git_reference_iterator_new(&it, repo) == GIT_OK )
		{
			git_reference* ref;

			while ( git_reference_next(&ref, it) == GIT_OK )
			{
				const char* const s = git_reference_name(ref);

				// Get the relevant commit
				git_oid c;

				if ( git_reference_name_to_id(&c, repo, s) == GIT_OK )
				{
					oids.push_back(c);

					if ( git_reference_is_branch(ref) != 0 )
					{
						branches.emplace(s + 11, hash_type::from_array(c.id) );
					}
					else if ( git_reference_is_remote(ref) != 0 )
					{
						if ( const char* x = std::strchr(s + 13, '/') )
						{
							/// @todo string_view
							const std::string_view r(s + 13, x - ( s + 13 ) );

							/// @todo Improve on linear search
							remote_id k = INVALID_REMOTE;

							for ( const auto& z : remotes )
							{
								if ( z.second.name == r )
								{
									k = z.first;
									break;
								}
							}

							if ( k == INVALID_REMOTE )
							{
								std::string t(r.data(), r.size() );
								k = remotes.emplace(std::move(t), get_remote_url(
									t.c_str() ), std::vector< branch_type >{});
							}

							remotes[k].branches.push_back(branch_type{ x + 1, hash_type::from_array(c.id) });
						}
					}
					else if ( git_reference_is_tag(ref) != 0 )
					{
						std::string msg;
						std::string user;
						std::string email;
						hash_type   hash = hash_type::from_array(c.id);

						git_oid oid;

						if ( git_reference_name_to_id(&oid, repo, s) == GIT_OK )
						{
							git_tag* tag;

							if ( git_tag_lookup(&tag, repo, &oid) == GIT_OK )
							{
								if ( const char* p = git_tag_message(tag) )
								{
									msg = p;
								}

								if ( const git_signature* p = git_tag_tagger(tag) )
								{
									user  = p->name;
									email = p->email;
								}

								if ( const git_oid* p = git_tag_target_id(tag) )
								{
									char x[GIT_OID_HEXSZ + 1];

									if ( git_oid_tostr(x, sizeof( x ), p) == x )
									{
										hash = hash_type::from_array(p->id);
									}
								}
							}
						}

						tags.emplace(s + 10, hash, std::move(user), std::move(email), std::move(msg) );
					}
				}
			}

			git_reference_iterator_free(it);
		}
	}

	std::set< git_oid, bool ( * )(const git_oid&, const git_oid&) > seen(less_oid);

	std::vector< std::pair< commit_type, std::vector< hash_type > > > rels;

	std::vector< contributor_type > contributors_;

	std::vector< std::size_t > contributor_index_;

	// Follow all refs to parent commits
	/// @todo It might be faster to traverse commits as they appear on disk, since git stores them in .git/objects
	/// On the other hand, maybe git_reference_iterator_xxx would do exactly that
	while ( !oids.empty() )
	{
		const git_oid oid = oids.back();
		oids.pop_back();

		if ( seen.insert(oid).second )
		{
			git_commit* c;

			if ( git_commit_lookup(&c, repo, &oid) == GIT_OK )
			{
				// Visit commit
				commit_type properties = { hash_type::from_array(oid.id) };

				if ( const char* s = git_commit_message(c) )
				{
					properties.message = s;
				}

				if ( const char* s = git_commit_summary(c) )
				{
					properties.summary = s;
				}

				if ( const git_signature* s = git_commit_author(c) )
				{
					auto first = contributor_index_.begin();
					auto last  = contributor_index_.end();
					auto it    = std::lower_bound(first, last, std::pair< const char*, const char* >(s->name,
						s->email), CompareContributor{ contributors_ });

					if ( it != last && contributors_[*it].name == s->name && contributors_[*it].email == s->email )
					{
						// found the contributor
						contributors_[*it].num_authorships += 1;
						properties.author                   = contributor_id(*it);
					}
					else
					{
						// need to create the contributor
						const std::size_t i = contributors_.size();
						contributors_.push_back(contributor_type{ s->name, s->email, 1, 0, INVALID_COMMIT,
							                                      INVALID_COMMIT });
						contributor_index_.insert(it, i);
						properties.author = contributor_id(i);
					}
				}

				if ( const git_signature* s = git_commit_committer(c) )
				{
					auto first = contributor_index_.begin();
					auto last  = contributor_index_.end();
					auto it    = std::lower_bound(first, last, std::pair< const char*, const char* >(s->name,
						s->email), CompareContributor{ contributors_ });

					if ( it != last && contributors_[*it].name == s->name && contributors_[*it].email == s->email )
					{
						// found the contributor
						contributors_[*it].num_commits += 1;
						properties.committer            = contributor_id(*it);
					}
					else
					{
						// need to create the contributor
						const std::size_t i = contributors_.size();
						contributors_.push_back(contributor_type{ s->name, s->email, 0, 1, INVALID_COMMIT,
							                                      INVALID_COMMIT });
						contributor_index_.insert(it, i);
						properties.committer = contributor_id(i);
					}
				}

				#if 0
				{
					// All files at the time of the commit
					git_tree* tree;

					if ( git_commit_tree(&tree, c) == GIT_OK )
					{
						const std::size_t nentries = git_tree_entrycount(tree);

						for ( std::size_t i = 0; i < nentries; ++i )
						{
							/// @todo If entry is another tree, descend
							const git_tree_entry* e = git_tree_entry_byindex(tree, i);
							properties.allfiles.push_back(git_tree_entry_name(e) );
						}

						git_tree_free(tree);
					}
				}
				#endif

				properties.actual_time = git_commit_time(c);

				{
					const unsigned count = git_commit_parentcount(c);

					std::vector< hash_type > parents;

					// Add parents to queue
					for ( unsigned i = 0; i < count; ++i )
					{
						if ( const git_oid* poid = git_commit_parent_id(c, i) )
						{
							parents.push_back(hash_type::from_array(poid->id) );

							oids.push_back(*poid);
						}
					}

					rels.emplace_back(std::move(properties), std::move(parents) );
				}

				git_commit_free(c);
			}
		}
	}

	/// @todo It may make more sense to leave this alone, and sort commits_by_hash. Possibly skipping rels altogether and
	/// construction the graph directly
	std::sort(rels.begin(), rels.end(), compare_by_hash);

	for ( std::size_t j = 0, n = rels.size(); j < n; ++j )
	{
		const auto committer = rels[j].first.committer;
		const auto author    = rels[j].first.author;
		const auto t         = rels[j].first.actual_time;

		const auto c = commits.emplace(std::move(rels[j].first) );
		commits_by_hash.emplace_back(c);

		{
			const auto i = contributors_[committer.get()].first_activity;

			if ( i == INVALID_COMMIT || std::difftime(t, commits[i.get()].actual_time) < 0 )
			{
				contributors_[committer.get()].first_activity = commit_id(c);
			}
		}

		{
			const auto i = contributors_[committer.get()].last_activity;

			if ( i == INVALID_COMMIT || std::difftime(commits[i.get()].actual_time, t) < 0 )
			{
				contributors_[committer.get()].last_activity = commit_id(c);
			}
		}

		{
			const auto i = contributors_[author.get()].first_activity;

			if ( i == INVALID_COMMIT || std::difftime(t, commits[i.get()].actual_time) < 0 )
			{
				contributors_[author.get()].first_activity = commit_id(c);
			}
		}

		{
			const auto i = contributors_[author.get()].last_activity;

			if ( i == INVALID_COMMIT || std::difftime(commits[i.get()].actual_time, t) < 0 )
			{
				contributors_[author.get()].last_activity = commit_id(c);
			}
		}
	}

	contributors      = std::move(contributors_);
	contributor_index = std::move(contributor_index_);

	// Find the head
	{
		git_oid headid;

		if ( git_reference_name_to_id(&headid, repo, "HEAD") == GIT_OK )
		{
			head = hash_type::from_array(headid.id);
		}
	}

	// Search for branch cross reference
	for ( const auto& b : branches )
	{
		auto it = xrefs.find(b.second.hash);

		if ( it != xrefs.end() )
		{
			it->second.branches.insert(b.first);
		}
		else
		{
			xrefs.emplace(b.second.hash, commit_to_type{ { b.first } });
		}
	}

	// Search for tag cross reference
	for ( const auto& t : tags )
	{
		auto it = xrefs.find(t.second.hash);

		if ( it != xrefs.end() )
		{
			it->second.tags.insert(t.first);
		}
		else
		{
			xrefs.emplace(t.second.hash, commit_to_type{ {}, { t.first } });
		}
	}

	// Search for remotes cross reference
	for ( const auto& r : remotes )
	{
		const std::size_t n = r.second.branches.size();

		for ( std::size_t j = 0; j < n; ++j )
		{
			auto it = xrefs.find(r.second.branches[j].hash);

			if ( it != xrefs.end() )
			{
				it->second.remotes.emplace_back(r.first, j);
			}
			else
			{
				xrefs.emplace(r.second.branches[j].hash, commit_to_type{ {}, {}, { { r.first, j } } });
			}
		}
	}

	// fixup parent refs
	{
		const auto first = commits_by_hash.begin();
		const auto last  = commits_by_hash.end();

		for ( std::size_t i = 0, n = rels.size(); i < n; ++i )
		{
			const std::size_t m = rels[i].second.size();

			for ( std::size_t j = 0; j < m; ++j )
			{
				const auto it = std::lower_bound(first, last, rels[i].second[j], find_hash(commits) );

				if ( it != last && commits[it->get()].hashxxx == rels[i].second[j] )
				{
					commits.connect(it->get(), commits_by_hash[i].get() );
				}
			}
		}
	}

	update_stash();
	update_status();
}

std::vector< blame_type > RepositoryImpl::blame(const std::string& s) const
{
	std::vector< blame_type > res;

	git_blame* blame;

	if ( git_blame_file(&blame, repo, s.c_str(), nullptr) == GIT_OK )
	{
		/// @bug What if file has been modified? Does blame reflect the current file, or only what's committed?
		std::ifstream is(path + "/" + s);

		const uint32_t n = git_blame_get_hunk_count(blame);

		for ( uint32_t i = 0; i < n; ++i )
		{
			if ( const git_blame_hunk* hunk = git_blame_get_hunk_byindex(blame, i) )
			{
				contributor_id c = INVALID_CONTRIBUTOR;

				if ( hunk->final_signature != nullptr )
				{
					auto first = contributor_index.begin();
					auto last  = contributor_index.end();
					auto it    =
						std::lower_bound(first, last,
							std::pair< const char*, const char* >(hunk->final_signature->name,
								hunk->final_signature->email), CompareContributor{ contributors });

					if ( it != last && contributors[*it].name == hunk->final_signature->name &&
					     contributors[*it].email == hunk->final_signature->email )
					{
						c = contributor_id(*it);
					}
				}

				for ( std::size_t j = 0; j < hunk->lines_in_hunk; ++j )
				{
					char buf[GIT_OID_HEXSZ + 1];
					git_oid_tostr(buf, sizeof( buf ), &( hunk->final_commit_id ) );

					std::string l;
					std::getline(is, l);

					res.push_back({ hunk->final_start_line_number + j, buf, c, l });
				}
			}
		}

		git_blame_free(blame);
	}

	return res;
}

bool RepositoryImpl::checkout(branch_id b) const
{
	auto it = branches.find(b);

	if ( it != branches.end() )
	{
		git_object* o;

		if ( git_revparse_single(&o, repo, it->second.name.c_str() ) == GIT_OK )
		{
			git_checkout_options opts = GIT_CHECKOUT_OPTIONS_INIT;
			opts.checkout_strategy = GIT_CHECKOUT_SAFE;

			if ( git_checkout_tree(repo, o, &opts) == GIT_OK )
			{
				if ( git_repository_set_head(repo, reference(b).c_str() ) == GIT_OK )
				{
					return true;
				}
			}

			git_object_free(o);
		}
	}

	return false;
}

std::vector< reflog_type > RepositoryImpl::reflog(const std::string& s) const
{
	std::vector< reflog_type > res;

	if ( repo != nullptr )
	{
		git_reflog* l = nullptr;

		if ( git_reflog_read(&l, repo, s.c_str() ) == GIT_OK )
		{
			const std::size_t n = git_reflog_entrycount(l);

			for ( std::size_t i = 0; i < n; ++i )
			{
				const git_reflog_entry* e = git_reflog_entry_byindex(l, i);

				if ( e != nullptr )
				{
					// curr, prev, name, email, message
					reflog_type x;

					if ( const auto oid = git_reflog_entry_id_new(e) )
					{
						x.curr = hash_type::from_array(oid->id);
					}

					if ( const auto oid = git_reflog_entry_id_old(e) )
					{
						x.prev = hash_type::from_array(oid->id);
					}

					if ( const auto p = git_reflog_entry_message(e) )
					{
						x.message = p;
					}

					if ( const auto sig = git_reflog_entry_committer(e) )
					{
						if ( sig->name )
						{
							x.name = sig->name;
						}

						if ( sig->email )
						{
							x.email = sig->email;
						}
					}

					res.push_back(x);
				}
			}

			git_reflog_free(l);
		}
	}

	return res;
}

std::vector< reflog_type > RepositoryImpl::reflog(branch_id b) const
{
	return reflog(reference(b) );
}

std::string RepositoryImpl::reference(branch_id b) const
{
	auto it = branches.find(b);

	if ( it != branches.end() )
	{
		return "refs/heads/" + it->second.name;
	}

	return std::string();
}

const std::map< const hash_type, commit_to_type >::value_type& RepositoryImpl::relations(const hash_type& h) const
{
	auto it = xrefs.find(h);

	if ( it != xrefs.end() )
	{
		return *it;
	}

	static const std::map< const hash_type, commit_to_type >::value_type fallback;
	return fallback;
}
