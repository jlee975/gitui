#-------------------------------------------------
#
# Project created by QtCreator 2018-07-14T21:26:25
#
#-------------------------------------------------
include(../vars.pri)

TARGET = repo
TEMPLATE = lib
CONFIG += staticlib
CONFIG -= qt c++11 c++14

SOURCES += \
    repository.cpp \
    repositoryimpl.cpp

HEADERS += \
    repository.h \
    repositoryimpl.h \
    repotypes.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

INCLUDEPATH += $$PWD/../jlee
