#ifndef REPOSITORYIMPL_H
#define REPOSITORYIMPL_H

#include <string>
#include <vector>
#include <set>
#include <map>
#include <optional>

#include <git2/types.h>
#include <git2/oid.h>
#include <git2/diff.h>

#include "idmap/idmap.h"
#include "graph/dag.h"

#include "repotypes.h"

/// @todo Take string_view or string-by-value as appropriate
/// @todo A lot of this functionality can be moved to Repository, as it doesn't depend on git
struct RepositoryImpl
{
	explicit RepositoryImpl(std::string path_);

	RepositoryImpl(const RepositoryImpl&) = delete;
	RepositoryImpl(RepositoryImpl&&)      = delete;

	~RepositoryImpl();

	RepositoryImpl& operator=(const RepositoryImpl&) = delete;
	RepositoryImpl& operator=(RepositoryImpl&&)      = delete;

	bool is_valid() const;

	const contributor_type& contributor(contributor_id) const;
	std::size_t number_of_contributors() const;

	bool add_to_index(const std::string&);
	bool remove_from_index(const std::vector< status_id >&);
// bool untrack(const std::vector< status_id >&);
	bool commit_index(const std::string&);

	std::vector< reflog_type > reflog(const std::string&) const;

	commit_id hash_to_commit(const hash_type&) const;

	bool rename_branch(branch_id i, const std::string& s);
	bool remove_branch(branch_id i);
	branch_id find_branch(const std::string&) const;
	bool rebase(const hash_type&, branch_id, const std::optional< hash_type >&, bool);
	commit_id branch_to_commit(branch_id) const;
	branch_id create_branch_from_branch(branch_id b, const std::string& s);
	branch_id create_branch_from_commit(commit_id, const std::string & s);
	bool move_branch_to_commit(branch_id, commit_id);
	bool checkout(branch_id) const;
	std::vector< reflog_type > reflog(branch_id) const;
	std::string reference(branch_id) const;

	bool rename_remote(remote_id, const std::string&);
	remote_id add_remote(const std::string&, const std::string&);
	bool set_url_remote(remote_id, const std::string&);
	commit_id remote_branch_to_commit(remote_id, std::size_t) const;
	remote_id find_remote(const std::string&) const;
	std::size_t find_remote_branch(remote_id, const std::string&) const;
	bool remove_remote_branch(remote_id, std::size_t);

	bool create_tag_from_commit(commit_id i, const std::string& s);
	bool create_tag_from_commit(commit_id i, const std::string& s, const std::string&);
	bool remove_tag(tag_id i);
	bool move_tag_to_commit(tag_id, commit_id);
	commit_id tag_to_commit(tag_id) const;
	tag_id find_tag(const std::string&) const;
	std::string reference(tag_id) const;
	std::vector< reflog_type > reflog(tag_id) const;
	bool checkout(tag_id) const;

	bool push_stash();
	bool pop_stash(stash_id);
	bool drop_stash(stash_id);
    commit_id stash_to_commit(stash_id) const;

	diff_type diff_commits(commit_id, commit_id, bool) const;
	diff_type diff_with_index(commit_id) const;
	diff_type diff_with_workdir(commit_id) const;

	reduction topological_order() const;
	reduction reduce(commit_id, commit_id) const;
	reduction reduce(const std::vector< std::string >&) const;

	std::vector< blame_type > blame(const std::string&) const;
	static int file_callback(const git_diff_delta* delta, float, void* t_);
	static int find_file_callback(const git_diff_delta* delta, float, void* t_);
	static int hunk_callback(const git_diff_delta*, const git_diff_hunk* h, void* t_);
	static int line_callback(const git_diff_delta*, const git_diff_hunk*, const git_diff_line* l, void* t_);
	static int stash_callback(std::size_t, const char*, const git_oid*, void*);
	static int status_callback(const char*, unsigned, void*);
	branch_id create_branch(git_oid oid, const std::string& s);
	diff_type diff_inner(const git_commit* from, const git_commit* to, bool brief) const;
	diff_type diff_with_index_inner(const git_commit*, bool) const;
	diff_type diff_with_workdir_inner(const git_commit*, bool) const;
	void diff_inner_inner(git_diff* diff, diff_type& t, bool) const;
	void diff_find_file(git_diff* diff, std::pair< bool, std::set< std::string > >& p) const;
	bool branch_to_oid(git_oid*, branch_id) const;
	bool move_branch_to_commit_inner(branch_id, const git_oid*);
	bool modifies_files(commit_id, commit_id, const std::set< std::string >&) const;
	bool rebase_inner(const git_signature*, const git_oid*, branch_id, const git_oid*);
	reduction topo_graph(const std::vector< typename directed_acyclic_graph< commit_type >::node_id >&) const;
	void update_file_status(const std::string&);
	void update_status();
	std::string get_remote_url(const char*) const;
	bool fetch_remote(remote_id);
	void refresh();
	commit_type* find_commit_by_hash(const hash_type& h);
	void remove_stash(idmap< stash_type, stash_id >::iterator);
	void update_stash();
	const std::map< const hash_type, commit_to_type >::value_type& relations(const hash_type&) const;
	bool rebase_recursive(const git_signature*, const hash_type&, branch_id, const std::optional< hash_type >&, bool,
	                      const std::unordered_set< commit_id >&, const std::unordered_set< commit_id >&);
	bool is_direct_child_of(const std::unordered_set< commit_id >&, commit_id, commit_id) const;
	bool is_descendant_of(commit_id, commit_id) const;

	/// @todo Could have faster containers by using internal members of each struct for ordering. Ex., commits
	/// have a hash associated with them.
	/// @todo Most of these members should be moved to Repository, and logic about them, too
	/// @todo Could probably use 8 or even 4 bytes of hash instead of 20
	std::string path;
	git_repository* repo;
	idmap< branch_type, branch_id > branches;
	idmap< remote_type, remote_id > remotes;
	idmap< tag_type, tag_id > tags;
	std::vector< contributor_type > contributors;
	std::vector< std::size_t > contributor_index;
	directed_acyclic_graph< commit_type > commits;
	std::unordered_map< hash_type, commit_to_type > xrefs;

	/// @todo May not need this after switch to commit_id
	/// @todo unordered_set using hash of commits[id]. Possibly setup as a bimap
	std::vector< commit_id > commits_by_hash;
	idmap< stash_type, stash_id > stashes;
	idmap< status_type, status_id > status;
	hash_type head;
};


#endif // REPOSITORYIMPL_H
