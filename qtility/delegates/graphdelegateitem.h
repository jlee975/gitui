#ifndef GRAPHDELEGATEITEM_H
#define GRAPHDELEGATEITEM_H

#include <QMetaType>
#include <QList>

struct GraphDelegateItem
{
	QList< int > in;
	QList< int > out;
	QList< int > here;
	QStringList labels;
};

Q_DECLARE_METATYPE(GraphDelegateItem)

#endif // GRAPHDELEGATEITEM_H
