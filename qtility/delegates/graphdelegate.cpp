#include "graphdelegate.h"

#include <set>

#include <QPainter>
#include <QColor>

#include "graphdelegateitem.h"

QRgb colours[] =
{
	0x212121,
	0xFF0000,
	0x3F51B5,
	0x4CAF50,
	0x9C27B0,
	0xe57373,
	0x2196F3,
	0xffa000,
	0xFF00FF,
	0x0000FF,
	0x795548
};

GraphDelegate::GraphDelegate(QWidget* parent_)
	: QStyledItemDelegate(parent_)
{
	colourlist.assign(std::begin(colours), std::end(colours) );
}

void GraphDelegate::paint(
	QPainter*                   painter,
	const QStyleOptionViewItem& option,
	const QModelIndex&          idx
) const
{
	// Base class can draw the background, etc.
	QStyledItemDelegate::paint(painter, option, idx);

	QVariant v = idx.data();

	if ( v.canConvert< GraphDelegateItem >() )
	{
		const GraphDelegateItem z = v.value< GraphDelegateItem >();

		std::set< int > in;
		std::set< int > out;
		std::set< int > here;

		int m = 0;

		for ( int i = 0; i < z.in.count(); ++i )
		{
			const int t = z.in.at(i);
			in.insert(t);

			if ( t > m )
			{
				m = t;
			}
		}

		for ( int i = 0; i < z.out.count(); ++i )
		{
			const int t = z.out.at(i);
			out.insert(t);

			if ( t > m )
			{
				m = t;
			}
		}

		for ( int i = 0; i < z.here.count(); ++i )
		{
			const int t = z.here.at(i);
			here.insert(t);

			if ( t > m )
			{
				m = t;
			}
		}

		const int y0 = option.rect.topLeft().y();
		const int y1 = option.rect.bottomRight().y();
		const int ym = ( y0 + y1 ) / 2;

		const QPen original_pen = painter->pen();
		QPen       pen(Qt::SolidLine);
		pen.setWidth(0);
		pen.setCosmetic(true);

		int node_drawn = -1;

		for ( int i = 0; i <= m; ++i )
		{
			const QColor c = colourlist[i % colourlist.size()];
			pen.setColor(c);
			painter->setPen(pen);
			const int x = 9 * i + 4;

			const bool isin  = in.count(i);
			const bool isout = out.count(i);

			if ( isin )
			{
				if ( isout )
				{
					// goes straight through
					painter->drawLine(x, y0, x, y1);
				}
				else
				{
					// ends here
					painter->drawLine(x, y0, x, ym);
				}
			}
			else if ( isout )
			{
				// starts here and continues
				painter->drawLine(x, ym, x, y1);
			}

			// Draw boxes
			if ( here.count(i) != 0 )
			{
				painter->fillRect(x - 3, ym - 3, 7, 7, c);

				if ( node_drawn != -1 )
				{
					painter->drawLine(9 * node_drawn + 8, ym, x, ym);
				}

				node_drawn = i;
			}
		}

		// Reset the text color
		if ( option.state & QStyle::State_Selected )
		{
			painter->setPen(option.palette.color(QPalette::HighlightedText) );
		}
		else
		{
			painter->setPen(original_pen);
		}

		if ( !z.labels.isEmpty() )
		{
			// Draw branch, tag labels
			QRect r = option.rect;
			r.setLeft(9 * ( m + 1 ) + 1);

			for ( int i = 0; i < z.labels.count(); ++i )
			{
				/// @todo Should be drawn in bold font if the cell would have used it
				QRect bounding;
				painter->drawText(r, Qt::AlignLeft | Qt::AlignVCenter | Qt::TextSingleLine, z.labels.at(i), &bounding);
				r.setLeft(bounding.right() + 6);
			}
		}

		painter->setPen(original_pen);
	}
}

void GraphDelegate::setColors(const std::vector< QColor >& cl)
{
	if ( !cl.empty() )
	{
		colourlist = cl;
	}
	else
	{
		colourlist.assign(std::begin(colours), std::end(colours) );
	}
}
