#ifndef GRAPHDELEGATE_H
#define GRAPHDELEGATE_H

#include <vector>

#include <QStyledItemDelegate>
#include <QColor>

class GraphDelegate
	: public QStyledItemDelegate
{
	Q_OBJECT
public:

	GraphDelegate(QWidget* = nullptr);

	void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const override;
	void setColors(const std::vector< QColor >&);
private:
	std::vector< QColor > colourlist;
};


#endif // GRAPHDELEGATE_H
