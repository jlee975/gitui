#include "graphview.h"

#include "../models/graphmodel.h"
#include "../delegates/graphdelegate.h"

GraphView::GraphView(QWidget* parent_)
	: HideableColumnsTable(parent_)
{

}

void GraphView::setModel(QAbstractItemModel* m)
{
	// remove delegates
	for ( const auto [c, d] : graph_delegates )
	{
		setItemDelegateForColumn(c, nullptr);
		delete d;
	}

	graph_delegates.clear();

	HideableColumnsTable::setModel(m);

	// add delegates, if appropriate
	if ( auto gm = dynamic_cast< GraphModel* >( m ) )
	{
		const int nc = gm->columnCount();

		for ( int c = 0; c < nc; ++c )
		{
			if ( gm->is_graph_column(c) )
			{
				auto d = new GraphDelegate(this);
				setItemDelegateForColumn(c, d);
				graph_delegates[c] = d;
			}
		}
	}
}

void GraphView::set_graph_colours(
	int                          c,
	const std::vector< QColor >& cl
)
{
	auto it = graph_delegates.find(c);

	if ( it != graph_delegates.end() )
	{
		it->second->setColors(cl);
	}
}
