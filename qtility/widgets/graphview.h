#ifndef GRAPHVIEW_H
#define GRAPHVIEW_H

#include <map>

#include "widgets/hideablecolumnstable.h"
class GraphDelegate;

class GraphView
	: public HideableColumnsTable
{
	Q_OBJECT
public:
	explicit GraphView(QWidget*);
	void setModel(QAbstractItemModel*) override;
protected:
	void set_graph_colours(int, const std::vector< QColor >&);
private:
	std::map< int, GraphDelegate* > graph_delegates;
};

#endif // GRAPHVIEW_H
