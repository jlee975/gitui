#ifndef HIDEABLECOLUMNSTABLE_H
#define HIDEABLECOLUMNSTABLE_H

#include <QTreeView>

class QMenu;

class HideableColumnsTable
	: public QTreeView
{
public:
	explicit HideableColumnsTable(QWidget* = nullptr);
	~HideableColumnsTable();

	void setModel(QAbstractItemModel*) override;
private slots:
	void header_menu(const QPoint&);
	void toggle_column(int);
private:

	QMenu* menu;
};

#endif // HIDEABLECOLUMNSTABLE_H
