#include "hideablecolumnstable.h"

#include <iostream>

#include <QHeaderView>
#include <QMenu>

HideableColumnsTable::HideableColumnsTable(QWidget* parent_)
        : QTreeView(parent_), menu(nullptr)
{
	header()->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(header(), &QHeaderView::customContextMenuRequested, this, &HideableColumnsTable::header_menu);
}

HideableColumnsTable::~HideableColumnsTable()
{
	delete menu;
}

void HideableColumnsTable::setModel(QAbstractItemModel* m)
{
	QTreeView::setModel(m);

	if ( const int n = m->columnCount() )
	{
		QMenu* t = new QMenu(this);

		for ( int i = 0; i < n; ++i )
		{
			QAction* a = t->addAction(m->headerData(i, Qt::Horizontal).toString(), this, [this, i]() {
				toggle_column(i);
			});
			a->setCheckable(true);
			a->setChecked(!isColumnHidden(i) );
		}

		QMenu* u = menu;
		menu = t;
		delete u;
	}
}

void HideableColumnsTable::header_menu(const QPoint& posn)
{
	if ( menu )
	{
		QPoint gpos = mapToGlobal(posn);

		menu->exec(gpos);
	}
}

void HideableColumnsTable::toggle_column(int c)
{
	setColumnHidden(c, !isColumnHidden(c) );
}
