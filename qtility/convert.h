#ifndef QUTILITY_CONVERT_H
#define QUTILITY_CONVERT_H

#include <vector>
#include <string>

#include <QStringList>

namespace qutility
{
QStringList convert(const std::vector< std::string >& v)
{
	QStringList l;

	for ( std::size_t i = 0, n = v.size(); i < n; ++i )
	{
		l << QString::fromStdString(v[i]);
	}

	return l;
}

}

#endif // QUTILITY_CONVERT_H
