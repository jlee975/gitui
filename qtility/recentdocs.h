#ifndef RECENTDOCS_H
#define RECENTDOCS_H

#include <QObject>
#include <QStringList>
class QAction;
class QMenu;

class QSettings;

class RecentDocs
	: public QObject
{
	Q_OBJECT
public:
	RecentDocs();
	void load(const QStringList&);
	QStringList getRecentDocs() const;
	void rebuild(QMenu*, QAction*);
	bool update(const QString&);
signals:
	void open_file(const QString&);
private slots:
	void open_recent(int);
private:
	QStringList       recentdocs;
	QList< QAction* > recentdoc_actions;
};

#endif // RECENTDOCS_H
