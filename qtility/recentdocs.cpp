#include "recentdocs.h"

#include <QSettings>
#include <QMenu>
#include <QAction>

namespace
{
void shrink(
	QStringList& l,
	int          n
)
{
	for ( int i = l.count(); i > n; --i )
	{
		l.pop_back();
	}
}

}

RecentDocs::RecentDocs()
{
}

void RecentDocs::load(const QStringList& recentdocs_)
{
	recentdocs = recentdocs_;
	recentdocs.removeDuplicates();
	shrink(recentdocs, 9);
}

QStringList RecentDocs::getRecentDocs() const
{
	return recentdocs;
}

void RecentDocs::rebuild(
	QMenu*   menu,
	QAction* before
)
{
	// remove existing actions
	for ( int i = 0, n = recentdoc_actions.count(); i < n; ++i )
	{
		menu->removeAction(recentdoc_actions.at(i) );
	}

	recentdoc_actions.clear();

	if ( !recentdocs.empty() )
	{
		// create new ones
		QAction* sep = menu->insertSeparator(before);
		recentdoc_actions.push_back(sep);

		for ( int i = 0; i < recentdocs.count(); ++i )
		{
			QAction* act = new QAction(recentdocs.at(i), this);
			recentdoc_actions.push_back(act);
			act->setShortcut(Qt::CTRL + ( Qt::Key_1 + i ) );
			menu->insertAction(sep, act);
			connect(act, &QAction::triggered, this, [this, i]() { open_recent(i); });
		}
	}
}

bool RecentDocs::update(const QString& filename)
{
	if ( recentdocs.isEmpty() || recentdocs.front() != filename )
	{
		recentdocs.removeAll(filename);
		recentdocs.push_front(filename);
		shrink(recentdocs, 9);
		return true;
	}

	return false;
}

void RecentDocs::open_recent(int i)
{
	if ( i < recentdocs.count() )
	{
		emit open_file(recentdocs.at(i) );
	}
}
