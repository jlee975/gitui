#ifndef GRAPHMODEL_H
#define GRAPHMODEL_H

#include <vector>
#include <QList>
#include <QAbstractItemModel>

#include "delegates/graphdelegateitem.h"

class GraphModel
	: public QAbstractItemModel
{
public:
	explicit GraphModel(QObject* = nullptr);

	bool is_graph_column(int) const;
protected:
	void set_graph(std::vector< QList< int > >, std::vector< QList< int > >);
	GraphDelegateItem getGraphItem(int) const;
private:
	virtual bool is_graph_column_(int) const;

	std::vector< QList< int > > leaving;
	std::vector< QList< int > > nodes;
};

#endif // GRAPHMODEL_H
