#include "graphmodel.h"

GraphModel::GraphModel(QObject* parent_)
	: QAbstractItemModel(parent_)
{

}

bool GraphModel::is_graph_column(int c) const
{
	return is_graph_column_(c);
}

bool GraphModel::is_graph_column_(int c) const
{
	return false;
}

void GraphModel::set_graph(
	std::vector< QList< int > > leaving_,
	std::vector< QList< int > > nodes_
)
{
	leaving = std::move(leaving_);
	nodes   = std::move(nodes_);
}

GraphDelegateItem GraphModel::getGraphItem(int r) const
{
	GraphDelegateItem x;

	// incoming colours to this row are the outgoing colours of the previous row
	if ( r > 0 && static_cast< unsigned >( r - 1 ) < leaving.size() )
	{
		x.in << leaving[r - 1];
	}

	// outgoing colours
	if ( r >= 0 && static_cast< unsigned >( r ) < leaving.size() )
	{
		x.out << leaving[r];
	}

	// list of colours that begin/end on this row
	if ( r >= 0 && static_cast< unsigned >( r ) < nodes.size() )
	{
		x.here << nodes[r];
	}

	return x;
}
