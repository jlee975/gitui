#include "patchviewerinner.h"

#include <iostream>
#include <sstream>

#include <QPainter>
#include <QPaintEvent>

namespace
{
std::size_t width(int x)
{
	std::ostringstream ss;

	ss << x;
	return ss.str().length();
}

}

PatchViewerInner::PatchViewerInner(QWidget* parent)
	: QWidget(parent)
{
	setFont(QFont("Courier New") );
}

void PatchViewerInner::setDiff(const Diff& diff)
{
	std::vector< line_info > lines_;
	const QFontMetrics&      fm = fontMetrics();

	for ( std::size_t i = 0; i < diff.deltas.size(); ++i )
	{
		if ( i != 0 )
		{
			lines_.push_back(line_info{ "------------------------------------------------\n", {} });
		}

		const Delta& delta = diff.deltas[i];

		lines_.push_back(line_info{ delta.old_name + "\n", {} });
		lines_.push_back(line_info{ delta.new_name + "\n", {} });

		for ( const auto& hunk : delta.hunks )
		{
			lines_.push_back(line_info{ hunk.header, {} });
			lines_.push_back(line_info{ "\n" });

			std::size_t wo = 0;
			std::size_t wn = 0;

			for ( const auto& line : hunk.lines )
			{
				if ( line.old_lineno >= 0 )
				{
					wo = std::max(wo, ::width(line.old_lineno) );
				}

				if ( line.new_lineno >= 0 )
				{
					wn = std::max(wn, ::width(line.new_lineno) );
				}
			}

			for ( const auto& line : hunk.lines )
			{
				const QString s1 = line.old_lineno >= 0 ? QString::number(line.old_lineno) : QString();
				const QString s2 = line.new_lineno >= 0 ? QString::number(line.new_lineno) : QString();
				const QString s  = line.origin + QString(" ") + s1.rightJustified(wo) + " " + s2.rightJustified(wn) +
				                   " " + line.content;

				QColor c;

				if ( line.old_lineno < 0 )
				{
					c = 0xaaffaa;
				}
				else if ( line.new_lineno < 0 )
				{
					c = 0xffaaaa;
				}

				lines_.push_back(line_info{ s, c });
			}
		}
	}

	lines.swap(lines_);
	QString text;

	for ( const auto& l : lines )
	{
		text += l.content;
	}

	QRect rect = fm.boundingRect(0, 0, INT_MAX, INT_MAX, Qt::AlignLeft | Qt::AlignTop, text);

	setMinimumSize(rect.width(), rect.height() );
	update();
}

void PatchViewerInner::paintEvent(QPaintEvent*)
{
	QPainter painter(this);

	#if 0
	painter.drawText(0, 0, rect.width(), rect.height(), Qt::AlignTop | Qt::AlignLeft, text);
	#else
	const int    h  = fontMetrics().lineSpacing();
	const int    w  = width();
	const QBrush bg = painter.background();

	for ( std::size_t i = 0, n = lines.size(); i < n; ++i )
	{
		painter.fillRect(0, i * h, w, h, lines[i].background.isValid() ? lines[i].background : bg);
		painter.drawText(0, i * h, w, h, Qt::AlignTop | Qt::AlignLeft, lines[i].content);
	}

	#endif
}
