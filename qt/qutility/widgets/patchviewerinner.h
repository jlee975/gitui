#ifndef PATCHVIEWERINNER_H
#define PATCHVIEWERINNER_H

#include <QWidget>

#include "qutility/patch.h"

class PatchViewerInner
	: public QWidget
{
	Q_OBJECT
public:
	explicit PatchViewerInner(QWidget* parent = nullptr);
	void setDiff(const Diff&);
private:
	struct line_info
	{
		QString content;
		QColor background;
	};

	void paintEvent(QPaintEvent*) override;

	std::vector< line_info > lines;
};

#endif // PATCHVIEWERINNER_H
