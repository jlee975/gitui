#ifndef PATCHVIEWER_H
#define PATCHVIEWER_H

#include <QWidget>

#include "qutility/patch.h"

namespace Ui
{
class PatchViewer;
}

class PatchViewer
	: public QWidget
{
	Q_OBJECT
public:
	explicit PatchViewer(QWidget* parent = nullptr);
	~PatchViewer();

	void clear();
	void setDiff(const Diff&);
private:
	Ui::PatchViewer* ui;
};

#endif // PATCHVIEWER_H
