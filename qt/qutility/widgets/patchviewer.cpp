#include "patchviewer.h"
#include "ui_patchviewer.h"

#include <QScrollBar>

PatchViewer::PatchViewer(QWidget* parent)
	: QWidget(parent),
	ui(new Ui::PatchViewer)
{
	ui->setupUi(this);
	ui->scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
}

PatchViewer::~PatchViewer()
{
	delete ui;
}

void PatchViewer::clear()
{
	ui->scrollAreaWidgetContents->setDiff(Diff() );
}

void PatchViewer::setDiff(const Diff& diff_)
{
	ui->scrollAreaWidgetContents->setDiff(diff_);
}
