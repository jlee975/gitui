#ifndef PATCH_H
#define PATCH_H

#include <vector>
#include <QString>

struct Line
{
	char origin;
	int old_lineno;
	int new_lineno;
	QString content;
};

struct Hunk
{
	QString header;
	int old_start;
	int old_lines;
	int new_start;
	int new_lines;
	std::vector< Line > lines;
};

struct Delta
{
	bool added;
	bool removed;
	QString old_name;
	QString new_name;
	std::vector< Hunk > hunks;
};

struct Diff
{
	std::vector< Delta > deltas;
};

#endif // PATCH_H
