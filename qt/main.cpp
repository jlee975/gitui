#include "mainwindow.h"
#include <QApplication>
#include <QDir>

#include <git2.h>

int main(
	int   argc,
	char* argv[]
)
{
	const QString app = argv[0];

	git_libgit2_init();

	QApplication a(argc, argv);

	const QString dir  = QDir::currentPath();
	QString       path = ( argc > 1 ? argv[1] : QString() );

	MainWindow w(app, dir, path);
	w.show();

	const int res = a.exec();

	git_libgit2_shutdown();

	return res;
}
