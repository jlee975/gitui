#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <ctime>

#include <QCloseEvent>
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QToolButton>
#include <QSettings>
#include <QProcess>

#include <git2.h>

#include "repository.h"

#include "forms/commitdialog.h"
#include "forms/reflogform.h"
#include "dialogs/globalconfigurationdialog.h"
#include "dialogs/preferencesdialog.h"
#include "dialogs/repopropertiesdialog.h"
#include "dialogs/createtagdialog.h"
#include "dialogs/rebasedialog.h"
#include "dialogs/clonedialog.h"
#include "dialogs/addremotedialog.h"
#include "dialogs/selectcommitdialog.h"
#include "forms/blameform.h"
#include "forms/commitviewer.h"
#include "forms/overviewform.h"
#include "forms/filehistory.h"
#include "settings.h"

MainWindow::MainWindow(
	QString  apppath_,
	QString  dir_,
	QString  path,
	QWidget* parent
)
	: QMainWindow(parent), ui(new Ui::MainWindow), apppath(std::move(apppath_) ), dir(std::move(dir_) ), overview(
		nullptr)
{
	ui->setupUi(this);

	recentdocs.load(global_settings().recentDocs() );
	connect(&recentdocs, &RecentDocs::open_file, this, &MainWindow::open);
	rebuild_recent_doc_actions();

	open(path.isEmpty() ? dir : path);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::closeEvent(QCloseEvent* event)
{
	close_all();
	event->accept();
}

void MainWindow::on_actionOpen_triggered()
{
	const QString dir_ = QFileDialog::getExistingDirectory(this, "Open Repository");

	open(dir_);
}

void MainWindow::on_actionShow_Branches_triggered(bool checked)
{
	if ( overview )
	{
		overview->show_branches(checked);
	}
}

void MainWindow::on_actionShow_Remotes_triggered(bool checked)
{
	if ( overview )
	{
		overview->show_remotes(checked);
	}
}

void MainWindow::on_actionShow_Tags_triggered(bool checked)
{
	if ( overview )
	{
		overview->show_tags(checked);
	}
}

void MainWindow::on_actionShow_Stashes_triggered(bool checked)
{
	if ( overview )
	{
		overview->show_stashes(checked);
	}
}

void MainWindow::on_actionShow_Status_triggered(bool checked)
{
	if ( overview )
	{
		overview->show_status(checked);
	}
}

void MainWindow::on_actionGlobal_Configuration_triggered()
{
	GlobalConfigurationDialog dlg(this);

	dlg.exec();
}

void MainWindow::open(const QString& dir_)
{
	if ( !dir_.isEmpty() )
	{
		if ( repo )
		{
			QProcess::startDetached(apppath, QStringList(dir_), dir);
		}
		else
		{
			auto p = std::make_shared< Repository >(dir_.toStdString() );

			if ( p->is_valid() )
			{
				repo     = std::move(p);
				overview = new OverviewForm(this);

				connect(overview, &OverviewForm::show_diff, this, &MainWindow::show_diff);
				connect(overview, &OverviewForm::file_history, this, &MainWindow::file_history);
				connect(overview, &OverviewForm::blame, this, &MainWindow::blame);
				connect(overview, &OverviewForm::commit_index, this, &MainWindow::commit_index);
				connect(overview, &OverviewForm::remove_from_index, this, &MainWindow::remove_from_index);
				connect(this, &MainWindow::settings_updated, overview, &OverviewForm::settings_updated);
				connect(overview, &OverviewForm::checkout_branch, this, &MainWindow::checkout_branch);
				connect(overview, &OverviewForm::checkout_tag, this, &MainWindow::checkout_tag);
				connect(overview, &OverviewForm::create_branch, this, &MainWindow::create_branch);
				connect(overview, &OverviewForm::rename_branch, this, &MainWindow::rename_branch);
				connect(overview, &OverviewForm::rebase_branch, this, &MainWindow::rebase_branch);
				connect(overview, &OverviewForm::reflog_branch, this, &MainWindow::reflog_branch);
				connect(overview, &OverviewForm::reflog_tag, this, &MainWindow::reflog_tag);
				connect(overview, &OverviewForm::reflog_head, this, &MainWindow::reflog_head);
				connect(overview, &OverviewForm::remove_branch, this, &MainWindow::remove_branch);
				connect(overview, &OverviewForm::remove_tag, this, &MainWindow::remove_tag);
				connect(overview, &OverviewForm::pop, this, &MainWindow::pop_stash);
				connect(overview, &OverviewForm::drop, this, &MainWindow::drop_stash);
				connect(overview, &OverviewForm::push_stash, this, &MainWindow::push_stash);
				connect(overview, &OverviewForm::add_remote, this, &MainWindow::add_remote);
				connect(overview, &OverviewForm::remove_remote, this, &MainWindow::remove_remote);
				connect(overview, &OverviewForm::rename_remote, this, &MainWindow::rename_remote);
				connect(overview, &OverviewForm::set_url, this, &MainWindow::set_url);
				connect(overview, &OverviewForm::create_tag, this, &MainWindow::create_tag);
				connect(overview, &OverviewForm::diff_choose, this, &MainWindow::diff_choose);
				connect(overview, &OverviewForm::relocate_branch, this, &MainWindow::relocate_branch);
				connect(overview, &OverviewForm::relocate_tag, this, &MainWindow::relocate_tag);
				connect(overview, &OverviewForm::fetch_remote, this, &MainWindow::fetch_remote);

				overview->open(repo);

				ui->tabWidget->addTab(overview, dir_);
				file_opened(dir_);
			}
		}
	}
}

void MainWindow::show_diff(
	commit_id i,
	commit_id j,
	int       x
)
{
	CommitViewer* w = ( j != INVALID_COMMIT ? new CommitViewer(this, repo, i, j) : new CommitViewer(this, repo, i, x) );

	connect(this, &MainWindow::settings_updated, w, &CommitViewer::settings_updated);
	const int idx = ui->tabWidget->addTab(w, "Diff");
	ui->tabWidget->setCurrentIndex(idx);
}

void MainWindow::file_history(QStringList l)
{
	if ( repo )
	{
		l.removeDuplicates();

		std::vector< std::string > v;

		for ( const QString& s : l )
		{
			v.push_back(s.toStdString() );
		}

		auto w = new FileHistory(repo, repo->reduce(v), l, this);
		connect(w, &FileHistory::blame, this, &MainWindow::blame);

		const int idx = ui->tabWidget->addTab(w, "File History");

		ui->tabWidget->setCurrentIndex(idx);
	}
}

void MainWindow::on_actionPreferences_triggered()
{
	PreferencesDialog dlg(this);

	if ( dlg.exec() == QDialog::Accepted )
	{
		emit settings_updated();
	}
}

void MainWindow::close_all()
{
	// close all widgets
	while ( ui->tabWidget->count() != 0 )
	{
		QWidget* wi = ui->tabWidget->widget(0);
		ui->tabWidget->removeTab(0);
		delete wi;
	}

	overview = nullptr;
	repo.reset();
}

void MainWindow::on_tabWidget_tabCloseRequested(int idx)
{
	if ( idx != -1 )
	{
		QWidget* w = ui->tabWidget->widget(idx);

		if ( w == overview )
		{
			close_all();
		}
		else
		{
			ui->tabWidget->removeTab(ui->tabWidget->indexOf(w) );
			delete w;
		}
	}
}

void MainWindow::on_action_repo_Directory_triggered()
{
	if ( repo )
	{
		RepoPropertiesDialog dlg(repo, global_settings().hashDisplayLength(), this);

		dlg.exec();
	}
}

void MainWindow::on_actionRefresh_triggered()
{
	refresh();
}

void MainWindow::refresh()
{
	if ( repo )
	{
		repo->refresh();
		overview->refresh();
	}
}

void MainWindow::rebuild_recent_doc_actions()
{
	recentdocs.rebuild(ui->menuFile, ui->actionExit);
}

void MainWindow::file_opened(const QString& filename)
{
	if ( recentdocs.update(filename) )
	{
		global_settings().recentDocs(recentdocs.getRecentDocs() );
		rebuild_recent_doc_actions();
	}
}

void MainWindow::on_actionExit_triggered()
{
	close();
}

void MainWindow::on_actionClone_triggered()
{
	CloneDialog dlg;

	if ( dlg.exec() == QDialog::Accepted )
	{
		const std::string s = dlg.getSource().toStdString();
		const std::string d = dlg.getDest().toStdString();

		git_repository* repo_ = nullptr;

		if ( git_clone(&repo_, s.c_str(), d.c_str(), nullptr) == 0 )
		{
			git_repository_free(repo_);

			open(dlg.getDest() );
		}
	}
}

void MainWindow::on_actionCommit_Index_triggered()
{
	if ( overview )
	{
		overview->commit_index();
	}
}

void MainWindow::on_actionPath_History_triggered()
{
	if ( repo )
	{
		const QString root = QString::fromStdString(repo->path() );
		QString       s    = QFileDialog::getOpenFileName(this, "Select File", root);

		if ( !s.isEmpty() )
		{
			s.remove(0, root.length() + 1);
			file_history(QStringList(s) );
		}
	}
}

void MainWindow::on_actionPath_History_Manual_triggered()
{
	if ( repo )
	{
		QString s = QInputDialog::getText(this, "Enter path", "Enter path relative to root directory of repository");

		if ( !s.isEmpty() )
		{
			file_history(QStringList(s) );
		}
	}
}

void MainWindow::on_actionBlame_triggered()
{
	if ( repo )
	{
		QString dir_ = QString::fromStdString(repo->path() );

		if ( !dir_.isEmpty() )
		{
			if ( !dir_.endsWith('/') )
			{
				dir_ += '/';
			}

			QString file = QFileDialog::getOpenFileName(this, "Select File", dir_);

			if ( !file.isEmpty() )
			{
				if ( file.startsWith(dir_) )
				{
					file.remove(0, dir_.length() );
				}

				blame(QStringList(file) );
			}
		}
	}
}

void MainWindow::blame(const QStringList& l)
{
	const int i = ui->tabWidget->addTab(new BlameForm(repo, l, this), "Blame " + l.join(',') );

	ui->tabWidget->setCurrentIndex(i);
}

void MainWindow::commit_index()
{
	if ( repo )
	{
		CommitDialog dlg;

		if ( dlg.exec() == QDialog::Accepted )
		{
			/// @todo If index is empty, probably don't want to do anything
			if ( repo->commit_index(dlg.message().toStdString() ) )
			{
				/// @todo Probably don't need to refresh the whole thing
				refresh();
			}
			else
			{
				QMessageBox::critical(this, "Repository error", "Could not commit");
			}
		}
	}
}

void MainWindow::remove_from_index(std::vector< status_id > ids)
{
	if ( repo )
	{
		if ( repo->remove_from_index(ids) )
		{
			/// @todo Only need to refresh things interested in status
			refresh();
		}
		else
		{
			QMessageBox::critical(this, "Repository error", "Could not remove file from index");
		}
	}
}

void MainWindow::checkout_branch(branch_id b)
{
	if ( !repo )
	{
		return;
	}

	if ( repo->checkout(b) )
	{
		refresh();
	}
	else
	{
		QMessageBox::critical(this, "Repository error", "Checkout failed");
	}
}

void MainWindow::checkout_tag(tag_id t)
{
	if ( !repo )
	{
		return;
	}

	if ( repo->checkout(t) )
	{
		refresh();
	}
	else
	{
		QMessageBox::critical(this, "Repository error", "Checkout failed");
	}
}

void MainWindow::create_branch(commit_id c)
{
	if ( !repo )
	{
		return;
	}

	bool          ok = false;
	const QString s  = QInputDialog::getText(this, "Create branch", "Please enter a new branch name",
		QLineEdit::Normal, QString(), &ok);

	if ( !ok )
	{
		return;
	}

	if ( repo->create_branch_from_commit(c, s.toStdString() ) != INVALID_BRANCH )
	{
		refresh();
	}
	else
	{
		QMessageBox::critical(this, "Repository error", "Branch could not be created");
	}
}

void MainWindow::rename_branch(branch_id b)
{
	if ( !repo )
	{
		return;
	}

	bool          ok = false;
	const QString s  = QInputDialog::getText(this, "Rename branch", "Please enter a new branch name",
		QLineEdit::Normal, QString::fromStdString(repo->branch(b).name), &ok);

	if ( ok )
	{
		if ( repo->rename_branch(b, s.toStdString() ) )
		{
			refresh();
		}
		else
		{
			QMessageBox::critical(this, "Repository error", "Branch could not be renamed");
		}
	}
}

void MainWindow::reflog_branch(branch_id b)
{
	if ( repo )
	{
		const auto i = ui->tabWidget->addTab(new ReflogForm(repo, QString::fromStdString(repo->reference(
			b) ), this), QString::fromStdString(repo->branch(b).name) + " Reflog");
		ui->tabWidget->setCurrentIndex(i);
	}
}

void MainWindow::reflog_tag(tag_id t)
{
	if ( repo )
	{
		const auto i = ui->tabWidget->addTab(new ReflogForm(repo, QString::fromStdString(repo->reference(
			t) ), this), QString::fromStdString(repo->tag(t).name) + " Reflog");
		ui->tabWidget->setCurrentIndex(i);
	}
}

void MainWindow::reflog_head()
{
	if ( repo )
	{
		const auto i = ui->tabWidget->addTab(new ReflogForm(repo, "HEAD", this), "HEAD Reflog");
		ui->tabWidget->setCurrentIndex(i);
	}
}

void MainWindow::rebase_branch(branch_id b)
{
	if ( !repo )
	{
		return;
	}

	/// @todo Do not allow rebasing onto ancestors (esp. self)
	RebaseDialog dlg(repo, QString::fromStdString(repo->branch(b).name), this);

	if ( dlg.exec() == QDialog::Accepted )
	{
		const branch_id                  tip  = dlg.tip();
		const hash_type                  onto = dlg.onto();
		const std::optional< hash_type > from = dlg.from();

		if ( repo->rebase(onto, tip, from, dlg.recursive() ) )
		{
			refresh();
		}
		else
		{
			QMessageBox::critical(this, "Rebase failed",
				QString::fromStdString("Could not rebase " + repo->branch(tip).name + " onto " +
					onto.to_string() ) );
		}
	}
}

void MainWindow::remove_branch(branch_id b)
{
	if ( !repo )
	{
		return;
	}

	if ( QMessageBox::question(this, "Delete branch",
		"Are you sure you want to delete the branch '" + QString::fromStdString(repo->branch(b).name) + "'?") ==
	     QMessageBox::Yes )
	{
		if ( repo->remove_branch(b) )
		{
			refresh();
		}
		else
		{
			QMessageBox::critical(this, "Repository error", "Branch could not be deleted");
		}
	}
}

void MainWindow::remove_tag(tag_id t)
{
	if ( !repo )
	{
		return;
	}

	if ( QMessageBox::question(this, "Delete tag",
		"Are you sure you want to delete the tag '" + QString::fromStdString(repo->tag(t).name) + "'?",
		QMessageBox::Yes,
		QMessageBox::No) == QMessageBox::Yes )
	{
		if ( repo->remove_tag(t) )
		{
			refresh();
		}
		else
		{
			QMessageBox::critical(this, "Repository error", "Tag could not be deleted");
		}
	}
}

void MainWindow::pop_stash(stash_id i)
{
	if ( !repo )
	{
		return;
	}

	if ( repo->pop_stash(i) )
	{
		refresh();
	}
	else
	{
		QMessageBox::critical(this, "Repository error", "Pop did not succeed");
	}
}

void MainWindow::drop_stash(stash_id i)
{
	if ( !repo )
	{
		return;
	}

	if ( repo->drop_stash(i) )
	{
		refresh();
	}
	else
	{
		QMessageBox::critical(this, "Repository error", "Drop did not succeed");
	}
}

void MainWindow::push_stash()
{
	if ( !repo )
	{
		return;
	}

	if ( repo->push_stash() )
	{
		refresh();
	}
	else
	{
		QMessageBox::critical(this, "Repository error", "Push did not succeed");
	}
}

void MainWindow::add_remote()
{
	if ( !repo )
	{
		return;
	}

	AddRemoteDialog dlg;

	if ( dlg.exec() == QDialog::Accepted )
	{
		if ( repo->add_remote(dlg.url().toStdString(), dlg.name().toStdString() ) != INVALID_REMOTE )
		{
			refresh();
		}
		else
		{
			QMessageBox::critical(this, "Repository error", "Could not create remote");
		}
	}
}

void MainWindow::remove_remote(
	remote_id   r,
	std::size_t b
)
{
	if ( !repo )
	{
		return;
	}

	if ( QMessageBox::question(this, "Delete branch",
		"Are you sure you want to delete the branch '" + QString::fromStdString(repo->remote(r).full_name(b) ) +
		"'?") == QMessageBox::Yes )
	{
		if ( repo->remove_remote_branch(r, b) )
		{
			refresh();
		}
		else
		{
			QMessageBox::critical(this, "Repository error", "Remote branch could not be deleted");
		}
	}
}

void MainWindow::rename_remote(remote_id r)
{
	if ( !repo )
	{
		return;
	}

	bool          ok = false;
	const QString s  = QInputDialog::getText(this, "Rename remote", "Please enter a new remote name",
		QLineEdit::Normal, QString::fromStdString(repo->remote(r).name), &ok);

	if ( ok )
	{
		if ( repo->rename_remote(r, s.toStdString() ) )
		{
			refresh();
		}
		else
		{
			QMessageBox::critical(this, "Repository error", "Remote could not be renamed");
		}
	}
}

void MainWindow::fetch_remote(remote_id r)
{
	if ( !repo )
	{
		return;
	}

	if ( repo->fetch_remote(r) )
	{
		refresh();
	}
	else
	{
		QMessageBox::critical(this, "Repository error", "Fetch failed");
	}
}

void MainWindow::set_url(remote_id r)
{
	if ( !repo )
	{
		return;
	}

	bool          ok = false;
	const QString s  = QInputDialog::getText(this, "Set URL for remote",
		"Please enter a new URL for the remote", QLineEdit::Normal,
		QString::fromStdString(repo->remote(r).url), &ok);

	if ( ok )
	{
		if ( repo->set_url_remote(r, s.toStdString() ) )
		{
			refresh();
		}
		else
		{
			QMessageBox::critical(this, "Repository error", "Remote could not set URL");
		}
	}
}

void MainWindow::diff_choose(commit_id i)
{
	if ( !repo )
	{
		return;
	}

	SelectCommitDialog dlg(repo, this);

	if ( dlg.exec() == QDialog::Accepted )
	{
		/// @todo Could probably modify dlg.hash to return commit instead
		show_diff(i, dlg.get_commit(), 0);
	}
}

void MainWindow::create_tag(commit_id i)
{
	CreateTagDialog dlg(this);

	if ( dlg.exec() == QDialog::Accepted )
	{
		const QString msg = dlg.message();

		if ( msg.isEmpty() )
		{
			if ( repo->create_tag_from_commit(i, dlg.tag_name().toStdString() ) )
			{
				refresh();
				return;
			}
		}
		else
		{
			if ( repo->create_tag_from_commit(i, dlg.tag_name().toStdString(), msg.toStdString() ) )
			{
				refresh();
				return;
			}
		}

		QMessageBox::critical(this, "Repository error", "Tag could not be created");
	}
}

void MainWindow::relocate_branch(
	branch_id b,
	commit_id c
)
{
	if ( !repo )
	{
		return;
	}

	if ( QMessageBox::question(this, "Move " + QString::fromStdString(repo->branch(b).name),
		"Are you sure?") == QMessageBox::Yes )
	{
		if ( repo->move_branch_to_commit(b, c) )
		{
			refresh();
		}
		else
		{
			QMessageBox::critical(this, "Repository error", "Could not move branch");
		}
	}
}

void MainWindow::relocate_tag(
	tag_id    t,
	commit_id c
)
{
	if ( !repo )
	{
		return;
	}

	if ( QMessageBox::question(this, "Move " + QString::fromStdString(repo->tag(t).name),
		"Are you sure?") == QMessageBox::Yes )
	{
		if ( repo->move_tag_to_commit(t, c) )
		{
			refresh();
		}
		else
		{
			QMessageBox::critical(this, "Repository error", "Could not move tag");
		}
	}
}

void MainWindow::on_actionFind_triggered()
{
	if ( overview )
	{
		overview->find_message();
	}
}
