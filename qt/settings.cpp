#include "settings.h"

#include <QColor>

const char recent_docs_key[]  = "recent";
const char short_hashes_key[] = "hashlen";
const char colour_list_key[]  = "colour_list";

QRgb default_branch_colours[] =
{
	0x212121,
	0xFF0000,
	0x3F51B5,
	0x4CAF50,
	0x9C27B0,
	0xe57373,
	0x2196F3,
	0xffa000,
	0xFF00FF,
	0x0000FF,
	0x795548
};

GitUISettings& global_settings()
{
	static GitUISettings settings;

	return settings;
}

GitUISettings::GitUISettings()
	: settings("Gitui", "Gitui")
{
}

QStringList GitUISettings::recentDocs() const
{
	return settings.value(recent_docs_key).toStringList();
}

void GitUISettings::recentDocs(const QStringList& recentdocs)
{
	settings.setValue(recent_docs_key, recentdocs);
}

QVariantList GitUISettings::defaultColourList() const
{
	QVariantList default_colour_list;

	for ( std::size_t i = 0; i < sizeof( default_branch_colours ) / sizeof( default_branch_colours[0] ); ++i )
	{
		default_colour_list << QColor(default_branch_colours[i]);
	}

	return default_colour_list;
}

QVariantList GitUISettings::getColourList() const
{
	/// @todo Only calculate if needed
	return settings.value(colour_list_key, defaultColourList() ).toList();
}

void GitUISettings::setColourList(const QVariantList& l)
{
	settings.setValue(colour_list_key, l);
}

int GitUISettings::hashDisplayLength() const
{
	QVariant short_hashes = settings.value(short_hashes_key, 40);

	return short_hashes.toInt();

}

void GitUISettings::hashDisplayLength(int value)
{
	settings.setValue(short_hashes_key, value);
}
