#ifndef REFLOGFORM_H
#define REFLOGFORM_H

#include <memory>
#include <QWidget>
#include "repository.h"

namespace Ui
{
class ReflogForm;
}

class ReflogForm
	: public QWidget
{
	Q_OBJECT
public:
	ReflogForm(std::shared_ptr< Repository >, const QString&, QWidget* parent = nullptr);
	~ReflogForm();
private:
	Ui::ReflogForm* ui;

	std::shared_ptr< Repository > repo;
};

#endif // REFLOGFORM_H
