#include "filehistory.h"
#include "ui_filehistory.h"

#include "repository.h"

#include "models/logmodel.h"
#include "forms/blameform.h"

#include "settings.h"

/// @todo Only show commits involving the specified files. Be careful of branches.
FileHistory::FileHistory(
	std::shared_ptr< Repository > repo_,
	reduction                     v,
	QStringList                   l,
	QWidget*                      parent
)
	: QWidget(parent),
	ui(new Ui::FileHistory), repo(repo_)
{
	ui->setupUi(this);
	ui->comboBox->addItems(l);
	model = new LogModel(this);
	model->setRepository(repo, std::move(v) );
	ui->commits->setModel(model);
	connect(ui->commits->selectionModel(), &QItemSelectionModel::currentChanged, this, &FileHistory::commit_selected);
}

FileHistory::~FileHistory()
{
	delete ui;
}

void FileHistory::settings_updated()
{
	model->settings_updated();
}

void FileHistory::commit_selected(const QModelIndex& index)
{
	const std::size_t hashlen = global_settings().hashDisplayLength();

	ui->hash->setText(QString::fromStdString(model->get_hash(index).to_string(hashlen) ) );

	ui->diffwith->clear();

	for ( const auto& h : model->get_diff_parents(index) )
	{
		ui->diffwith->addItem(QString::fromStdString(h.to_string(hashlen) ), QVariant::fromValue(h) );
	}

	ui->diffwith->insertSeparator(ui->diffwith->count() );

	if ( const commit_id h = repo->head_commit();h != INVALID_COMMIT )
	{
		ui->diffwith->addItem("HEAD", QVariant::fromValue(repo->commit(h).hashxxx) );
	}

	ui->diffwith->addItem("Index", -1);
	ui->diffwith->addItem("Workdir", -2);
	bool needs_separator = true;

	for ( const branch_id bi : repo->branch_ids() )
	{
		const branch_type& b = repo->branch(bi);

		if ( needs_separator )
		{
			ui->diffwith->insertSeparator(ui->diffwith->count() );
			needs_separator = false;
		}

		ui->diffwith->addItem(QString::fromStdString(b.name), QVariant::fromValue(b.hash) );
	}

	needs_separator = true;

	for ( const remote_id ri : repo->remote_ids() )
	{
		const remote_type& r = repo->remote(ri);

		for ( std::size_t i = 0; i < r.branches.size(); ++i )
		{
			if ( needs_separator )
			{
				ui->diffwith->insertSeparator(ui->diffwith->count() );
				needs_separator = false;
			}

			ui->diffwith->addItem(QString::fromStdString(r.full_name(i) ), QVariant::fromValue(r.branches[i].hash) );
		}
	}

	needs_separator = true;

	for ( const tag_id ti : repo->tag_ids() )
	{
		const tag_type& t = repo->tag(ti);

		if ( needs_separator )
		{
			ui->diffwith->insertSeparator(ui->diffwith->count() );
			needs_separator = false;
		}

		ui->diffwith->addItem(QString::fromStdString(t.name), QVariant::fromValue(t.hash) );
	}

	ui->diffwith->setCurrentIndex(-1);

}

void FileHistory::on_diffwith_currentIndexChanged(const QString&)
{
	view_patch(ui->comboBox->currentText() );
}

void FileHistory::view_patch(const QString& s)
{
	if ( ui->diffwith->currentIndex() == -1 )
	{
		ui->patch->clear();
	}
	else
	{
		const int r0 = ui->commits->selectionModel()->currentIndex().row();

		if ( r0 != -1 )
		{
			const QVariant v = ui->diffwith->currentData();

			Diff d0;

			if ( v.type() == QVariant::Int )
			{
				switch ( v.toInt() )
				{
				case -1:
					d0 = model->get_diff_with_index(r0);
					break;
				case -2:
					d0 = model->get_diff_with_workdir(r0);
					break;
				}
			}
			else
			{
				const hash_type h  = v.value< hash_type >();
				const int       r1 = model->find_hash(h);

				if ( r1 != -1 )
				{
					d0 = model->get_diff(r0, r1);
				}
			}

			Diff d1;

			for ( const auto& delta : d0.deltas )
			{
				if ( delta.new_name == s || delta.old_name == s )
				{
					d1.deltas.push_back(delta);
				}
			}

			ui->patch->setDiff(d1);
		}
	}
}

void FileHistory::on_comboBox_currentIndexChanged(const QString& arg1)
{
	view_patch(arg1);
}

void FileHistory::on_blame_clicked()
{
	emit blame(QStringList(ui->comboBox->currentText() ) );
}

void FileHistory::on_go_clicked()
{
	if ( repo )
	{
		const QVariant v = ui->diffwith->currentData();

		if ( v.type() != QVariant::Int )
		{
			const commit_id c = repo->hash_to_commit(v.value< hash_type >() );

			if ( c != INVALID_COMMIT )
			{
				ui->commits->setCurrentIndex(model->index(model->find_commit(c), 0) );
			}
		}
	}
}
