#ifndef LOGVIEW_H
#define LOGVIEW_H

#include "widgets/graphview.h"

class LogView
	: public GraphView
{
	Q_OBJECT
public:
	explicit LogView(QWidget*);
	void settings_updated();
};

#endif // LOGVIEW_H
