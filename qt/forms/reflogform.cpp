#include "reflogform.h"
#include "ui_reflogform.h"

#include "models/reflogmodel.h"

ReflogForm::ReflogForm(
	std::shared_ptr< Repository > repo_,
	const QString&                ref,
	QWidget*                      parent
)
	: QWidget(parent),
	ui(new Ui::ReflogForm), repo(std::move(repo_) )
{
	ui->setupUi(this);

	ui->treeView->setModel(new ReflogModel(repo, ref, this) );
}

ReflogForm::~ReflogForm()
{
	delete ui;
}
