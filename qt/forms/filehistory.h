#ifndef FILEHISTORY_H
#define FILEHISTORY_H

#include <memory>

#include <QWidget>
class QListWidgetItem;

#include "qrepotypes.h"

class Repository;
class LogModel;

namespace Ui
{
class FileHistory;
}

class FileHistory
	: public QWidget
{
	Q_OBJECT
public:
	FileHistory(std::shared_ptr< Repository >, reduction, QStringList, QWidget* parent = nullptr);
	~FileHistory();
signals:
	void blame(const QStringList&);
private slots:
	void settings_updated();

	void commit_selected(const QModelIndex&);

	void on_diffwith_currentIndexChanged(const QString& arg1);

	void on_comboBox_currentIndexChanged(const QString& arg1);
	void on_blame_clicked();

	void on_go_clicked();
private:
	void view_patch(const QString&);
	Ui::FileHistory*              ui;
	std::shared_ptr< Repository > repo;
	LogModel*                     model;
};

#endif // FILEHISTORY_H
