#ifndef HISTORYVIEWER_H
#define HISTORYVIEWER_H

#include <memory>
#include <QWidget>

class QSettings;
class QListWidgetItem;
class QMenu;

#include "qrepotypes.h"

class LogModel;
class Repository;

namespace Ui
{
class HistoryViewer;
}

class HistoryViewer
	: public QWidget
{
	Q_OBJECT
public:
	explicit HistoryViewer(QWidget* parent = nullptr);
	~HistoryViewer();
	void setData(std::shared_ptr< Repository >);
	void settings_updated();
	void set_current_commit(commit_id);
public slots:
	void find_message();
signals:
	void branches_changed();
	void tags_changed();
	void diff_with_index(commit_id);
	void diff_with_workdir(commit_id);
	void diff_commit(commit_id, commit_id);
    void diff_choose(commit_id);
    void file_history(QStringList);
	void blame(QStringList);
	void create_branch(commit_id);
	void create_tag(commit_id);
	void remove_tag(tag_id);
	void checkout_branch(branch_id);
	void rename_branch(branch_id);
	void remove_branch(branch_id);
	void rebase_branch(branch_id);
	void reflog_branch(branch_id);
	void reflog_head();
	void reflog_tag(tag_id);
	void checkout_tag(tag_id);
	void relocate_branch(branch_id, commit_id);
	void relocate_tag(tag_id, commit_id);
private slots:
    void choose_diff();
	void commit_selected(const QModelIndex& index);
	void on_commits_customContextMenuRequested(const QPoint& pos);
	void on_actionBranch_here_triggered();
	void on_actionTag_here_triggered();
	void move_branch(branch_id);
	void move_tag(tag_id);
	void diff_index();
	void diff_workdir();
	void diff_selected(commit_id);
	void on_diffwith_currentIndexChanged(int);
	void on_parents_itemDoubleClicked(QListWidgetItem* item);
	void on_choosediff_clicked();
	void on_go_clicked();
	void on_copyhash_clicked();
	void on_next_match_clicked();

	void on_close_find_clicked();

	void on_previous_match_clicked();

	void on_needle_returnPressed();

	void on_copycommitter_clicked();

	void on_copyauthor_clicked();

	void on_copydate_clicked();

	void on_copymessage_clicked();

	void on_copyparents_clicked();

	void on_copycommitteremail_clicked();

	void on_copyauthoremail_clicked();
private:
	commit_id current_commit() const;

	Ui::HistoryViewer*            ui;
	LogModel*                     logmodel;
	std::shared_ptr< Repository > repo_;
};

#endif // HISTORYVIEWER_H
