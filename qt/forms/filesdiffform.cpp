#include "filesdiffform.h"
#include "ui_filesdiffform.h"

#include "repository.h"

FilesDiffForm::FilesDiffForm(QWidget* parent)
	: QWidget(parent), ui(new Ui::FilesDiffForm)
{
	ui->setupUi(this);
	model = new DiffModel(this);
	ui->files->setModel(model);
	connect(
		ui->files->selectionModel(), &QItemSelectionModel::currentChanged, this, &FilesDiffForm::current_file_changed);
	menu = new QMenu(this);
	menu->addAction("Path History", this, &FilesDiffForm::file_history);
	menu->addAction("Blame", this, &FilesDiffForm::blame_file);
}

FilesDiffForm::~FilesDiffForm()
{
	delete ui;
}

void FilesDiffForm::current_file_changed(
	const QModelIndex& idx,
	const QModelIndex&
)
{
	ui->before->setDiff(model->getDiff(idx) );
}

void FilesDiffForm::clear()
{
	setDiff(Diff() );
}

void FilesDiffForm::setDiff(Diff diff_)
{
	ui->before->clear();
	model->setDiff(std::move(diff_) );
	ui->files->expandAll();
	ui->files->resizeColumnToContents(0);
}

void FilesDiffForm::on_files_customContextMenuRequested(const QPoint& posn)
{
	const QModelIndex idx = ui->files->indexAt(posn);

	if ( idx.isValid() )
	{
		QPoint gpos = ui->files->mapToGlobal(posn);
		menu->exec(gpos);
	}
}

void FilesDiffForm::file_history()
{
	QModelIndexList l = ui->files->selectionModel()->selectedIndexes();
	emit            show_file_history(model->files(l) );
}

void FilesDiffForm::blame_file()
{
	QModelIndexList l = ui->files->selectionModel()->selectedIndexes();
	emit            blame(model->files(l) );
}
