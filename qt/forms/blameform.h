#ifndef BLAMEFORM_H
#define BLAMEFORM_H

#include <memory>
#include <QWidget>
#include "repository.h"

namespace Ui
{
class BlameForm;
}

class BlameForm
	: public QWidget
{
	Q_OBJECT
public:
	BlameForm(std::shared_ptr< Repository >, const QStringList&, QWidget* parent = nullptr);
	~BlameForm() override;
private slots:
	void on_comboBox_currentIndexChanged(int index);
private:
	struct BlameInfo
	{
		std::size_t line;
		QString commit;
		QString name;
		QString email;
		QString content;
	};

	Ui::BlameForm* ui;

	std::shared_ptr< Repository >           repo;
	std::vector< std::vector< BlameInfo > > blameinfo;
};

#endif // BLAMEFORM_H
