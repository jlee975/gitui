#ifndef FILESDIFFFORM_H
#define FILESDIFFFORM_H

#include <QWidget>
#include <QTreeWidgetItem>
#include <QMenu>

#include "models/diffmodel.h"

namespace Ui
{
class FilesDiffForm;
}

class FilesDiffForm
	: public QWidget
{
	Q_OBJECT
public:
	explicit FilesDiffForm(QWidget* parent = nullptr);
	~FilesDiffForm();

	void clear();
	void setDiff(Diff);
signals:
	void show_file_history(QStringList);
	void blame(QStringList);
private slots:
	void current_file_changed(const QModelIndex&, const QModelIndex&);
	void on_files_customContextMenuRequested(const QPoint& pos);
	void file_history();
	void blame_file();
private:
	void visit(Diff&, QTreeWidgetItem*);

	Ui::FilesDiffForm* ui;
	DiffModel*         model;
	QMenu*             menu;
};

#endif // FILESDIFFFORM_H
