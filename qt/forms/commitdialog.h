#ifndef COMMITDIALOG_H
#define COMMITDIALOG_H

#include <QDialog>

namespace Ui
{
class CommitDialog;
}

class CommitDialog
	: public QDialog
{
	Q_OBJECT
public:
	explicit CommitDialog(QWidget* parent = nullptr);
	~CommitDialog();
	QString message() const;
private slots:
	void on_pushButton_clicked();
private:
	Ui::CommitDialog* ui;
};

#endif // COMMITDIALOG_H
