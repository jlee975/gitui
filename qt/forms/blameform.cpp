#include "blameform.h"
#include "ui_blameform.h"

/// @bug Does not support more than one file
BlameForm::BlameForm(
	std::shared_ptr< Repository > repo_,
	const QStringList&            files,
	QWidget*                      parent_
)
	: QWidget(parent_),
	ui(new Ui::BlameForm), repo(std::move(repo_) )
{
	ui->setupUi(this);

	if ( repo && !files.isEmpty() )
	{
		for ( int i = 0; i < files.count(); ++i )
		{
			const auto& file = files.at(i);

			/// @todo Since the refactoring, the intermediate variable "lines" and the "BlameInfo" are no longer necessary
			std::vector< BlameInfo > lines;

			for ( const blame_type& x : repo->blame(file.toStdString() ) )
			{
				lines.push_back({ x.lineno, QString::fromStdString(x.commit), QString::fromStdString(
					repo->contributor(x.author).name), QString::fromStdString(repo->contributor(
									  x.author).email), QString::fromStdString(x.line) });
			}

			blameinfo.push_back(std::move(lines) );
		}

		ui->comboBox->addItems(files);
	}
}

BlameForm::~BlameForm()
{
	delete ui;
}

void BlameForm::on_comboBox_currentIndexChanged(int index)
{
	if ( index >= 0 && static_cast< unsigned >( index ) < blameinfo.size() )
	{
		QString s;

		for ( const auto& b : blameinfo[index] )
		{
			s += QString::number(b.line) + ":" + b.commit + ":" + b.name + ":" + b.email + ":" + b.content + "\n";
		}

		ui->plainTextEdit->setPlainText(s);
	}
	else
	{
		ui->plainTextEdit->setPlainText(QString() );
	}
}
