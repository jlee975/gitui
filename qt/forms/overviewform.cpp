#include "overviewform.h"
#include "ui_overviewform.h"

#include <QMenu>
#include <QInputDialog>
#include <QMessageBox>
#include <QFileDialog>

#include "models/branchesmodel.h"
#include "models/remotesmodel.h"
#include "models/tagsmodel.h"
#include "models/stashesmodel.h"
#include "models/statusmodel.h"

#include "repository.h"
#include "forms/commitviewer.h"
#include "settings.h"
#include "dialogs/rebasedialog.h"
#include "dialogs/addremotedialog.h"
#include "forms/commitdialog.h"
#include "forms/filehistory.h"

OverviewForm::OverviewForm(QWidget* parent)
	: QWidget(parent),
        ui(new Ui::OverviewForm), repo(nullptr)
{
	ui->setupUi(this);
	ui->history->settings_updated();

	connect(ui->history, &HistoryViewer::branches_changed, this, &OverviewForm::branches_changed);
	connect(ui->history, &HistoryViewer::tags_changed, this, &OverviewForm::tags_changed);
	connect(ui->history, &HistoryViewer::diff_commit, this, &OverviewForm::diff_commit);
	connect(ui->history, &HistoryViewer::diff_with_index, this, &OverviewForm::diff_index);
	connect(ui->history, &HistoryViewer::diff_with_workdir, this, &OverviewForm::diff_workdir);
	connect(ui->history, &HistoryViewer::file_history, this, &OverviewForm::file_history);
	connect(ui->history, &HistoryViewer::blame, this, &OverviewForm::blame);
	connect(ui->history, &HistoryViewer::create_branch, this, &OverviewForm::create_branch);
	connect(ui->history, &HistoryViewer::checkout_branch, this, &OverviewForm::checkout_branch);
    connect(ui->history, &HistoryViewer::diff_choose, this, &OverviewForm::diff_choose);
    connect(ui->history, &HistoryViewer::create_tag, this, &OverviewForm::create_tag);
	connect(ui->history, &HistoryViewer::remove_tag, this, &OverviewForm::remove_tag);
	connect(ui->history, &HistoryViewer::rebase_branch, this, &OverviewForm::rebase_branch);
	connect(ui->history, &HistoryViewer::remove_branch, this, &OverviewForm::remove_branch);
	connect(ui->history, &HistoryViewer::rename_branch, this, &OverviewForm::rename_branch);
	connect(ui->history, &HistoryViewer::relocate_branch, this, &OverviewForm::relocate_branch);
	connect(ui->history, &HistoryViewer::reflog_branch, this, &OverviewForm::reflog_branch);
	connect(ui->history, &HistoryViewer::relocate_tag, this, &OverviewForm::relocate_tag);
	connect(ui->history, &HistoryViewer::reflog_tag, this, &OverviewForm::reflog_tag);
	connect(ui->history, &HistoryViewer::checkout_tag, this, &OverviewForm::checkout_tag);
	connect(ui->history, &HistoryViewer::reflog_head, this, &OverviewForm::reflog_head);

	statusmenu = new QMenu("Status", this);

	remotesmenu = new QMenu("Remotes", this);

	tagsmenu = new QMenu("Tags", this);

	stashmenu = new QMenu("Stash", this);

	branchesmodel = new BranchesModel;
	remotesmodel  = new RemotesModel;
	tagsmodel     = new TagsModel;
	stashesmodel  = new StashesModel;
	statusmodel   = new StatusModel;

	ui->branches->setModel(branchesmodel);
	ui->remotes->setModel(remotesmodel);
	ui->tags->setModel(tagsmodel);
	ui->stashes->setModel(stashesmodel);
	ui->status->setModel(statusmodel);
}

OverviewForm::~OverviewForm()
{
	delete statusmodel;
	delete stashesmodel;
	delete tagsmodel;
	delete remotesmodel;
	delete branchesmodel;
	delete ui;
}

void OverviewForm::open(const std::shared_ptr< Repository >& repo_)
{
	if ( repo_ )
	{
		repo = repo_;
	}

	refresh();
}

void OverviewForm::refresh()
{
	ui->history->setData(repo);
	branchesmodel->setRepository(repo);
	remotesmodel->setRepository(repo);
	tagsmodel->setRepository(repo);
	stashesmodel->setRepository(repo);
	statusmodel->setRepository(repo);
	ui->remotes->expandAll();
}

void OverviewForm::show_branches(bool checked)
{
	ui->branches->setHidden(!checked);
}

void OverviewForm::show_remotes(bool checked)
{
	ui->remotes->setHidden(!checked);

}

void OverviewForm::show_tags(bool checked)
{
	ui->tags->setHidden(!checked);

}

void OverviewForm::show_stashes(bool checked)
{
	ui->stashes->setHidden(!checked);

}

void OverviewForm::show_status(bool checked)
{
	ui->status->setHidden(!checked);
}

void OverviewForm::settings_updated()
{
	ui->history->settings_updated();
	remotesmodel->settings_updated();
	tagsmodel->settings_updated();
	branchesmodel->settings_updated();
}

void OverviewForm::index_add()
{
	if ( repo )
	{
		QModelIndexList l = ui->status->selectionModel()->selectedIndexes();

		if ( !statusmodel->add_to_index(l) )
		{
			QMessageBox::critical(this, "Repository error", "Could not add file to index");
		}
	}
}

void OverviewForm::index_remove()
{
	if ( repo )
	{
		QModelIndexList l = ui->status->selectionModel()->selectedIndexes();

		std::unordered_set< status_id > statuses;

		for ( const auto& i : l )
		{
			statuses.insert(statusmodel->id(i) );
		}

		emit remove_from_index(std::vector< status_id >{ statuses.begin(), statuses.end() });
	}
}

/// @bug If checkout succeeds the log window should be updated with the new HEAD
void OverviewForm::branches_checkout()
{
	if ( repo )
	{
		QModelIndexList l = ui->branches->selectionModel()->selectedIndexes();

		if ( !l.isEmpty() )
		{
			emit checkout_branch(branchesmodel->get_branch(l.front() ) );
		}
	}
}

void OverviewForm::branches_create()
{
	if ( repo )
	{
		QModelIndexList l = ui->branches->selectionModel()->selectedIndexes();

		if ( !l.isEmpty() )
		{
			emit create_branch(repo->branch_to_commit(branchesmodel->get_branch(l.front() ) ) );
		}
	}
}

void OverviewForm::branches_rename()
{
	if ( repo )
	{
		QModelIndexList l = ui->branches->selectionModel()->selectedIndexes();

		if ( !l.isEmpty() )
		{
			emit rename_branch(branchesmodel->get_branch(l.front() ) );
		}
	}
}

void OverviewForm::branches_rebase()
{
	if ( repo )
	{
		QModelIndexList l = ui->branches->selectionModel()->selectedIndexes();

		if ( !l.isEmpty() )
		{
			emit rebase_branch(branchesmodel->get_branch(l.front() ) );
		}
	}
}

void OverviewForm::branches_reflog()
{
	if ( repo )
	{
		QModelIndexList l = ui->branches->selectionModel()->selectedIndexes();

		if ( !l.isEmpty() )
		{
			emit reflog_branch(branchesmodel->get_branch(l.front() ) );
		}
	}
}

void OverviewForm::branches_delete()
{
	if ( repo )
	{
		QModelIndexList l = ui->branches->selectionModel()->selectedIndexes();

		if ( !l.isEmpty() )
		{
			emit remove_branch(branchesmodel->get_branch(l.front() ) );
		}
	}
}

void OverviewForm::tags_checkout()
{
	if ( repo )
	{
		QModelIndexList l = ui->tags->selectionModel()->selectedIndexes();

		if ( !l.isEmpty() )
		{
			emit checkout_tag(tagsmodel->get_tag(l.front() ) );
		}
	}
}

void OverviewForm::tags_delete()
{
	if ( repo )
	{
		QModelIndexList l = ui->tags->selectionModel()->selectedIndexes();

		if ( !l.isEmpty() )
		{
			emit remove_tag(tagsmodel->get_tag(l.front() ) );
		}
	}
}

void OverviewForm::tags_reflog()
{
	if ( repo )
	{
		QModelIndexList l = ui->tags->selectionModel()->selectedIndexes();

		if ( !l.isEmpty() )
		{
			emit reflog_tag(tagsmodel->get_tag(l.front() ) );
		}
	}
}

void OverviewForm::pop_stash()
{
	if ( repo )
	{
		QModelIndexList l = ui->stashes->selectionModel()->selectedIndexes();

		if ( !l.isEmpty() )
		{
			emit pop(stashesmodel->get_stash(l.front() ) );
		}
	}
}

void OverviewForm::drop_stash()
{
	if ( repo )
	{
		QModelIndexList l = ui->stashes->selectionModel()->selectedIndexes();

		if ( !l.isEmpty() )
		{
			emit drop(stashesmodel->get_stash(l.front() ) );
		}
	}
}

void OverviewForm::on_status_customContextMenuRequested(const QPoint& posn)
{
	const QModelIndexList l = ui->status->selectionModel()->selectedIndexes();

	statusmenu->clear();

	bool empty = true;

	if ( !repo->is_index_empty() )
	{
		statusmenu->addAction("Commit Index", this, &OverviewForm::commit_index);
		empty = false;
	}

	if ( statusmodel->not_in_index(l) )
	{
		statusmenu->addAction("Add to Index", this, &OverviewForm::index_add);
		empty = false;
	}

	if ( statusmodel->is_in_index(l) )
	{
		statusmenu->addAction("Remove from Index", this, &OverviewForm::index_remove);
		empty = false;
	}

	if ( !empty )
	{
		statusmenu->addSeparator();
	}

	statusmenu->addAction(ui->actionFile_History);
	statusmenu->addAction(ui->actionShow_Ignored_Files);
	statusmenu->addAction(ui->actionStash);
	QPoint gpos = ui->status->mapToGlobal(posn);
	statusmenu->exec(gpos);
}

void OverviewForm::on_branches_customContextMenuRequested(const QPoint& posn)
{
	const QModelIndex idx = ui->branches->indexAt(posn);

	if ( idx.isValid() )
	{
		QMenu branchesmenu("Branch");
		branchesmenu.clear();
        branchesmenu.addAction("Branch here", this, &OverviewForm::branches_create);
        branchesmenu.addSeparator();
        branchesmenu.addAction("Checkout", this, &OverviewForm::branches_checkout);
		branchesmenu.addAction("Rename", this, &OverviewForm::branches_rename);
		branchesmenu.addAction("Delete", this, &OverviewForm::branches_delete);
		branchesmenu.addAction("Rebase", this, &OverviewForm::branches_rebase);
		branchesmenu.addAction("Reflog", this, &OverviewForm::branches_reflog);
        QPoint gpos = ui->branches->mapToGlobal(posn);
		branchesmenu.exec(gpos);
	}
}

void OverviewForm::on_remotes_customContextMenuRequested(const QPoint& posn)
{
	const QModelIndex idx = ui->remotes->indexAt(posn);

	remotesmenu->clear();

	if ( idx.isValid() )
	{
		if ( !idx.parent().isValid() )
		{
			remotesmenu->addAction("Rename", this, &OverviewForm::remote_rename);
			remotesmenu->addAction("Change URL", this, &OverviewForm::remote_set_url);
			remotesmenu->addAction("Fetch", this, &OverviewForm::remote_fetch);
		}
		else
		{
			remotesmenu->addAction("Delete", this, &OverviewForm::remote_delete);
		}

		remotesmenu->addSeparator();
	}

	remotesmenu->addAction("Add Remote", this, &OverviewForm::remote_add);
	QPoint gpos = ui->remotes->mapToGlobal(posn);
	remotesmenu->exec(gpos);
}

void OverviewForm::on_tags_customContextMenuRequested(const QPoint& posn)
{
	const QModelIndex idx = ui->tags->indexAt(posn);

	if ( idx.isValid() )
	{
		QPoint gpos = ui->tags->mapToGlobal(posn);
		tagsmenu->clear();
		tagsmenu->addAction("Checkout", this, &OverviewForm::tags_checkout);
		tagsmenu->addAction("Delete", this, &OverviewForm::tags_delete);
		tagsmenu->addAction("Reflog", this, &OverviewForm::tags_reflog);
		tagsmenu->exec(gpos);
	}
}

void OverviewForm::on_stashes_customContextMenuRequested(const QPoint& posn)
{
	const QModelIndex idx = ui->stashes->indexAt(posn);

	if ( idx.isValid() )
	{
		QPoint gpos = ui->stashes->mapToGlobal(posn);
		stashmenu->clear();
		stashmenu->addAction("Pop", this, &OverviewForm::pop_stash);
		stashmenu->addAction("Drop", this, &OverviewForm::drop_stash);
		stashmenu->exec(gpos);
	}
}

void OverviewForm::branches_changed()
{
	branchesmodel->refresh();
}

void OverviewForm::tags_changed()
{
	tagsmodel->refresh();
}

void OverviewForm::diff_index(commit_id i)
{
	emit show_diff(i, INVALID_COMMIT, -1);
}

void OverviewForm::diff_workdir(commit_id i)
{
	emit show_diff(i, INVALID_COMMIT, -2);
}

void OverviewForm::diff_commit(
	commit_id i,
	commit_id j
)
{
	emit show_diff(i, j, 0);
}

void OverviewForm::on_status_doubleClicked(const QModelIndex& index)
{
	QStringList l;

	if ( index.isValid() )
	{
		l << index.sibling(index.row(), StatusModel::PATH).data().toString();
	}

	emit file_history(l);
}

void OverviewForm::on_branches_doubleClicked(const QModelIndex& index)
{
	const commit_id c = repo->branch_to_commit(branchesmodel->get_branch(index) );

	ui->history->set_current_commit(c);
}

void OverviewForm::on_tags_doubleClicked(const QModelIndex& index)
{
	const commit_id c = repo->tag_to_commit(tagsmodel->get_tag(index) );

	ui->history->set_current_commit(c);
}

void OverviewForm::on_remotes_doubleClicked(const QModelIndex& index)
{
	const commit_id c = remotesmodel->get_commit(index);

	ui->history->set_current_commit(c);
}

void OverviewForm::on_stashes_doubleClicked(const QModelIndex &index)
{
    const auto s = stashesmodel->get_stash(index);

    ui->history->set_current_commit(repo->stash_to_commit(s));
}

void OverviewForm::remote_add()
{
	emit add_remote();
}

void OverviewForm::remote_delete()
{
	if ( repo )
	{
		QModelIndexList l = ui->remotes->selectionModel()->selectedIndexes();

		if ( !l.isEmpty() )
		{
			const auto idx = l.front();

			if ( idx.parent().isValid() )
			{
				emit remove_remote(remotesmodel->get_remote(idx), idx.row() );
			}
		}
	}
}

void OverviewForm::remote_rename()
{
	if ( repo )
	{
		QModelIndexList l = ui->remotes->selectionModel()->selectedIndexes();

		if ( !l.isEmpty() )
		{
			emit rename_remote(remotesmodel->get_remote(l.front() ) );
		}
	}
}

void OverviewForm::remote_fetch()
{
	QModelIndexList l = ui->remotes->selectionModel()->selectedIndexes();

	if ( !l.isEmpty() )
	{
		emit fetch_remote(remotesmodel->get_remote(l.front() ) );
	}
}

void OverviewForm::remote_set_url()
{
	if ( repo )
	{
		QModelIndexList l = ui->remotes->selectionModel()->selectedIndexes();

		if ( !l.isEmpty() )
		{
			emit set_url(remotesmodel->get_remote(l.front() ) );
		}
	}
}

void OverviewForm::on_actionShow_Ignored_Files_toggled(bool arg1)
{
	statusmodel->set_show_ignored_files(arg1);
}

void OverviewForm::on_actionFile_History_triggered()
{
	QStringList l;

	for ( const QModelIndex& i : ui->status->selectionModel()->selectedIndexes() )
	{
		l << i.sibling(i.row(), StatusModel::PATH).data().toString();
	}

	emit file_history(l);
}

void OverviewForm::on_actionStash_triggered()
{
	emit push_stash();
}

void OverviewForm::find_message()
{
	ui->history->find_message();
}
