#include "commitdialog.h"
#include "ui_commitdialog.h"

#include <QMessageBox>

CommitDialog::CommitDialog(QWidget* parent)
	: QDialog(parent),
	ui(new Ui::CommitDialog)
{
	ui->setupUi(this);
}

CommitDialog::~CommitDialog()
{
	delete ui;
}

QString CommitDialog::message() const
{
	return ui->message->toPlainText();
}

void CommitDialog::on_pushButton_clicked()
{
	QString s = ui->message->toPlainText();

	if ( !s.isEmpty() ||
	     QMessageBox::question(this, "Empty commit message",
			 "Are you sure you don't want to have a commit message?") == QMessageBox::Yes )
	{
		accept();
	}
}
