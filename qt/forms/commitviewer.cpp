#include "commitviewer.h"
#include "ui_commitviewer.h"

#include "repository.h"
#include "models/logmodel.h"

CommitViewer::CommitViewer(QWidget* parent)
	: QWidget(parent), ui(new Ui::CommitViewer)
{
	ui->setupUi(this);
	ui->commits->settings_updated();

	LogModel* model = new LogModel(this);
	ui->commits->setModel(model);
}

CommitViewer::CommitViewer(
	QWidget*                      parent,
	std::shared_ptr< Repository > repo_,
	commit_id                     c_,
	int                           p_
)
	: CommitViewer(parent)
{
	LogModel* model = static_cast< LogModel* >( ui->commits->model() );

	if ( p_ == -1 )
	{
		// Diff with index
		model->setRepository(repo_);

		ui->diff->setDiff(model->get_diff_with_index(c_) );
	}
	else if ( p_ == -2 )
	{
		// Diff with index
		model->setRepository(repo_);

		ui->diff->setDiff(model->get_diff_with_workdir(c_) );
	}
}

CommitViewer::CommitViewer(
	QWidget*                      parent,
	std::shared_ptr< Repository > repo_,
	commit_id                     c_,
	commit_id                     p_
)
	: CommitViewer(parent)
{
	LogModel* model = static_cast< LogModel* >( ui->commits->model() );

	// Diff between commits
	model->setRepository(repo_, repo_->reduce(c_, p_) );

	/// @todo model->find_commit(c_)
	const int c = model->find_hash(repo_->commit(c_).hashxxx);
	const int p = model->find_hash(repo_->commit(p_).hashxxx);
	model->setHighlight(c, Qt::red);
	model->setHighlight(p, Qt::green);

	ui->diff->setDiff(model->get_diff(c, p) );
}

CommitViewer::~CommitViewer()
{
	delete ui;
}

void CommitViewer::settings_updated()
{
	ui->commits->settings_updated();
}
