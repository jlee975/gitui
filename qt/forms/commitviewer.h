#ifndef COMMITVIEWER_H
#define COMMITVIEWER_H

#include <memory>

#include <QWidget>

class QSettings;

#include "qrepotypes.h"

class Repository;

namespace Ui
{
class CommitViewer;
}

class CommitViewer
	: public QWidget
{
	Q_OBJECT
public:
	/// @todo Last arg should be an enum
	explicit CommitViewer(QWidget* parent);
	CommitViewer(QWidget* parent, std::shared_ptr< Repository >, commit_id, int);
	CommitViewer(QWidget* parent, std::shared_ptr< Repository >, commit_id, commit_id);
	~CommitViewer();
public slots:
	void settings_updated();
private:
	Ui::CommitViewer* ui;
};

#endif // COMMITVIEWER_H
