#include "historyviewer.h"
#include "ui_historyviewer.h"

#include <QClipboard>
#include <QDebug>
#include <QMenu>
#include <QInputDialog>
#include <QMessageBox>
#include <QSignalMapper>
#include <QListWidgetItem>

#include "repository.h"

#include "dialogs/selectcommitdialog.h"
#include "dialogs/createtagdialog.h"
#include "dialogs/rebasedialog.h"
#include "models/logmodel.h"
#include "settings.h"

HistoryViewer::HistoryViewer(QWidget* parent)
	: QWidget(parent),
        ui(new Ui::HistoryViewer), repo_(nullptr)
{
	ui->setupUi(this);
	ui->widget->hide();

	qRegisterMetaType< commit_id >();

	logmodel = new LogModel(this);
	ui->commits->setModel(logmodel);
	connect(ui->commits->selectionModel(), &QItemSelectionModel::currentChanged, this, &HistoryViewer::commit_selected);
	connect(ui->diff, &FilesDiffForm::show_file_history, this, &HistoryViewer::file_history);
	connect(ui->diff, &FilesDiffForm::blame, this, &HistoryViewer::blame);
}

HistoryViewer::~HistoryViewer()
{
	delete ui;
}

void HistoryViewer::setData(std::shared_ptr< Repository > repo)
{
	repo_ = repo;
	logmodel->setRepository(repo_);
}

void HistoryViewer::settings_updated()
{
	ui->commits->settings_updated();
	logmodel->settings_updated();
}

void HistoryViewer::set_current_commit(commit_id c)
{
	ui->commits->setCurrentIndex(logmodel->index(logmodel->find_commit(c), 0) );
}

void HistoryViewer::find_message()
{
	ui->widget->show();
	ui->needle->setFocus();
}

void HistoryViewer::commit_selected(const QModelIndex& index)
{
	const std::size_t hashlen = global_settings().hashDisplayLength();

	const commit_id ci = logmodel->get_commit(index);

	ui->parents->clear();
	ui->diffwith->clear();

	for ( const auto pi : repo_->parents(ci) )
	{
		const auto&      h = repo_->commit(pi).hashxxx;
		const QString    s = QString::fromStdString(h.to_string(hashlen) );
		const QVariant   v = QVariant::fromValue(h);
		QListWidgetItem* i = new QListWidgetItem(s);
		i->setData(Qt::UserRole, v);
		ui->parents->addItem(i);

		ui->diffwith->addItem(s, v);
	}

	ui->diffwith->insertSeparator(ui->diffwith->count() );

	if ( const commit_id h = repo_->head_commit();h != INVALID_COMMIT && h != ci )
	{
		ui->diffwith->addItem("HEAD", QVariant::fromValue(repo_->commit(h).hashxxx) );
	}

	ui->diffwith->addItem("Index", -1);
	ui->diffwith->addItem("Workdir", -2);
	bool needs_separator = true;

	for ( const branch_id bi : repo_->branch_ids() )
	{
		const branch_type& b = repo_->branch(bi);

		if ( repo_->branch_to_commit(bi) != ci )
		{
			if ( needs_separator )
			{
				ui->diffwith->insertSeparator(ui->diffwith->count() );
				needs_separator = false;
			}

			ui->diffwith->addItem(QString::fromStdString(b.name), QVariant::fromValue(b.hash) );
		}
	}

	needs_separator = true;

	for ( const remote_id ri : repo_->remote_ids() )
	{
		const remote_type& r = repo_->remote(ri);

		for ( std::size_t i = 0; i < r.branches.size(); ++i )
		{
			if ( repo_->remote_branch_to_commit(ri, i) != ci )
			{
				if ( needs_separator )
				{
					ui->diffwith->insertSeparator(ui->diffwith->count() );
					needs_separator = false;
				}

				ui->diffwith->addItem(QString::fromStdString(r.full_name(i) ),
					QVariant::fromValue(r.branches[i].hash) );
			}
		}
	}

	needs_separator = true;

	for ( const tag_id ti : repo_->tag_ids() )
	{
		const tag_type& t = repo_->tag(ti);

		if ( repo_->tag_to_commit(ti) != ci )
		{
			if ( needs_separator )
			{
				ui->diffwith->insertSeparator(ui->diffwith->count() );
				needs_separator = false;
			}

			ui->diffwith->addItem(QString::fromStdString(t.name), QVariant::fromValue(t.hash) );
		}
	}

	ui->diffwith->setCurrentIndex(-1);
	ui->message->setPlainText(QString::fromStdString(repo_->commit(ci).message) );
	ui->committer->setText(QString::fromStdString(repo_->contributor(repo_->commit(ci).committer).name) );
	ui->committer_email->setText(QString::fromStdString(repo_->contributor(repo_->commit(ci).committer).email) );
	ui->author->setText(QString::fromStdString(repo_->contributor(repo_->commit(ci).author).name) );
	ui->author_email->setText(QString::fromStdString(repo_->contributor(repo_->commit(ci).author).email) );
	ui->hash->setText(QString::fromStdString(repo_->commit(ci).hashxxx.to_string() ) );
	QDateTime dt;
	dt.setTime_t(repo_->commit(ci).actual_time);
	ui->date->setText(dt.toString(Qt::ISODate) );
}

void HistoryViewer::on_commits_customContextMenuRequested(const QPoint& posn)
{
	const QModelIndex idx = ui->commits->indexAt(posn);

	if ( idx.isValid() )
	{
		QPoint gpos = ui->commits->mapToGlobal(posn);

		QMenu commitsmenu;

		// diff with parents
		const commit_id ci = logmodel->get_commit(idx);

		std::vector< branch_id > sorted_branches = repo_->branch_ids();
		std::sort(sorted_branches.begin(), sorted_branches.end(), [this](branch_id l, branch_id r) {
			return repo_->branch(l).name < repo_->branch(r).name;
		});

		const std::size_t     nbranch     = sorted_branches.size();
		std::vector< tag_id > sorted_tags = repo_->tag_ids();
		std::sort(sorted_tags.begin(), sorted_tags.end(), [this](tag_id l, tag_id r) {
			return repo_->tag(l).name < repo_->tag(r).name;
		});

		const std::size_t              ntag       = sorted_tags.size();
		const std::vector< remote_id > remote_ids = repo_->remote_ids();

		{
			QMenu* diffwith = commitsmenu.addMenu("Diff with");

			// diff with parents
			const auto        parents = repo_->parents(ci);
			const std::size_t nparent = parents.size();

			if ( nparent > 0 )
			{
				if ( nparent == 1 )
				{
					const auto x = parents.at(0);
					diffwith->addAction("Parent", this, [this, x]() { diff_selected(x); });
				}
				else
				{
					for ( std::size_t i = 0; i < nparent; ++i )
					{
						const auto x = parents.at(i);
						diffwith->addAction("Parent (" + QString::fromStdString(repo_->commit(x).hashxxx.to_string(
							8) ) + ")", this, [this, x]() { diff_selected(x); });
					}
				}
			}

			diffwith->addAction("Workdir", this, &HistoryViewer::diff_workdir);
			diffwith->addAction("Index", this, &HistoryViewer::diff_index);

			{
				const auto x = repo_->head_commit();

				if ( x != ci )
				{
					diffwith->addAction("HEAD", this, [this, x]() { diff_selected(x); });
				}
			}

            diffwith->addAction("Choose...", this, &HistoryViewer::choose_diff);

			bool needs_separator = true;

			// diff with branches
			for ( const auto b : sorted_branches )
			{
				const auto x = repo_->branch_to_commit(b);

				if ( x != ci )
				{
					if ( needs_separator )
					{
						diffwith->addSeparator();
						needs_separator = false;
					}

					diffwith->addAction(QString::fromStdString(repo_->branch(b).name), this, [this, x]() {
						diff_selected(x);
					});
				}
			}

			needs_separator = true;

			for ( const auto i : remote_ids )
			{
				const QString     r = QString::fromStdString(repo_->remote(i).name);
				const std::size_t n = repo_->remote(i).branches.size();

				for ( std::size_t j = 0; j < n; ++j )
				{
					const QString b = QString::fromStdString(repo_->remote(i).branches[j].name);
					const QString z = r + "/" + b;

					const commit_id x = repo_->hash_to_commit(repo_->remote(i).branches[j].hash);

					if ( x != ci )
					{
						if ( needs_separator )
						{
							diffwith->addSeparator();
							needs_separator = false;
						}

						diffwith->addAction(z, this, [this, x]() { diff_selected(x); });
					}
				}
			}

			needs_separator = true;

			for ( const auto i : sorted_tags )
			{
				const auto x = repo_->tag_to_commit(i);

				if ( x != ci )
				{
					if ( needs_separator )
					{
						diffwith->addSeparator();
						needs_separator = false;
					}

					const QString b = QString::fromStdString(repo_->tag(i).name);

					diffwith->addAction(b, this, [this, x]() { diff_selected(x); });
				}
			}

			needs_separator = true;
		}

		if ( nbranch + ntag > repo_->branch_ids(ci).size() + repo_->tag_ids(ci).size() )
		{
			QMenu* movehere = commitsmenu.addMenu("Move here");

			bool needs_separator = false;

			// Menu to move branches here
			if ( nbranch > repo_->branch_ids(ci).size() )
			{
				for ( const auto b : sorted_branches )
				{
					if ( repo_->branch_to_commit(b) != ci )
					{
						if ( needs_separator )
						{
							movehere->addSeparator();
							needs_separator = false;
						}

						movehere->addAction(QString::fromStdString(repo_->branch(b).name), this, [this, b]() {
							move_branch(b);
						});
					}
				}

				needs_separator = true;
			}

			if ( ntag > repo_->tag_ids(ci).size() )
			{
				for ( const auto i : sorted_tags )
				{
					if ( repo_->tag_to_commit(i) != ci )
					{
						if ( needs_separator )
						{
							movehere->addSeparator();
							needs_separator = false;
						}

						const QString b = QString::fromStdString(repo_->tag(i).name);

						movehere->addAction(b, this, [this, i]() { move_tag(i); });
					}
				}

				needs_separator = true;
			}
		}

		if ( ci == repo_->head_commit() )
		{
			commitsmenu.addSeparator();
			commitsmenu.addAction("HEAD reflog", this, [this]() { emit reflog_head(); });
		}

		commitsmenu.addSeparator();

		// for any branches
		commitsmenu.addAction(ui->actionBranch_here);

		for ( const auto b : sorted_branches )
		{
			if ( repo_->branch_to_commit(b) == ci )
			{
				QMenu* branchmenu = commitsmenu.addMenu(QString::fromStdString(repo_->branch(b).name) );

				branchmenu->addAction("Checkout", this, [this, b]() { emit checkout_branch(b); });
				branchmenu->addAction("Rename", this, [this, b]() { emit rename_branch(b); });
				branchmenu->addAction("Delete", this, [this, b]() { emit remove_branch(b); });
				branchmenu->addAction("Rebase", this, [this, b]() { emit rebase_branch(b); });
				branchmenu->addAction("Reflog", this, [this, b]() { emit reflog_branch(b); });
			}
		}

		commitsmenu.addSeparator();

		// for any tags
		commitsmenu.addAction(ui->actionTag_here);

		// Menu to move tags here
		for ( const auto t : sorted_tags )
		{
			if ( repo_->tag_to_commit(t) == ci )
			{
				QMenu* tagsmenu = commitsmenu.addMenu(QString::fromStdString(repo_->tag(t).name) );
				tagsmenu->addAction("Checkout", this, [this, t]() { emit checkout_tag(t); });
				tagsmenu->addAction("Delete", this, [this, t]() { emit remove_tag(t); });
				tagsmenu->addAction("Reflog", this, [this, t]() { emit reflog_tag(t); });
			}
		}

		commitsmenu.exec(gpos);
	}
}

void HistoryViewer::diff_index()
{
	if ( repo_ )
	{
		QModelIndexList l = ui->commits->selectionModel()->selectedIndexes();

		if ( !l.isEmpty() )
		{
			emit diff_with_index(logmodel->get_commit(l.front() ) );
		}
	}
}

void HistoryViewer::diff_workdir()
{
	if ( repo_ )
	{
		QModelIndexList l = ui->commits->selectionModel()->selectedIndexes();

		if ( !l.isEmpty() )
		{
			emit diff_with_workdir(logmodel->get_commit(l.front() ) );
		}
	}
}

void HistoryViewer::on_actionBranch_here_triggered()
{
	QModelIndexList l = ui->commits->selectionModel()->selectedIndexes();

	if ( !l.isEmpty() )
	{
		emit create_branch(logmodel->get_commit(l.front() ) );
	}
}

void HistoryViewer::on_actionTag_here_triggered()
{
	QModelIndexList l = ui->commits->selectionModel()->selectedIndexes();

	if ( !l.isEmpty() )
	{
		emit create_tag(logmodel->get_commit(l.front() ) );
	}
}

void HistoryViewer::move_branch(branch_id b)
{
	QModelIndexList l = ui->commits->selectionModel()->selectedIndexes();

	if ( !l.isEmpty() )
	{
		emit relocate_branch(b, logmodel->get_commit(l.front() ) );
	}
}

void HistoryViewer::move_tag(tag_id t)
{
	QModelIndexList l = ui->commits->selectionModel()->selectedIndexes();

	if ( !l.isEmpty() )
	{
		emit relocate_tag(t, logmodel->get_commit(l.front() ) );
	}
}

void HistoryViewer::diff_selected(commit_id b)
{
	QModelIndexList l = ui->commits->selectionModel()->selectedIndexes();

	if ( !l.isEmpty() )
	{
		emit diff_commit(logmodel->get_commit(l.front() ), b);
	}
}

void HistoryViewer::on_diffwith_currentIndexChanged(int idx)
{
	if ( idx == -1 )
	{
		ui->diff->clear();
	}
	else
	{
		const int r0 = ui->commits->selectionModel()->currentIndex().row();

		if ( r0 != -1 )
		{
			const QVariant v = ui->diffwith->currentData();

			if ( v.type() == QVariant::Int )
			{
				switch ( v.toInt() )
				{
				case -1:
					ui->diff->setDiff(logmodel->get_diff_with_index(r0) );
					break;
				case -2:
					ui->diff->setDiff(logmodel->get_diff_with_workdir(r0) );
					break;
				}
			}
			else
			{
				const int r1 = logmodel->find_hash(v.value< hash_type >() );

				if ( r1 != -1 )
				{
					ui->diff->setDiff(logmodel->get_diff(r0, r1) );
				}
			}
		}
	}
}

void HistoryViewer::on_parents_itemDoubleClicked(QListWidgetItem* item)
{
	if ( item )
	{
		const int r = logmodel->find_hash(item->data(Qt::UserRole).value< hash_type >() );

		ui->commits->setCurrentIndex(logmodel->index(r, 0) );
	}
}

commit_id HistoryViewer::current_commit() const
{
	if ( repo_ )
	{
		QModelIndexList l = ui->commits->selectionModel()->selectedIndexes();

		if ( !l.isEmpty() )
		{
			const QModelIndex jdx = l.front();

			if ( jdx.isValid() )
			{
				return logmodel->row_to_commit(jdx.row() );
			}
		}
	}

	return INVALID_COMMIT;
}

void HistoryViewer::choose_diff()
{
    emit diff_choose(current_commit());
}


void HistoryViewer::on_choosediff_clicked()
{
	SelectCommitDialog dlg(repo_, this);

	if ( dlg.exec() == QDialog::Accepted )
	{
		const auto      c = dlg.get_commit();
		const hash_type h = repo_->commit(c).hashxxx;

		const int i = ui->diffwith->count();
		ui->diffwith->addItem(QString::fromStdString(h.to_string() ), QVariant::fromValue(h) );
		ui->diffwith->setCurrentIndex(i);
	}
}

void HistoryViewer::on_go_clicked()
{
	const QVariant v = ui->diffwith->currentData();

	if ( v.type() != QVariant::Int )
	{
		const commit_id i = repo_->hash_to_commit(v.value< hash_type >() );

		if ( i != INVALID_COMMIT )
		{
			set_current_commit(i);
		}
	}
}

void HistoryViewer::on_next_match_clicked()
{
	if ( repo_ && logmodel )
	{
		// get selected index
		QModelIndexList l = ui->commits->selectionModel()->selectedIndexes();

		const QModelIndex idx = l.isEmpty() ? logmodel->index(0, 0) : l.front();

		const QString     s0 = ui->needle->text();
		const std::string s  = s0.toStdString();

		if ( s.empty() )
		{
			return;
		}

		const int n = logmodel->rowCount();

		const int start = idx.row();

		for ( int j = start + 1; j <= start + n; ++j )
		{
			const auto jdx = idx.siblingAtRow(j % n);
			const auto c   = logmodel->get_commit(jdx);

			const std::string& msg = repo_->commit(c).message;

			if ( msg.find(s) != std::string::npos )
			{
				ui->commits->setCurrentIndex(jdx);
				return;
			}
		}

		QMessageBox::warning(this, "String not found", "No commit with the string '" + s0 + "'");
	}
}

void HistoryViewer::on_close_find_clicked()
{
	ui->widget->hide();
}

void HistoryViewer::on_previous_match_clicked()
{
	if ( repo_ && logmodel )
	{
		// get selected index
		QModelIndexList l = ui->commits->selectionModel()->selectedIndexes();

		const QModelIndex idx = l.isEmpty() ? logmodel->index(0, 0) : l.front();

		const QString     s0 = ui->needle->text();
		const std::string s  = s0.toStdString();

		if ( s.empty() )
		{
			return;
		}

		const int n = logmodel->rowCount();

		const int start = idx.row();

		for ( int j = start - 1; j >= start - n; --j )
		{
			const auto jdx = idx.siblingAtRow( ( j + n ) % n );
			const auto c   = logmodel->get_commit(jdx);

			const std::string& msg = repo_->commit(c).message;

			if ( msg.find(s) != std::string::npos )
			{
				ui->commits->setCurrentIndex(jdx);
				return;
			}
		}

		QMessageBox::warning(this, "String not found", "No commit with the string '" + s0 + "'");
	}
}

void HistoryViewer::on_needle_returnPressed()
{
	on_next_match_clicked();
}

void HistoryViewer::on_copyhash_clicked()
{
	QGuiApplication::clipboard()->setText(ui->hash->text() );
}

void HistoryViewer::on_copycommitter_clicked()
{
	QGuiApplication::clipboard()->setText(ui->committer->text() );
}

void HistoryViewer::on_copyauthor_clicked()
{
	QGuiApplication::clipboard()->setText(ui->author->text() );
}

void HistoryViewer::on_copydate_clicked()
{
	QGuiApplication::clipboard()->setText(ui->date->text() );
}

void HistoryViewer::on_copymessage_clicked()
{
	QGuiApplication::clipboard()->setText(ui->message->toPlainText() );
}

void HistoryViewer::on_copyparents_clicked()
{
	QStringList l;

	for ( int i = 0, n = ui->parents->count(); i < n; ++i )
	{
		if ( QListWidgetItem* item = ui->parents->item(i) )
		{
			const QVariant v = item->data(Qt::UserRole);
			const auto     h = v.value< hash_type >();
			l << QString::fromStdString(h.to_string() );
		}
	}

	QGuiApplication::clipboard()->setText(l.join(' ') );
}

void HistoryViewer::on_copycommitteremail_clicked()
{
	QGuiApplication::clipboard()->setText(ui->committer_email->text() );
}

void HistoryViewer::on_copyauthoremail_clicked()
{
	QGuiApplication::clipboard()->setText(ui->author_email->text() );
}
