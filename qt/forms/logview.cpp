#include "logview.h"

#include <QPainter>
#include <QHeaderView>
#include <QSettings>

// qtility
#include "delegates/graphdelegate.h"

// project
#include "models/logmodel.h"
#include "settings.h"

LogView::LogView(QWidget* parent_)
	: GraphView(parent_)
{
	setRootIsDecorated(false);
}

void LogView::settings_updated()
{
	/// @todo Forward to LogModel
	auto& settings = global_settings();

	QVariantList v = settings.getColourList();

	std::vector< QColor > cl;

	for ( int i = 0; i < v.count(); ++i )
	{
		cl.push_back(v.at(i).value< QColor >() );
	}

	set_graph_colours(LogModel::GRAPH, cl);

	if ( LogModel* m = dynamic_cast< LogModel* >( model() ) )
	{
		m->settings_updated();
	}

	repaint();

}
