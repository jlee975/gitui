#ifndef OVERVIEWFORM_H
#define OVERVIEWFORM_H

#include <memory>

#include <QWidget>
class QMenu;
class QSettings;

#include "qrepotypes.h"

class BranchesModel;
class RemotesModel;
class TagsModel;
class StashesModel;
class StatusModel;
class Repository;
class CommitViewer;
class FileHistory;

namespace Ui
{
class OverviewForm;
}

class OverviewForm
	: public QWidget
{
	Q_OBJECT
public:
	explicit OverviewForm(QWidget* parent = nullptr);
	~OverviewForm();

	void open(const std::shared_ptr< Repository >&);
	void refresh();
	void show_branches(bool);
	void show_remotes(bool);
	void show_tags(bool);
	void show_stashes(bool);
	void show_status(bool);
public slots:
	void settings_updated();
	void find_message();
signals:
	void show_diff(commit_id, commit_id, int);
	void file_history(QStringList);
	void blame(const QStringList&);
	void commit_index();
	void remove_from_index(std::vector< status_id >);
	void checkout_branch(branch_id);
	void checkout_tag(tag_id);
	void create_branch(commit_id);
	void rename_branch(branch_id);
	void rebase_branch(branch_id);
	void reflog_branch(branch_id);
	void reflog_head();
	void remove_branch(branch_id);
	void relocate_branch(branch_id, commit_id);
	void remove_tag(tag_id);
	void reflog_tag(tag_id);
	void relocate_tag(tag_id, commit_id);
	void pop(stash_id);
	void drop(stash_id);
	void push_stash();
	void add_remote();
	void rename_remote(remote_id);
	void remove_remote(remote_id, std::size_t);
	void set_url(remote_id);
	void diff_choose(commit_id);
	void create_tag(commit_id);
	void fetch_remote(remote_id);
private slots:
	void branches_changed();
	void tags_changed();
	void index_add();
	void index_remove();
	void branches_checkout();
	void branches_create();
	void branches_rename();
	void branches_rebase();
	void branches_delete();
	void branches_reflog();
	void remote_add();
	void remote_delete();
	void remote_rename();
	void remote_fetch();
	void remote_set_url();
	void tags_delete();
	void tags_reflog();
	void tags_checkout();
	void pop_stash();
	void drop_stash();
	void diff_commit(commit_id, commit_id);
	void diff_index(commit_id);
	void diff_workdir(commit_id);
	void on_branches_doubleClicked(const QModelIndex& index);
	void on_tags_doubleClicked(const QModelIndex& index);
	void on_remotes_doubleClicked(const QModelIndex& index);
	void on_remotes_customContextMenuRequested(const QPoint& pos);
	void on_stashes_customContextMenuRequested(const QPoint& pos);
	void on_status_customContextMenuRequested(const QPoint& pos);
	void on_actionShow_Ignored_Files_toggled(bool arg1);
	void on_branches_customContextMenuRequested(const QPoint& pos);
	void on_tags_customContextMenuRequested(const QPoint& pos);
	void on_actionFile_History_triggered();
	void on_actionStash_triggered();
	void on_status_doubleClicked(const QModelIndex& index);
    void on_stashes_doubleClicked(const QModelIndex &index);

private:
	Ui::OverviewForm* ui;

	QMenu* statusmenu;
	QMenu* remotesmenu;
	QMenu* tagsmenu;
	QMenu* stashmenu;

	BranchesModel* branchesmodel;
	RemotesModel*  remotesmodel;
	TagsModel*     tagsmodel;
	StashesModel*  stashesmodel;
	StatusModel*   statusmodel;

	std::shared_ptr< Repository > repo;
};

#endif // OVERVIEWFORM_H
