#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <memory>

#include <QMainWindow>

// qtility
#include "recentdocs.h"

// project
#include "qrepotypes.h"

class QSettings;

class Repository;
class BranchesModel;
class RemotesModel;
class TagsModel;
class StashesModel;
class CommitViewer;
class OverviewForm;
class FileHistory;

namespace Ui
{
class MainWindow;
}

class MainWindow
	: public QMainWindow
{
	Q_OBJECT
public:
	MainWindow(QString, QString, QString, QWidget* parent = nullptr);
	~MainWindow();
signals:
	void settings_updated();
private slots:
	void on_actionOpen_triggered();

	void on_actionShow_Branches_triggered(bool checked);

	void on_actionShow_Remotes_triggered(bool checked);

	void on_actionShow_Tags_triggered(bool checked);

	void on_actionShow_Stashes_triggered(bool checked);

	void on_actionGlobal_Configuration_triggered();

	void show_diff(commit_id, commit_id, int);
	void file_history(QStringList);
	void on_actionPreferences_triggered();
	void on_tabWidget_tabCloseRequested(int index);

	void on_action_repo_Directory_triggered();
	void on_actionRefresh_triggered();
	void on_actionExit_triggered();
	void on_actionShow_Status_triggered(bool checked);
	void on_actionClone_triggered();

	void on_actionCommit_Index_triggered();

	void on_actionPath_History_triggered();

	void on_actionPath_History_Manual_triggered();

	void on_actionBlame_triggered();
	void blame(const QStringList&);
	void commit_index();
	void remove_from_index(std::vector< status_id >);
	void checkout_branch(branch_id);
	void checkout_tag(tag_id);
	void create_branch(commit_id);
	void rename_branch(branch_id);
	void rebase_branch(branch_id);
	void reflog_branch(branch_id);
	void reflog_tag(tag_id);
	void reflog_head();
	void remove_branch(branch_id);
	void remove_tag(tag_id);
	void pop_stash(stash_id);
	void drop_stash(stash_id);
	void push_stash();
	void add_remote();
	void rename_remote(remote_id);
	void remove_remote(remote_id, std::size_t);
	void set_url(remote_id);
	void diff_choose(commit_id);
	void create_tag(commit_id);
	void relocate_branch(branch_id, commit_id);
	void relocate_tag(tag_id, commit_id);
	void closeEvent(QCloseEvent*) override;
	void on_actionFind_triggered();
	void fetch_remote(remote_id);
private:
	void open(const QString&);
	void close_all();
	void refresh();
	void rebuild_recent_doc_actions();
	void file_opened(const QString& filename);

	Ui::MainWindow* ui;

	// The command used to run the application in the first place
	QString apppath;

	// The directory where the app was launched from
	QString dir;

	RecentDocs recentdocs;

	std::shared_ptr< Repository > repo;
	OverviewForm*                 overview;
};

#endif // MAINWINDOW_H
