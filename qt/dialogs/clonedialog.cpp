#include "clonedialog.h"
#include "ui_clonedialog.h"

#include <QFileDialog>
#include <QMessageBox>

CloneDialog::CloneDialog(QWidget* parent)
	: QDialog(parent),
	ui(new Ui::CloneDialog)
{
	ui->setupUi(this);
}

CloneDialog::~CloneDialog()
{
	delete ui;
}

void CloneDialog::on_choosedir_clicked()
{
	QString s = QFileDialog::getExistingDirectory(this);

	if ( !s.isEmpty() )
	{
		ui->dest->setText(s);
	}
}

void CloneDialog::accept()
{
	if ( !ui->source->text().isEmpty() && !ui->dest->text().isEmpty() )
	{
		QDialog::accept();
	}
	else
	{
		QMessageBox::critical(this, "Cannot clone", "Fields cannot be empty");
	}
}

QString CloneDialog::getSource() const
{
	return ui->source->text();
}

QString CloneDialog::getDest() const
{
	QString s = ui->dest->text();

	if ( ui->create->isChecked() )
	{
		QString   t = ui->source->text();
		const int i = t.lastIndexOf('/');

		if ( i != -1 )
		{
			const int j = t.lastIndexOf('.');

			if ( j > i )
			{
				s += '/' + t.mid(i + 1, j - ( i + 1 ) );
			}
			else
			{
				s += '/' + t.mid(i + 1);
			}
		}
	}

	return s;
}
