#ifndef ADDREMOTEDIALOG_H
#define ADDREMOTEDIALOG_H

#include <QDialog>

namespace Ui
{
class AddRemoteDialog;
}

class AddRemoteDialog
	: public QDialog
{
	Q_OBJECT
public:
	explicit AddRemoteDialog(QWidget* parent = nullptr);
	~AddRemoteDialog();
	QString url() const;
	QString name() const;
private:
	Ui::AddRemoteDialog* ui;
};

#endif // ADDREMOTEDIALOG_H
