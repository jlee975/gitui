#ifndef CLONEDIALOG_H
#define CLONEDIALOG_H

#include <QDialog>

namespace Ui
{
class CloneDialog;
}

class CloneDialog
	: public QDialog
{
	Q_OBJECT
public:
	explicit CloneDialog(QWidget* parent = nullptr);
	~CloneDialog();

	QString getSource() const;
	QString getDest() const;
private slots:
	void on_choosedir_clicked();

	void accept() override;
private:
	Ui::CloneDialog* ui;
};

#endif // CLONEDIALOG_H
