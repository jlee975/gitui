#include "globalconfigurationdialog.h"
#include "ui_globalconfigurationdialog.h"

#include <QMessageBox>

#include <git2.h>

const char key_username[]   = "user.name";
const char key_useremail[]  = "user.email";
const char key_difftool[]   = "diff.tool";
const char key_diffprompt[] = "difftool.prompt";

GlobalConfigurationDialog::GlobalConfigurationDialog(QWidget* parent)
	: QDialog(parent),
        ui(new Ui::GlobalConfigurationDialog), cfg(nullptr), global(nullptr)
{
	ui->setupUi(this);

	// Load user name and e-mail
	git_config* cfg_ = nullptr;

	if ( git_config_open_default(&cfg_) == 0 )
	{
		git_config* global_ = nullptr;

		if ( git_config_open_level(&global_, cfg_, GIT_CONFIG_LEVEL_GLOBAL) == 0 )
		{
			global = global_;
			cfg    = cfg_;

			reset();
			return;
		}

		git_config_free(cfg_);
	}
}

GlobalConfigurationDialog::~GlobalConfigurationDialog()
{
	if ( global )
	{
		git_config_free(static_cast< git_config* >( global ) );
		git_config_free(static_cast< git_config* >( cfg ) );
	}

	delete ui;
}

void GlobalConfigurationDialog::on_buttonBox_accepted()
{
	apply();
}

void GlobalConfigurationDialog::on_buttonBox_clicked(QAbstractButton* button)
{
	switch ( ui->buttonBox->standardButton(button) )
	{
	case QDialogButtonBox::Reset:
		reset();
		break;
	case QDialogButtonBox::Apply:
		apply();
		break;
	default:
		break;
	}
}

void GlobalConfigurationDialog::reset()
{
	ui->userNameLineEdit->setText(get_string(key_username) );
	ui->eMailLineEdit->setText(get_string(key_useremail) );
	ui->difftool->setText(get_string(key_difftool) );
	ui->diffprompt->setChecked(get_bool(key_diffprompt, true) );
}

void GlobalConfigurationDialog::apply()
{
	// Save username and email
	if ( global )
	{
		set_string(key_username, ui->userNameLineEdit->text() );
		set_string(key_useremail, ui->eMailLineEdit->text() );
		set_string(key_difftool, ui->difftool->text() );
		set_bool(key_diffprompt, ui->diffprompt->isChecked() );
	}
}

bool GlobalConfigurationDialog::get_bool(
	const char* label,
	bool        fallback
)
{
	if ( global )
	{
		int value = 0;

		if ( git_config_get_bool(&value, static_cast< git_config* >( global ), label) == 0 )
		{
			return value;
		}
	}

	return fallback;
}

QString GlobalConfigurationDialog::get_string(const char* label)
{
	QString value;

	if ( global )
	{
		git_buf buf = {};

		if ( git_config_get_string_buf(&buf, static_cast< git_config* >( global ), label) == 0 )
		{
			value = QString::fromLatin1(buf.ptr, buf.size);
			git_buf_free(&buf);
		}
	}

	return value;
}

void GlobalConfigurationDialog::set_string(
	const char*    label,
	const QString& value
)
{
	if ( global )
	{
		const std::string t = value.toStdString();
		git_config_set_string(static_cast< git_config* >( global ), label, t.c_str() );
	}
}

void GlobalConfigurationDialog::set_bool(
	const char* label,
	bool        value
)
{
	if ( global )
	{
		git_config_set_bool(static_cast< git_config* >( global ), label, value);
	}
}
