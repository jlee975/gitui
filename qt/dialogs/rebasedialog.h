#ifndef REBASEDIALOG_H
#define REBASEDIALOG_H

#include <memory>
#include <optional>

#include <QDialog>

#include "qrepotypes.h"

class Repository;

namespace Ui
{
class RebaseDialog;
}

class RebaseDialog
	: public QDialog
{
	Q_OBJECT
public:
	RebaseDialog(std::shared_ptr< Repository >, QString, QWidget* parent = nullptr);
	~RebaseDialog();

	branch_id tip() const;
	hash_type onto() const;
	std::optional< hash_type > from() const;
	bool recursive() const;
private slots:
	void on_choose_onto_clicked();
	void on_ok_clicked();
	void on_cancel_clicked();
	void on_onto_currentIndexChanged(int index);
	void on_choose_from_clicked();
private:
	Ui::RebaseDialog* ui;

	std::shared_ptr< Repository > repo;
};

#endif // REBASEDIALOG_H
