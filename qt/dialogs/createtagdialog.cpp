#include "createtagdialog.h"
#include "ui_createtagdialog.h"

CreateTagDialog::CreateTagDialog(QWidget* parent)
	: QDialog(parent),
	ui(new Ui::CreateTagDialog)
{
	ui->setupUi(this);
}

CreateTagDialog::~CreateTagDialog()
{
	delete ui;
}

QString CreateTagDialog::tag_name() const
{
	return ui->tag->text();
}

QString CreateTagDialog::message() const
{
	return ui->message->toPlainText();
}
