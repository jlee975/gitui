#include "repopropertiesdialog.h"
#include "ui_repopropertiesdialog.h"

#include <filesystem>

#include <QTreeWidgetItem>
#include <QDateTime>
#include <QSortFilterProxyModel>

#include "models/contributorsmodel.h"

#include "repository.h"

RepoPropertiesDialog::RepoPropertiesDialog(
	const std::shared_ptr< Repository >& repo,
	std::size_t                          hashlen_,
	QWidget*                             parent
)
	: QDialog(parent), ui(new Ui::RepoPropertiesDialog)
{
	const QString dir = QString::fromStdString(repo->path() );

	const QString repodir = dir + "/.git";

	ui->setupUi(this);

	ui->contributorsview->setSortingEnabled(true);

	ui->path->setText(repodir);

	unsigned long filecount = 0;
	unsigned long dircount  = 1;
	unsigned long filebytes = 0;
	auto dirbytes  = std::filesystem::file_size(repodir.toStdString() );

	for ( std::filesystem::recursive_directory_iterator it(repodir.toStdString() ), last; it != last; ++it )
	{
		if ( std::filesystem::is_directory(it->status() ) )
		{
			++dircount;
			dirbytes += std::filesystem::file_size(it->path() );
		}
		else
		{
			++filecount;
			filebytes += std::filesystem::file_size(it->path() );
		}
	}

	ui->filecount->setText(QString::number(filecount) );
	ui->filebytes->setText(QString::number(filebytes) + " bytes");
	ui->dircount->setText(QString::number(dircount) );
	ui->dirbytes->setText(QString::number(dirbytes) + " bytes");
	ui->totalcount->setText(QString::number(dircount + filecount) );
	ui->totalbytes->setText(QString::number(dirbytes + filebytes) + " bytes");

	ui->numcommits->setText(QString::number(repo->number_of_commits() ) );
	ui->numcontributors->setText(QString::number(repo->number_of_contributors() ) );

	auto m = new ContributorsModel(repo, hashlen_, this);
	auto p = new QSortFilterProxyModel(this);
	p->setSourceModel(m);

	ui->contributorsview->setModel(p);
}

RepoPropertiesDialog::~RepoPropertiesDialog()
{
	delete ui;
}
