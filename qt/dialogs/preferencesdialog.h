#ifndef PREFERENCESDIALOG_H
#define PREFERENCESDIALOG_H

#include <QDialog>

namespace Ui
{
class PreferencesDialog;
}

class PreferencesDialog
	: public QDialog
{
	Q_OBJECT
public:
	explicit PreferencesDialog(QWidget* parent = nullptr);
	~PreferencesDialog();
private slots:
	void on_buttonBox_accepted();

	void on_add_colour_clicked();

	void on_remove_colour_clicked();

	void on_edit_colour_clicked();
	void on_reset_colours_clicked();

	void on_reset_hashlen_clicked();
private:
	void reset_colours_inner(const QVariantList&);

	Ui::PreferencesDialog* ui;
};

#endif // PREFERENCESDIALOG_H
