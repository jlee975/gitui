#include "rebasedialog.h"
#include "ui_rebasedialog.h"

#include <QMessageBox>

#include "repository.h"

#include "selectcommitdialog.h"

#include "settings.h"

RebaseDialog::RebaseDialog(
	std::shared_ptr< Repository > repo_,
	QString                       name,
	QWidget*                      parent
)
	: QDialog(parent),
	ui(new Ui::RebaseDialog), repo(std::move(repo_) )
{
	ui->setupUi(this);

	auto sorted_branches = repo->branch_ids();
	std::sort(sorted_branches.begin(), sorted_branches.end(), [this](branch_id l, branch_id r) {
		return repo->branch(l).name < repo->branch(r).name;
	});

	bool remotes_needs_separator = false;
	bool tags_needs_separator    = false;

	ui->from->addItem("Auto");

	for ( const auto bi : sorted_branches )
	{
		const auto& b = repo->branch(bi);
		QString     s = QString::fromStdString(b.name);
		ui->branch->addItem(s, QVariant::fromValue(bi) );
		ui->onto->addItem(s, QVariant::fromValue(b.hash) );
		ui->from->addItem(s, QVariant::fromValue(b.hash) );
		remotes_needs_separator = true;
	}

	for ( const auto& ri : repo->remote_ids() )
	{
		const auto& r = repo->remote(ri);

		for ( std::size_t i = 0; i < r.branches.size(); ++i )
		{
			if ( remotes_needs_separator )
			{
				ui->onto->insertSeparator(ui->onto->count() );
				ui->from->insertSeparator(ui->from->count() );
				remotes_needs_separator = false;
			}

			ui->onto->addItem(QString::fromStdString(r.full_name(i) ), QVariant::fromValue(r.branches[i].hash) );
			ui->from->addItem(QString::fromStdString(r.full_name(i) ), QVariant::fromValue(r.branches[i].hash) );
			tags_needs_separator = true;
		}
	}

	auto sorted_tags = repo->tag_ids();
	std::sort(sorted_tags.begin(), sorted_tags.end(), [this](tag_id l, tag_id r) {
		return repo->tag(l).name < repo->tag(r).name;
	});

	for ( const auto ti : sorted_tags )
	{
		if ( tags_needs_separator )
		{
			ui->onto->insertSeparator(ui->onto->count() );
			ui->from->insertSeparator(ui->from->count() );
			tags_needs_separator = false;
		}

		const auto& t = repo->tag(ti);

		ui->onto->addItem(QString::fromStdString(t.name), QVariant::fromValue(t.hash) );
		ui->from->addItem(QString::fromStdString(t.name), QVariant::fromValue(t.hash) );
	}

	ui->branch->setCurrentText(name);
	ui->onto->setCurrentIndex(-1);
	ui->from->setCurrentIndex(0);
}

RebaseDialog::~RebaseDialog()
{
	delete ui;
}

branch_id RebaseDialog::tip() const
{
	return ui->branch->currentData().value< branch_id >();
}

hash_type RebaseDialog::onto() const
{
	return ui->onto->currentData().value< hash_type >();
}

std::optional< hash_type > RebaseDialog::from() const
{
	const auto v = ui->from->currentData();

	if ( v.isNull() )
	{
		return {};
	}

	return v.value< hash_type >();
}

bool RebaseDialog::recursive() const
{
	return ui->recurse->isChecked();
}

void RebaseDialog::on_choose_onto_clicked()
{
	SelectCommitDialog dlg(repo, this);

	if ( dlg.exec() == QDialog::Accepted )
	{
		const auto      c = dlg.get_commit();
		const hash_type h = repo->commit(c).hashxxx;

		const QString s = QString::fromStdString(h.to_string(global_settings().hashDisplayLength() ) );

		ui->onto->addItem(s, QVariant::fromValue(h) );
		ui->onto->setCurrentText(s);
	}
}

/// @bug We should verify that the selected commit is actually upstream from selected branch
void RebaseDialog::on_choose_from_clicked()
{
	SelectCommitDialog dlg(repo, this);

	if ( dlg.exec() == QDialog::Accepted )
	{
		const auto      c = dlg.get_commit();
		const hash_type h = repo->commit(c).hashxxx;

		const QString s = QString::fromStdString(h.to_string(global_settings().hashDisplayLength() ) );

		ui->from->addItem(s, QVariant::fromValue(h) );
		ui->from->setCurrentText(s);
	}
}

/// @bug We should verify that the from commit is actually upstream from selected branch
void RebaseDialog::on_ok_clicked()
{
	if ( ui->onto->currentIndex() == -1 )
	{
		QMessageBox::critical(this, "Dialog incomplete", "You have not chosen a commit to be the new base");
		return;
	}

	accept();
}

void RebaseDialog::on_cancel_clicked()
{
	reject();
}

void RebaseDialog::on_onto_currentIndexChanged(int index)
{
	ui->ok->setEnabled(index != -1);
}
