#ifndef GLOBALCONFIGURATIONDIALOG_H
#define GLOBALCONFIGURATIONDIALOG_H

#include <QDialog>

class QAbstractButton;

namespace Ui
{
class GlobalConfigurationDialog;
}

class GlobalConfigurationDialog
	: public QDialog
{
	Q_OBJECT
public:
	explicit GlobalConfigurationDialog(QWidget* parent = nullptr);
	~GlobalConfigurationDialog();
private slots:
	void on_buttonBox_accepted();

	void on_buttonBox_clicked(QAbstractButton* button);
private:
	void apply();
	void reset();
	bool get_bool(const char*, bool);
	QString get_string(const char*);
	void set_string(const char*, const QString&);
	void set_bool(const char*, bool);

	Ui::GlobalConfigurationDialog* ui;

	void* cfg;
	void* global;
};

#endif // GLOBALCONFIGURATIONDIALOG_H
