#include "selectcommitdialog.h"
#include "ui_selectcommitdialog.h"

#include "models/logmodel.h"
#include "repository.h"

SelectCommitDialog::SelectCommitDialog(
	std::shared_ptr< Repository > repo,
	QWidget*                      parent
)
	: QDialog(parent),
	ui(new Ui::SelectCommitDialog)
{
	ui->setupUi(this);
	model = new LogModel(this);
	model->setRepository(repo);
	ui->treeView->setModel(model);
}

SelectCommitDialog::~SelectCommitDialog()
{
	delete model;
	delete ui;
}

commit_id SelectCommitDialog::get_commit() const
{
	return commit;
}

void SelectCommitDialog::on_buttonBox_accepted()
{
	QModelIndexList l = ui->treeView->selectionModel()->selectedIndexes();

	if ( !l.isEmpty() )
	{
		commit = model->get_commit(l.front() );
	}
}

void SelectCommitDialog::on_treeView_doubleClicked(const QModelIndex& index)
{
	commit = model->get_commit(index);

	accept();
}
