#include "preferencesdialog.h"
#include "ui_preferencesdialog.h"

#include <QSettings>
#include <QColorDialog>
#include <QMessageBox>

#include "settings.h"

PreferencesDialog::PreferencesDialog(QWidget* parent)
	: QDialog(parent), ui(new Ui::PreferencesDialog)
{
	ui->setupUi(this);

	const auto& settings = global_settings();

	ui->hashlen->setValue(settings.hashDisplayLength() );

	reset_colours_inner(settings.getColourList() );
}

PreferencesDialog::~PreferencesDialog()
{
	delete ui;
}

void PreferencesDialog::reset_colours_inner(const QVariantList& colour_list)
{
	ui->colours->clear();

	for ( int i = 0; i < colour_list.count(); ++i )
	{
		QVariant v = colour_list.at(i);

		QPixmap pm(16, 16);
		pm.fill(v.value< QColor >() );
		QListWidgetItem* item = new QListWidgetItem(pm, "Colour " + QString::number(i + 1) );
		item->setData(Qt::UserRole, v);
		ui->colours->addItem(item);
	}
}

void PreferencesDialog::on_buttonBox_accepted()
{
	auto& settings = global_settings();

	settings.hashDisplayLength(ui->hashlen->value() );

	QVariantList colour_list;

	for ( int i = 0, n = ui->colours->count(); i < n; ++i )
	{
		colour_list << ui->colours->item(i)->data(Qt::UserRole);
	}

	settings.setColourList(colour_list);
}

void PreferencesDialog::on_add_colour_clicked()
{
	const QColor c = QColorDialog::getColor();

	if ( c.isValid() )
	{
		QPixmap pm(16, 16);
		pm.fill(c);
		QListWidgetItem* item = new QListWidgetItem(pm, "Colour " + QString::number(ui->colours->count() + 1) );
		item->setData(Qt::UserRole, c);
		ui->colours->addItem(item);
	}
}

void PreferencesDialog::on_remove_colour_clicked()
{
	int i = ui->colours->currentRow();

	if ( i != -1 )
	{
		QListWidgetItem* item = ui->colours->takeItem(i);
		delete item;

		for ( int n = ui->colours->count(); i < n; ++i )
		{
			ui->colours->item(i)->setText("Colour " + QString::number(i + 1) );
		}
	}
}

void PreferencesDialog::on_edit_colour_clicked()
{
	if ( QListWidgetItem* item = ui->colours->currentItem() )
	{
		const QColor c0 = item->data(Qt::UserRole).value< QColor >();
		const QColor c  = QColorDialog::getColor(c0);

		if ( c.isValid() )
		{
			QPixmap pm(16, 16);
			pm.fill(c);
			item->setIcon(pm);
			item->setData(Qt::UserRole, c);
		}
	}
}

void PreferencesDialog::on_reset_colours_clicked()
{
	if ( QMessageBox::question(this, "Reset colours", "Are you sure?") == QMessageBox::Yes )
	{
		reset_colours_inner(global_settings().defaultColourList() );
	}
}

void PreferencesDialog::on_reset_hashlen_clicked()
{
	if ( QMessageBox::question(this, "Reset hash length", "Are you sure?") == QMessageBox::Yes )
	{
		ui->hashlen->setValue(40);
	}
}
