#include "addremotedialog.h"
#include "ui_addremotedialog.h"

AddRemoteDialog::AddRemoteDialog(QWidget* parent)
	: QDialog(parent),
	ui(new Ui::AddRemoteDialog)
{
	ui->setupUi(this);
}

AddRemoteDialog::~AddRemoteDialog()
{
	delete ui;
}

QString AddRemoteDialog::url() const
{
	return ui->url->text();
}

QString AddRemoteDialog::name() const
{
	return ui->name->text();
}
