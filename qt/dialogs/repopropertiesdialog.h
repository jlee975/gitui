#ifndef RepoPropertiesDialog_H
#define RepoPropertiesDialog_H

#include <memory>

#include <QDialog>

class Repository;

namespace Ui
{
class RepoPropertiesDialog;
}

class RepoPropertiesDialog
	: public QDialog
{
	Q_OBJECT
public:
	RepoPropertiesDialog(const std::shared_ptr< Repository >&, std::size_t, QWidget* parent = nullptr);
	~RepoPropertiesDialog();
private:
	Ui::RepoPropertiesDialog* ui;
};

#endif // RepoPropertiesDialog_H
