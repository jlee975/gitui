#ifndef SELECTCOMMITDIALOG_H
#define SELECTCOMMITDIALOG_H

#include <memory>
#include <QDialog>

#include "qrepotypes.h"

class Repository;
class LogModel;

namespace Ui
{
class SelectCommitDialog;
}

class SelectCommitDialog
	: public QDialog
{
	Q_OBJECT
public:
	explicit SelectCommitDialog(std::shared_ptr< Repository >, QWidget* parent = nullptr);
	~SelectCommitDialog();

	commit_id get_commit() const;
private slots:
	void on_buttonBox_accepted();

	void on_treeView_doubleClicked(const QModelIndex& index);
private:
	Ui::SelectCommitDialog* ui;
	LogModel*               model;
	commit_id               commit;
};

#endif // SELECTCOMMITDIALOG_H
