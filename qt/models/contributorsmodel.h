#ifndef CONTRIBUTORSMODEL_H
#define CONTRIBUTORSMODEL_H

#include <map>
#include <memory>

#include <QAbstractItemModel>

#include "repotypes.h"
class Repository;

class ContributorsModel
	: public QAbstractItemModel
{
	Q_OBJECT
public:
	enum {AUTHOR, EMAIL, NUM_AUTHORSHIPS, NUM_COMMITS, FIRST_COMMIT, FIRST_HASH, LAST_COMMIT, LAST_HASH, NUM_COLUMNS};

	explicit ContributorsModel(const std::shared_ptr< Repository >&, std::size_t, QObject* parent = nullptr);

	// Header:
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

	// Basic functionality:
	QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex() ) const override;
	QModelIndex parent(const QModelIndex& index) const override;

	int rowCount(const QModelIndex& parent    = QModelIndex() ) const override;
	int columnCount(const QModelIndex& parent = QModelIndex() ) const override;

	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
private:
	std::shared_ptr< Repository > repo;
	std::size_t                   hashlen;
};

#endif // CONTRIBUTORSMODEL_H
