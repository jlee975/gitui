#include "reflogmodel.h"

#include "repository.h"

#include "settings.h"

ReflogModel::ReflogModel(
	std::shared_ptr< Repository > repo_,
	const QString&                ref,
	QObject*                      parent_
)
	: QAbstractItemModel(parent_), hashlen(global_settings().hashDisplayLength() )
{
	if ( repo_ )
	{
		reflog = repo_->reflog(ref.toStdString() );
	}
}

QVariant ReflogModel::data(
	const QModelIndex& idx,
	int                role
) const
{
	if ( role == Qt::DisplayRole )
	{
		const int r = idx.row();

		if ( r >= 0 && static_cast< unsigned >( r ) < reflog.size() )
		{
			switch ( idx.column() )
			{
			case COMMIT:
				return QString::fromStdString(reflog[r].curr.to_string() ).left(hashlen);

			case MESSAGE:
				return QString::fromStdString(reflog[r].message);

			case COMMITTER_NAME:
				return QString::fromStdString(reflog[r].name);

			case COMMITTER_EMAIL:
				return QString::fromStdString(reflog[r].email);
			}
		}
	}

	return QVariant();
}

Qt::ItemFlags ReflogModel::flags(const QModelIndex& idx) const
{
	if ( idx.isValid() )
	{
		return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
	}

	return Qt::NoItemFlags;
}

QVariant ReflogModel::headerData(
	int             section,
	Qt::Orientation orientation,
	int             role
) const
{
	if ( orientation == Qt::Horizontal && role == Qt::DisplayRole )
	{
		switch ( section )
		{
		case COMMIT:
			return "Commit";

		case MESSAGE:
			return "Message";

		case COMMITTER_NAME:
			return "Committer";

		case COMMITTER_EMAIL:
			return "E-mail";

		default:
			break;
		} // switch

	}

	return QVariant();
}

QModelIndex ReflogModel::index(
	int                row,
	int                col,
	const QModelIndex& idx
) const
{
	if ( row >= 0 && col >= 0 && col < NUMBER_OF_COLUMNS )
	{
		if ( !idx.isValid() )
		{
			// Top level item
			if ( static_cast< unsigned >( row ) < reflog.size() )
			{
				return createIndex(row, col);
			}
		}
	}

	return QModelIndex();
}

QModelIndex ReflogModel::parent(const QModelIndex&) const
{
	return QModelIndex();
}

int ReflogModel::rowCount(const QModelIndex& idx) const
{
	if ( !idx.isValid() )
	{
		return reflog.size();
	}

	return 0;
}

int ReflogModel::columnCount(const QModelIndex& idx) const
{
	if ( !idx.isValid() )
	{
		return NUMBER_OF_COLUMNS;
	}

	return 0;
}

void ReflogModel::settings_updated()
{
	hashlen = global_settings().hashDisplayLength();

	if ( !reflog.empty() )
	{
		emit dataChanged(index(0, COMMIT), index(reflog.size() - 1, COMMIT) );
	}
}
