#ifndef DIFFMODEL_H
#define DIFFMODEL_H

#include <QAbstractItemModel>

#include "qutility/patch.h"

class DiffModel
	: public QAbstractItemModel
{
	Q_OBJECT
public:
	explicit DiffModel(QObject* parent = nullptr);

	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QModelIndex index(int row, int column, const QModelIndex& parent       = QModelIndex() ) const override;
	QModelIndex parent(const QModelIndex& index) const override;
	int rowCount(const QModelIndex& parent           = QModelIndex() ) const override;
	int columnCount(const QModelIndex& parent        = QModelIndex() ) const override;
	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

	void clear();
	void setDiff(Diff);
	Diff getDiff(const QModelIndex&);
	QStringList files(const QModelIndexList&) const;
private:
	struct Info
	{
		Info* parent;
		QString name;
		std::size_t idx;
		std::vector< Info* > children;
	};

	void destroy(Info*);
	void visit(Diff&, const Info*);
	Info* to_info(const QModelIndex&) const;

	Diff  diff;
	Info* root;
};

#endif // DIFFMODEL_H
