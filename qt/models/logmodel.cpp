#include "logmodel.h"

#include <set>
#include <iostream>

#include <QFont>
#include <QBrush>

// repo lib
#include "repository.h"

// qtility
#include "delegates/graphdelegateitem.h"
#include "convert.h"

// project
#include "settings.h"

namespace
{
Line to_line(const line_type& t)
{
	Line line =
	{
		t.origin, t.old_lineno, t.new_lineno, QString::fromStdString(t.content)
	};

	return line;
}

Hunk to_hunk(const hunk_type& t)
{
	Hunk hunk =
	{
		QString::fromStdString(t.header),
		t.old_start, t.old_lines, t.new_start, t.new_lines
	};

	for ( std::size_t i = 0; i < t.lines.size(); ++i )
	{
		hunk.lines.push_back(to_line(t.lines[i]) );
	}

	return hunk;
}

Diff to_diff(const diff_type& t)
{
	Diff info;

	for ( std::size_t i = 0, n = t.deltas.size(); i < n; ++i )
	{
		Delta d =
		{
			t.deltas[i].added,
			t.deltas[i].removed,
			QString::fromStdString(t.deltas[i].old_name),
			QString::fromStdString(t.deltas[i].new_name),
		};

		for ( std::size_t j = 0, m = t.deltas[i].hunks.size(); j < m; ++j )
		{
			d.hunks.push_back(to_hunk(t.deltas[i].hunks[j]) );
		}

		info.deltas.push_back(d);
	}

	return info;
}

}

LogModel::LogModel(QObject* parent_)
        : GraphModel(parent_), repo_(nullptr), hashlen(global_settings().hashDisplayLength() )
{
}

void LogModel::settings_updated()
{
	beginResetModel();
	hashlen = global_settings().hashDisplayLength();
	endResetModel();
}

void LogModel::setRepository(std::shared_ptr< Repository > r)
{
	if ( r )
	{
		setRepository(r, r->topological_order() );
	}
	else
	{
		setRepository(r, {});
	}
}

void LogModel::setRepository(
	std::shared_ptr< Repository > r,
	reduction                     reduction_
)
{
	beginResetModel();
	repo_      = r;
	reductionx = std::move(reduction_);
	std::vector< QList< int > > leaving_;
	std::vector< QList< int > > nodes_;
	highlights.clear();

	// (colour, where going to)
	std::set< int >                            colour;
	std::vector< std::pair< int, commit_id > > in;

	for ( std::size_t i = 0, n = reductionx.size(); i < n; ++i )
	{
		std::set< int > inout;

		// erase all incoming edges that stop here
		{
			std::size_t j = 0;

			for ( std::size_t k = 0; k < in.size(); ++k )
			{
				if ( in[k].second != reductionx[i].orig )
				{
					in[j++] = in[k];
				}
				else
				{
					inout.insert(in[k].first);
					colour.erase(in[k].first);
				}
			}

			in.resize(j);
		}

		// now add a line originating here for each parent
		const std::vector< std::size_t >& parents = reductionx[i].parents;

		for ( std::size_t j = 0; j < parents.size(); ++j )
		{
			int k = 0;

			while ( colour.count(k) != 0 )
			{
				++k;
			}

			inout.insert(k);
			colour.insert(k);
			in.emplace_back(k, reductionx[parents[j]].orig);
		}

		QList< int > l;

		for ( std::set< int >::iterator it = colour.begin(); it != colour.end(); ++it )
		{
			l << *it;
		}

		leaving_.push_back(l);

		QList< int > l2;

		for ( std::set< int >::iterator it = inout.begin(); it != inout.end(); ++it )
		{
			l2 << *it;
		}

		nodes_.push_back(l2);
	}

	set_graph(std::move(leaving_), std::move(nodes_) );

	endResetModel();
}

void LogModel::setHighlight(
	int    row,
	QBrush c
)
{
	highlights[row] = c;
}

QVariant LogModel::data(
	const QModelIndex& idx,
	int                role
) const
{
	if ( repo_ && idx.isValid() )
	{
		const int r = idx.row();
		const int c = idx.column();

		const auto& commit = repo_->commit(reductionx[r].orig);

		if ( role == Qt::DisplayRole )
		{
			switch ( c )
			{
			case GRAPH:
			{
				GraphDelegateItem x = getGraphItem(r);

				const auto& rel = repo_->relations(commit.hashxxx);

				for ( const branch_id i : rel.second.branches )
				{
					x.labels << QString::fromStdString(repo_->branch(i).name);
				}

				for ( const tag_id i : rel.second.tags )
				{
					x.labels << QString::fromStdString(repo_->tag(i).name);
				}

				for ( const std::pair< remote_id, std::size_t >& p : rel.second.remotes )
				{
					x.labels << QString::fromStdString(repo_->remote(p.first).full_name(p.second) );
				}

				/// @todo Probably want something like "is_head(commit_index)"
				if ( commit.hashxxx == repo_->head() )
				{
					x.labels << "HEAD";
				}

				QVariant v;
				v.setValue(x);
				return v;
			}
			case DATE:
			{
				char        buf[1000];
				std::tm*    m  = std::localtime(&commit.actual_time);
				std::size_t tn = std::strftime(buf, sizeof( buf ), "%d-%b-%Y", m);
				return QString::fromLatin1(buf, tn);
			}

			case HASH:
				return QString::fromStdString(commit.hashxxx.to_string(hashlen) );

			case SUMMARY:
				return QString::fromStdString(commit.summary);

			case COMMITTER:
				return QString::fromStdString(repo_->contributor(commit.committer).name);

			case COMMITTER_EMAIL:
				return QString::fromStdString(repo_->contributor(commit.committer).email);

			case AUTHOR:
				return QString::fromStdString(repo_->contributor(commit.author).name);

			case AUTHOR_EMAIL:
				return QString::fromStdString(repo_->contributor(commit.author).email);

			default:
				break;
			} // switch

		}
		else if ( role == Qt::ForegroundRole )
		{
			std::map< int, QBrush >::const_iterator it = highlights.find(r);

			if ( it != highlights.end() )
			{
				return it->second;
			}
		}
		else if ( role == Qt::FontRole )
		{
			if ( commit.hashxxx == repo_->head() )
			{
				QFont font;
				font.setBold(true);
				return font;
			}
		}
	}

	return QVariant();
}

Qt::ItemFlags LogModel::flags(const QModelIndex& idx) const
{
	if ( idx.isValid() )
	{
		return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
	}

	return Qt::NoItemFlags;
}

QVariant LogModel::headerData(
	int             section,
	Qt::Orientation orientation,
	int             role
) const
{
	if ( orientation == Qt::Horizontal && role == Qt::DisplayRole )
	{
		switch ( section )
		{
		case GRAPH:
			return "Graph";

		case HASH:
			return "Hash";

		case DATE:
			return "Date";

		case SUMMARY:
			return "Summary";

		case COMMITTER:
			return "Committer";

		case COMMITTER_EMAIL:
			return "Comitter E-mail";

		case AUTHOR:
			return "Author";

		case AUTHOR_EMAIL:
			return "Author E-mail";

		default:
			break;
		} // switch

	}

	return QVariant();
}

QModelIndex LogModel::index(
	int                row,
	int                col,
	const QModelIndex& idx
) const
{
	if ( !idx.isValid() && row >= 0 && col >= 0 && static_cast< unsigned >( row ) < reductionx.size() &&
	     col < NUMBER_OF_COLUMNS )
	{
		return createIndex(row, col);
	}

	return QModelIndex();
}

QModelIndex LogModel::parent(const QModelIndex&) const
{
	return QModelIndex();
}

int LogModel::rowCount(const QModelIndex& idx) const
{
	if ( idx.isValid() )
	{
		return 0;
	}

	return reductionx.size();
}

int LogModel::columnCount(const QModelIndex&) const
{
	return NUMBER_OF_COLUMNS;
}

commit_id LogModel::get_commit(const QModelIndex& idx) const
{
	if ( idx.isValid() )
	{
		return row_to_commit(idx.row() );
	}

	return INVALID_COMMIT;
}

commit_id LogModel::row_to_commit(int i) const
{
	if ( i >= 0 && static_cast< unsigned >( i ) < reductionx.size() )
	{
		return reductionx[i].orig;
	}

	return INVALID_COMMIT;
}

void LogModel::commit_changed(
	commit_id commit,
	int       column
)
{
	for ( std::size_t i = 0, n = reductionx.size(); i < n; ++i )
	{
		if ( reductionx[i].orig == commit )
		{
			const QModelIndex jdx = index(i, column);
			emit              dataChanged(jdx, jdx);
		}
	}
}

bool LogModel::is_graph_column_(int c) const
{
	return c == GRAPH;
}

hash_type LogModel::get_hash(const QModelIndex& idx) const
{
	if ( repo_ && idx.isValid() )
	{
		const commit_type& t = repo_->commit(reductionx[idx.row()].orig);
		return t.hashxxx;
	}

	return hash_type();
}

std::vector< hash_type > LogModel::get_diff_parents(const QModelIndex& idx) const
{
	std::vector< hash_type > l;

	if ( repo_ && idx.isValid() )
	{
		const int r = idx.row();

		for ( const std::size_t p : reductionx[r].parents )
		{
			l.push_back(repo_->commit(reductionx[p].orig).hashxxx);
		}
	}

	return l;
}

Diff LogModel::get_diff(const QModelIndex& idx) const
{
	Diff info;

	if ( repo_ && idx.isValid() )
	{
		const commit_id ci      = reductionx[idx.row()].orig;
		const auto&     parents = repo_->parents(ci);

		if ( !parents.empty() )
		{
			info = to_diff(repo_->diff_brief(ci, parents[0]) );
		}
	}

	return info;
}

Diff LogModel::get_diff(
	const QModelIndex& idx1,
	const QModelIndex& idx2
) const
{
	Diff info;

	if ( repo_ && idx1.isValid() && idx2.isValid() )
	{
		info = to_diff(repo_->diff_full(reductionx[idx1.row()].orig, reductionx[idx2.row()].orig) );

	}

	return info;
}

Diff LogModel::get_diff(
	int first,
	int last
) const
{
	Diff info;

	if ( repo_ )
	{
		info = to_diff(repo_->diff_full(reductionx[first].orig, reductionx[last].orig) );
	}

	return info;
}

Diff LogModel::get_diff_with_index(commit_id first) const
{
	Diff info;

	if ( repo_ )
	{
		info = to_diff(repo_->diff_with_index(first) );
	}

	return info;
}

Diff LogModel::get_diff_with_index(int r) const
{
	Diff info;

	if ( repo_ )
	{
		info = to_diff(repo_->diff_with_index(reductionx[r].orig) );
	}

	return info;
}

Diff LogModel::get_diff_with_workdir(commit_id first) const
{
	Diff info;

	if ( repo_ )
	{
		info = to_diff(repo_->diff_with_workdir(first) );
	}

	return info;
}

Diff LogModel::get_diff_with_workdir(int r) const
{
	Diff info;

	if ( repo_ )
	{
		info = to_diff(repo_->diff_with_workdir(reductionx[r].orig) );
	}

	return info;
}

int LogModel::find_hash(const hash_type& s) const
{
	if ( repo_ )
	{
		const commit_id c = repo_->hash_to_commit(s);

		for ( std::size_t i = 0, n = reductionx.size(); i < n; ++i )
		{
			if ( reductionx[i].orig == c )
			{
				return i;
			}
		}
	}

	return -1;
}

int LogModel::find_commit(commit_id c) const
{
	if ( repo_ )
	{
		for ( std::size_t i = 0, n = reductionx.size(); i < n; ++i )
		{
			if ( reductionx[i].orig == c )
			{
				return i;
			}
		}
	}

	return -1;
}
