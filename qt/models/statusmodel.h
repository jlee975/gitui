#ifndef STATUSMODEL_H
#define STATUSMODEL_H

#include <memory>

#include <QAbstractItemModel>

#include "repotypes.h"

class Repository;

class StatusModel
	: public QAbstractItemModel
{
public:
	enum {PATH, INDEX, WORKING_DIR, NUMBER_OF_COLUMNS};

	StatusModel();

	void setRepository(std::shared_ptr< Repository >);

	QVariant data(const QModelIndex&, int = Qt::DisplayRole) const override;
	Qt::ItemFlags flags(const QModelIndex&) const override;
	QVariant headerData(int, Qt::Orientation, int  = Qt::DisplayRole) const override;
	QModelIndex index(int, int, const QModelIndex& = QModelIndex() ) const override;
	QModelIndex parent(const QModelIndex&) const override;
	int rowCount(const QModelIndex&    = QModelIndex() ) const override;
	int columnCount(const QModelIndex& = QModelIndex() ) const override;

	//
	status_id id(const QModelIndex&) const;
	bool add_to_index(const QModelIndexList&);
	void set_show_ignored_files(bool);
	bool is_in_index(const QModelIndexList&) const;
	bool not_in_index(const QModelIndexList&) const;
	void refresh();
private:
	void rebuild();

	std::shared_ptr< Repository > repo_;
	bool                          show_ignored_files;
	std::vector< status_id >      filter;
};

#endif // STATUSMODEL_H
