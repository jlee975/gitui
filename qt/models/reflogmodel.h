#ifndef REFLOGMODEL_H
#define REFLOGMODEL_H
#include <memory>

#include <QAbstractItemModel>

#include "qrepotypes.h"

class Repository;

class ReflogModel
	: public QAbstractItemModel
{
public:
	enum {COMMIT, MESSAGE, COMMITTER_NAME, COMMITTER_EMAIL, NUMBER_OF_COLUMNS};
	ReflogModel(std::shared_ptr< Repository >, const QString&, QObject* = nullptr);

	QVariant data(const QModelIndex&, int = Qt::DisplayRole) const override;
	Qt::ItemFlags flags(const QModelIndex&) const override;
	QVariant headerData(int, Qt::Orientation, int  = Qt::DisplayRole) const override;
	QModelIndex index(int, int, const QModelIndex& = QModelIndex() ) const override;
	QModelIndex parent(const QModelIndex&) const override;
	int rowCount(const QModelIndex&    = QModelIndex() ) const override;
	int columnCount(const QModelIndex& = QModelIndex() ) const override;

	void settings_updated();
private:
	std::vector< reflog_type > reflog;
	std::size_t                hashlen;
};

#endif // REFLOGMODEL_H
