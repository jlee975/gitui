#include "diffmodel.h"

#include <QColor>
#include <QIcon>
#include <QMimeDatabase>
#include <QApplication>
#include <QStyle>

namespace
{
// Variation of https://stackoverflow.com/a/45739529
QIcon icon_for_filename(const QString& filename)
{
	static QMimeDatabase mime_database;

	for ( const auto& mime_type : mime_database.mimeTypesForFileName(filename) )
	{
		const auto& icon = QIcon::fromTheme(mime_type.iconName() );

		if ( !icon.isNull() )
		{
			return icon;
		}
	}

	return QApplication::style()->standardIcon(QStyle::SP_FileIcon);
}

}

DiffModel::DiffModel(QObject* parent)
	: QAbstractItemModel(parent), root(nullptr)
{
}

DiffModel::Info* DiffModel::to_info(const QModelIndex& idx) const
{
	return idx.isValid() ? static_cast< Info* >( idx.internalPointer() ) : root;
}

QVariant DiffModel::headerData(
	int             section,
	Qt::Orientation orientation,
	int             role
) const
{
	if ( role == Qt::DisplayRole && orientation == Qt::Horizontal )
	{
		switch ( section )
		{
		case 0:
			return "File";

		case 1:
			return "New path";
		}
	}

	return QVariant();
}

QModelIndex DiffModel::index(
	int                row,
	int                column,
	const QModelIndex& parent
) const
{
	if ( row >= 0 && column >= 0 && column < 2 )
	{
		if ( const Info* info = to_info(parent) )
		{
			if ( static_cast< unsigned >( row ) < info->children.size() )
			{
				return createIndex(row, column, info->children[row]);
			}
		}
	}

	return QModelIndex();
}

QModelIndex DiffModel::parent(const QModelIndex& index) const
{
	if ( const Info* info = to_info(index) )
	{
		if ( Info* p = info->parent )
		{
			if ( p->parent )
			{
				for ( std::size_t r = 0, n = p->parent->children.size(); r < n; ++r )
				{
					if ( p->parent->children[r] == p )
					{
						return createIndex(r, 0, p);
					}
				}
			}
		}
	}

	return QModelIndex();
}

int DiffModel::rowCount(const QModelIndex& parent) const
{
	if ( Info* info = to_info(parent) )
	{
		return info->children.size();
	}

	return 0;
}

int DiffModel::columnCount(const QModelIndex&) const
{
	return 2;
}

QVariant DiffModel::data(
	const QModelIndex& index,
	int                role
) const
{
	if ( index.isValid() )
	{
		const Info* info = static_cast< Info* >( index.internalPointer() );

		if ( role == Qt::DisplayRole )
		{
			switch ( index.column() )
			{
			case 0:
				return info->name;

			case 1:

				if ( info->idx != std::size_t(-1) )
				{
					const std::size_t i = info->idx;

					if ( diff.deltas[i].added )
					{
						return "NEW";
					}
					else if ( diff.deltas[i].removed )
					{
						return "REMOVED";
					}
					else
					{
						if ( diff.deltas[i].new_name != diff.deltas[i].old_name )
						{
							return diff.deltas[i].new_name;
						}
					}
				}

				break;
			} // switch

		}
		else if ( role == Qt::ForegroundRole )
		{
			if ( info->idx != std::size_t(-1) )
			{
				const std::size_t i = info->idx;

				if ( diff.deltas[i].added )
				{
					return QColor(Qt::darkGreen);
				}
				else if ( diff.deltas[i].removed )
				{
					return QColor(Qt::red);
				}
			}
		}
		else if ( role == Qt::DecorationRole )
		{
			if ( index.column() == 0 )
			{
				if ( info->idx != std::size_t(-1) )
				{
					const std::size_t i = info->idx;
					return icon_for_filename(diff.deltas[i].old_name);
				}
			}
		}
	}

	return QVariant();
}

void DiffModel::clear()
{
	Info* old = root;

	beginResetModel();
	diff = Diff();
	root = nullptr;
	endResetModel();
	destroy(old);
}

void DiffModel::setDiff(Diff diff_)
{
	Info* const root_ = new Info{ nullptr, "Diff", std::size_t(-1), {} };

	for ( std::size_t i = 0, n = diff_.deltas.size(); i < n; ++i )
	{
		const QStringList p = diff_.deltas[i].old_name.split('/');

		Info* item = root_;

		for ( const QString& s : p )
		{
			Info*             child = nullptr;
			const std::size_t nk    = item->children.size();

			for ( std::size_t k = 0; k < nk; ++k )
			{
				Info* t = item->children[k];

				if ( t->name == s )
				{
					child = t;
					break;
				}
			}

			if ( !child )
			{
				// insert in sorted order
				std::size_t k = 0;

				while ( k < nk && item->children[k]->name < s )
				{
					++k;
				}

				child = new Info{ item, s, std::size_t(-1), {} };
				item->children.insert(item->children.begin() + k, child);
			}

			item = child;
		}

		item->idx = i;
	}

	Info* old = root;
	beginResetModel();
	diff = std::move(diff_);
	root = root_;
	endResetModel();

	destroy(old);
}

void DiffModel::destroy(Info* info)
{
	if ( info )
	{
		for ( auto i : info->children )
		{
			destroy(i);
		}

		delete info;
	}
}

Diff DiffModel::getDiff(const QModelIndex& idx)
{
	Diff diff_;

	visit(diff_, to_info(idx) );
	return diff_;
}

void DiffModel::visit(
	Diff&       diff_,
	const Info* item
)
{
	if ( item )
	{
		if ( item->idx != std::size_t(-1) )
		{
			diff_.deltas.push_back(diff.deltas[item->idx]);
		}

		for ( auto i : item->children )
		{
			visit(diff_, i);
		}
	}
}

QStringList DiffModel::files(const QModelIndexList& indices) const
{
	QStringList l;

	for ( const QModelIndex& idx : indices )
	{
		const Info* info = to_info(idx);

		if ( info->idx != std::size_t(-1) )
		{
			l << diff.deltas[info->idx].old_name;
		}
	}

	l.removeDuplicates();
	return l;
}
