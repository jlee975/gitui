#ifndef LOGMODEL_H
#define LOGMODEL_H

#include <memory>
#include <QDateTime>

#include "models/graphmodel.h"

#include "qutility/patch.h"
#include "qrepotypes.h"

class Repository;

class LogModel
	: public GraphModel
{
public:
	enum {GRAPH, SUMMARY, DATE, HASH, COMMITTER, COMMITTER_EMAIL, AUTHOR, AUTHOR_EMAIL, NUMBER_OF_COLUMNS};
	explicit LogModel(QObject* = nullptr);

	void setRepository(std::shared_ptr< Repository >);
	void setRepository(std::shared_ptr< Repository >, reduction);
	void setHighlight(int, QBrush);

	QVariant data(const QModelIndex&, int = Qt::DisplayRole) const override;
	Qt::ItemFlags flags(const QModelIndex&) const override;
	QVariant headerData(int, Qt::Orientation, int  = Qt::DisplayRole) const override;
	QModelIndex index(int, int, const QModelIndex& = QModelIndex() ) const override;
	QModelIndex parent(const QModelIndex&) const override;
	int rowCount(const QModelIndex&    = QModelIndex() ) const override;
	int columnCount(const QModelIndex& = QModelIndex() ) const override;

	// Get information
	commit_id get_commit(const QModelIndex&) const;
	commit_id row_to_commit(int) const;
	std::vector< hash_type > get_diff_parents(const QModelIndex&) const;
	Diff get_diff(const QModelIndex&) const;
	Diff get_diff(const QModelIndex&, const QModelIndex&) const;
	Diff get_diff(int, int) const;
	Diff get_diff_with_index(commit_id) const;
	Diff get_diff_with_index(int) const;
	Diff get_diff_with_workdir(commit_id) const;
	Diff get_diff_with_workdir(int) const;
	hash_type get_hash(const QModelIndex&) const;
	int find_hash(const hash_type&) const;
	int find_commit(commit_id) const;

	// Actions
	void settings_updated();
private:
	void commit_changed(commit_id, int);
	bool is_graph_column_(int) const override;

	std::shared_ptr< Repository > repo_;
	reduction                     reductionx;

	// for each item i, a list of "colours" leaving this row
	std::map< int, QBrush > highlights;

	std::size_t hashlen;
};

#endif // LOGMODEL_H
