#ifndef STASHESMODEL_H
#define STASHESMODEL_H

#include <memory>

#include <QAbstractItemModel>

#include "repotypes.h"

class Repository;

class StashesModel
	: public QAbstractItemModel
{
public:
	enum {STASH, NUMBER_OF_COLUMNS};

	StashesModel();

	void setRepository(std::shared_ptr< Repository >);

	QVariant data(const QModelIndex&, int = Qt::DisplayRole) const override;
	Qt::ItemFlags flags(const QModelIndex&) const override;
	QVariant headerData(int, Qt::Orientation, int  = Qt::DisplayRole) const override;
	QModelIndex index(int, int, const QModelIndex& = QModelIndex() ) const override;
	QModelIndex parent(const QModelIndex&) const override;
	int rowCount(const QModelIndex&    = QModelIndex() ) const override;
	int columnCount(const QModelIndex& = QModelIndex() ) const override;

	stash_id get_stash(const QModelIndex&);
private:
	void refresh();
	void rebuild();

	std::shared_ptr< Repository > repo_;
	std::vector< stash_id >       stashes;
};

#endif // STASHESMODEL_H
