#include "remotesmodel.h"

#include "repository.h"

#include "settings.h"

RemotesModel::RemotesModel()
	: repo_(), hashlen(global_settings().hashDisplayLength() )
{
}

void RemotesModel::settings_updated()
{
	beginResetModel();
	hashlen = global_settings().hashDisplayLength();
	endResetModel();
}

void RemotesModel::setRepository(std::shared_ptr< Repository > r)
{
	beginResetModel();
	repo_ = r;

	rebuild();

	endResetModel();
}

void RemotesModel::rebuild()
{
	remotes.clear();

	if ( repo_ )
	{
		for ( const auto i : repo_->remote_ids() )
		{
			const auto&                r  = repo_->remote(i);
			remote_info                ri = { i, QString::fromStdString(r.name), QString::fromStdString(r.url) };
			std::vector< branch_info > bs;

			for ( std::size_t j = 0, n = r.branches.size(); j < n; ++j )
			{
				branch_info bi = { j,
					               QString::fromStdString(r.branches[j].name),
					               QString::fromStdString(r.branches[j].hash.to_string() )
				};
				bs.push_back(std::move(bi) );
			}

			std::sort(bs.begin(), bs.end(), sort_branches_by_name);
			ri.branches = std::move(bs);

			remotes.push_back(std::move(ri) );
		}

		std::sort(remotes.begin(), remotes.end(), sort_remotes_by_name);
	}
}

QVariant RemotesModel::data(
	const QModelIndex& idx,
	int                role
) const
{
	if ( repo_ && idx.isValid() && role == Qt::DisplayRole )
	{
		const int r = idx.row();
		const int c = idx.column();

		const quintptr x = idx.internalId();

		switch ( c )
		{
		case REMOTE:

			if ( x == quintptr(-1) )
			{
				return remotes[r].name;
			}
			else
			{
				return remotes[x].branches[r].name;
			}

			break;
		case URL:

			if ( x == quintptr(-1) )
			{
				return remotes[r].url;
			}
			else
			{
				return remotes[x].branches[r].hash.left(hashlen);
			}

			break;
		} // switch

	}

	return QVariant();
}

Qt::ItemFlags RemotesModel::flags(const QModelIndex& idx) const
{
	if ( idx.isValid() )
	{
		return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
	}

	return Qt::NoItemFlags;
}

QVariant RemotesModel::headerData(
	int             section,
	Qt::Orientation orientation,
	int             role
) const
{
	if ( orientation == Qt::Horizontal && role == Qt::DisplayRole )
	{
		switch ( section )
		{
		case REMOTE:
			return "Remote";

		case URL:
			return "URL";

		default:
			break;
		} // switch

	}

	return QVariant();
}

QModelIndex RemotesModel::index(
	int                row,
	int                col,
	const QModelIndex& idx
) const
{
	if ( repo_ && row >= 0 && col >= 0 && col < NUMBER_OF_COLUMNS )
	{
		if ( !idx.isValid() )
		{
			return createIndex(row, col, quintptr(-1) );
		}
		else
		{
			return createIndex(row, col, idx.row() );
		}
	}

	return QModelIndex();
}

QModelIndex RemotesModel::parent(const QModelIndex& idx) const
{
	if ( idx.isValid() )
	{
		const quintptr x = idx.internalId();

		if ( x != quintptr(-1) )
		{
			return createIndex(x, 0, quintptr(-1) );
		}
	}

	return QModelIndex();
}

int RemotesModel::rowCount(const QModelIndex& idx) const
{
	if ( idx.isValid() )
	{
		if ( idx.internalId() == quintptr(-1) )
		{
			return remotes[idx.row()].branches.size();
		}

		return 0;
	}

	return remotes.size();
}

int RemotesModel::columnCount(const QModelIndex&) const
{
	return NUMBER_OF_COLUMNS;
}

remote_id RemotesModel::get_remote(const QModelIndex& idx) const
{
	if ( repo_ && idx.isValid() )
	{
		const int      r = idx.row();
		const quintptr x = idx.internalId();

		if ( x == quintptr(-1) )
		{
			return remotes[r].id;
		}
		else
		{
			return remotes[x].id;
		}
	}

	return INVALID_REMOTE;
}

commit_id RemotesModel::get_commit(const QModelIndex& idx) const
{
	if ( repo_ && idx.isValid() )
	{
		const quintptr p = idx.internalId();

		if ( p != quintptr(-1) )
		{
			const int r = idx.row();
			return repo_->remote_branch_to_commit(remotes[p].id, r);
		}
	}

	return INVALID_COMMIT;
}

bool RemotesModel::sort_remotes_by_name(
	const remote_info& l,
	const remote_info& r
)
{
	return l.name < r.name;
}

bool RemotesModel::sort_branches_by_name(
	const branch_info& l,
	const branch_info& r
)
{
	return l.name < r.name;
}
