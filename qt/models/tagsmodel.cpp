#include "tagsmodel.h"

#include "repository.h"

#include "settings.h"

TagsModel::TagsModel()
	: repo_(), hashlen(global_settings().hashDisplayLength() ),
	sortfn([](const tag_info& l, const tag_info& r) { return l.name < r.name; })
{
}

void TagsModel::settings_updated()
{
	beginResetModel();
	hashlen = global_settings().hashDisplayLength();
	endResetModel();
}

void TagsModel::setRepository(std::shared_ptr< Repository > r)
{
	beginResetModel();
	repo_ = r;
	rebuild();
	endResetModel();
}

void TagsModel::refresh()
{
	beginResetModel();
	rebuild();
	endResetModel();
}

void TagsModel::rebuild()
{
	tags.clear();

	if ( repo_ )
	{
		for ( const auto i : repo_->tag_ids() )
		{
			const auto& t = repo_->tag(i);

			tag_info ti = {
				i,
				QString::fromStdString(t.name),
				QString::fromStdString(t.hash.to_string() ),
				QString::fromStdString(t.user),
				QString::fromStdString(t.email),
				QString::fromStdString(t.message)
			};
			tags.push_back(std::move(ti) );
		}

		std::sort(tags.begin(), tags.end(), sortfn);
	}
}

QVariant TagsModel::data(
	const QModelIndex& idx,
	int                role
) const
{
	if ( repo_ && idx.isValid() && role == Qt::DisplayRole )
	{
		const int r = idx.row();
		const int c = idx.column();

		switch ( c )
		{
		case TAG:
			return tags[r].name;

		case COMMIT:
			return tags[r].hash.left(hashlen);

		case USER:
			return tags[r].user;

		case EMAIL:
			return tags[r].email;

		case MESSAGE:
			return tags[r].message;
		}
	}

	return QVariant();
}

Qt::ItemFlags TagsModel::flags(const QModelIndex& idx) const
{
	if ( idx.isValid() )
	{
		return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
	}

	return Qt::NoItemFlags;
}

QVariant TagsModel::headerData(
	int             section,
	Qt::Orientation orientation,
	int             role
) const
{
	if ( orientation == Qt::Horizontal && role == Qt::DisplayRole )
	{
		switch ( section )
		{
		case TAG:
			return "Tag";

		case COMMIT:
			return "Commit";

		case USER:
			return "User";

		case EMAIL:
			return "E-mail";

		case MESSAGE:
			return "Message";

		default:
			break;
		} // switch

	}

	return QVariant();
}

QModelIndex TagsModel::index(
	int                row,
	int                col,
	const QModelIndex& idx
) const
{
	if ( repo_ && row >= 0 && col >= 0 && col < NUMBER_OF_COLUMNS )
	{
		if ( !idx.isValid() )
		{
			// Top level item
			if ( static_cast< unsigned >( row ) < tags.size() )
			{
				return createIndex(row, col);
			}
		}
	}

	return QModelIndex();
}

QModelIndex TagsModel::parent(const QModelIndex&) const
{
	return QModelIndex();
}

int TagsModel::rowCount(const QModelIndex& idx) const
{
	if ( idx.isValid() )
	{
		return 0;
	}

	return tags.size();
}

int TagsModel::columnCount(const QModelIndex&) const
{
	return NUMBER_OF_COLUMNS;
}

tag_id TagsModel::get_tag(const QModelIndex& idx) const
{
	return idx.isValid() ? tags[idx.row()].id : INVALID_TAG;
}

void TagsModel::sort(
	int           c,
	Qt::SortOrder order
)
{
	switch ( c )
	{
	case TAG:

		if ( order == Qt::AscendingOrder )
		{
			sortfn = [](const tag_info& l, const tag_info& r) { return l.name < r.name; };
		}
		else
		{
			sortfn = [](const tag_info& l, const tag_info& r) { return !( l.name < r.name ); };
		}

		break;
	case COMMIT:

		if ( order == Qt::AscendingOrder )
		{
			sortfn = [](const tag_info& l, const tag_info& r) { return l.hash < r.hash; };
		}
		else
		{
			sortfn = [](const tag_info& l, const tag_info& r) { return !( l.hash < r.hash ); };
		}

		break;
	case USER:

		if ( order == Qt::AscendingOrder )
		{
			sortfn = [](const tag_info& l, const tag_info& r) { return l.user < r.user; };
		}
		else
		{
			sortfn = [](const tag_info& l, const tag_info& r) { return !( l.user < r.user ); };
		}

		break;
	case EMAIL:

		if ( order == Qt::AscendingOrder )
		{
			sortfn = [](const tag_info& l, const tag_info& r) { return l.email < r.email; };
		}
		else
		{
			sortfn = [](const tag_info& l, const tag_info& r) { return !( l.email < r.email ); };
		}

		break;
	case MESSAGE:

		if ( order == Qt::AscendingOrder )
		{
			sortfn = [](const tag_info& l, const tag_info& r) { return l.message < r.message; };
		}
		else
		{
			sortfn = [](const tag_info& l, const tag_info& r) { return !( l.message < r.message ); };
		}

		break;
	} // switch

	beginResetModel();
	std::sort(tags.begin(), tags.end(), sortfn);
	endResetModel();
}
