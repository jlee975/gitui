#ifndef BRANCHESMODEL_H
#define BRANCHESMODEL_H

#include <memory>

#include <QAbstractItemModel>

#include "qrepotypes.h"

class Repository;

class BranchesModel
	: public QAbstractItemModel
{
public:
	enum {BRANCH, COMMIT, NUMBER_OF_COLUMNS};

	BranchesModel();

	void setRepository(std::shared_ptr< Repository >);
	void refresh();

	QVariant data(const QModelIndex&, int = Qt::DisplayRole) const override;
	Qt::ItemFlags flags(const QModelIndex&) const override;
	QVariant headerData(int, Qt::Orientation, int  = Qt::DisplayRole) const override;
	QModelIndex index(int, int, const QModelIndex& = QModelIndex() ) const override;
	QModelIndex parent(const QModelIndex&) const override;
	int rowCount(const QModelIndex&    = QModelIndex() ) const override;
	int columnCount(const QModelIndex& = QModelIndex() ) const override;
	void sort(int, Qt::SortOrder       = Qt::AscendingOrder) override;

	branch_id get_branch(const QModelIndex&) const;

	void settings_updated();
private:
	struct branch_info
	{
		branch_id id;
		QString name;
		QString hash;
	};

	void rebuild();

	std::shared_ptr< Repository >                                 repo_;
	std::vector< branch_info >                                    branches;
	std::size_t                                                   hashlen;
	std::function< bool(const branch_info&, const branch_info&) > sortfn;
};

#endif // BRANCHESMODEL_H
