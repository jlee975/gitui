#include "statusmodel.h"

#include "repository.h"

StatusModel::StatusModel()
	: repo_(), show_ignored_files(false)
{

}

void StatusModel::set_show_ignored_files(bool x)
{
	show_ignored_files = x;

	setRepository(repo_);
}

bool StatusModel::is_in_index(const QModelIndexList& l) const
{
	if ( repo_ )
	{
		for ( const QModelIndex& idx : l )
		{
			if ( idx.isValid() )
			{
				const int r_ = idx.row();

				const unsigned x = repo_->status(filter[r_]).status;

				if ( (x& status_type::INDEX_ANY) != 0 )
				{
					return true;
				}
			}
		}
	}

	return false;
}

bool StatusModel::not_in_index(const QModelIndexList& l) const
{
	if ( repo_ )
	{
		for ( const QModelIndex& idx : l )
		{
			if ( idx.isValid() )
			{
				const int r_ = idx.row();

				const unsigned x = repo_->status(filter[r_]).status;

				if ( (x& status_type::INDEX_ANY) == 0 )
				{
					return true;
				}
			}
		}
	}

	return false;
}

void StatusModel::setRepository(std::shared_ptr< Repository > r)
{
	beginResetModel();
	repo_ = r;
	rebuild();
	endResetModel();
}

void StatusModel::refresh()
{
	beginResetModel();
	rebuild();
	endResetModel();
}

void StatusModel::rebuild()
{
	filter.clear();

	if ( repo_ )
	{
		for ( const status_id i : repo_->status_ids() )
		{
			if ( show_ignored_files || ( repo_->status(i).status & status_type::IGNORED ) == 0 )
			{
				filter.push_back(i);
			}
		}
	}
}

QVariant StatusModel::data(
	const QModelIndex& idx,
	int                role
) const
{
	if ( repo_ && idx.isValid() && role == Qt::DisplayRole )
	{
		const int r_ = idx.row();
		const int c  = idx.column();

		switch ( c )
		{
		case PATH:
			return QString::fromStdString(repo_->status(filter[r_]).path);

		case INDEX:
		{
			const unsigned x = repo_->status(filter[r_]).status;

			QStringList l;

			if ( x & status_type::INDEX_NEW )
			{
				l << "New";
			}

			if ( x & status_type::INDEX_MODIFIED )
			{
				l << "Modified";
			}

			if ( x & status_type::INDEX_DELETED )
			{
				l << "Deleted";
			}

			if ( x & status_type::INDEX_RENAMED )
			{
				l << "Renamed";
			}

			if ( x & status_type::INDEX_TYPECHANGE )
			{
				l << "Typechange";
			}

			return l.join(',');
		}
		case WORKING_DIR:
		{
			const unsigned x = repo_->status(filter[r_]).status;

			QStringList l;

			if ( x & status_type::CURRENT )
			{
				l << "Current";
			}

			if ( x & status_type::WT_NEW )
			{
				l << "New";
			}

			if ( x & status_type::WT_MODIFIED )
			{
				l << "Modified";
			}

			if ( x & status_type::WT_DELETED )
			{
				l << "Deleted";
			}

			if ( x & status_type::WT_TYPECHANGE )
			{
				l << "Typechange";
			}

			if ( x & status_type::WT_RENAMED )
			{
				l << "Renamed";
			}

			if ( x & status_type::WT_UNREADABLE )
			{
				l << "Unreadable";
			}

			if ( x & status_type::IGNORED )
			{
				l << "Ignored";
			}

			if ( x & status_type::CONFLICTED )
			{
				l << "Conflicted";
			}

			return l.join(',');
		}

		} // switch

	}

	return QVariant();
}

Qt::ItemFlags StatusModel::flags(const QModelIndex& idx) const
{
	if ( idx.isValid() )
	{
		return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
	}

	return Qt::NoItemFlags;
}

QVariant StatusModel::headerData(
	int             section,
	Qt::Orientation orientation,
	int             role
) const
{
	if ( orientation == Qt::Horizontal && role == Qt::DisplayRole )
	{
		switch ( section )
		{
		case PATH:
			return "Path";

		case INDEX:
			return "Index";

		case WORKING_DIR:
			return "Working Dir";

		default:
			break;
		} // switch

	}

	return QVariant();
}

QModelIndex StatusModel::index(
	int                row,
	int                col,
	const QModelIndex& idx
) const
{
	if ( repo_ && row >= 0 && col >= 0 && col < NUMBER_OF_COLUMNS )
	{
		if ( !idx.isValid() )
		{
			// Top level item
			if ( static_cast< unsigned >( row ) < filter.size() )
			{
				return createIndex(row, col);
			}
		}
	}

	return QModelIndex();
}

QModelIndex StatusModel::parent(const QModelIndex&) const
{
	return QModelIndex();
}

int StatusModel::rowCount(const QModelIndex& idx) const
{
	if ( idx.isValid() )
	{
		return 0;
	}

	return filter.size();
}

int StatusModel::columnCount(const QModelIndex&) const
{
	return NUMBER_OF_COLUMNS;
}

bool StatusModel::add_to_index(const QModelIndexList& l)
{
	for ( const QModelIndex& idx : l )
	{
		if ( idx.isValid() )
		{
			const int r = idx.row();

			if ( repo_->add_to_index(repo_->status(filter[r]).path) )
			{
				emit dataChanged(idx.sibling(r, 0), idx.sibling(r, NUMBER_OF_COLUMNS - 1) );
			}
			else
			{
				return false;
			}
		}
	}

	return true;
}

status_id StatusModel::id(const QModelIndex& idx) const
{
	if ( idx.isValid() )
	{
		const int r = idx.row();

		if ( r >= 0 && static_cast< unsigned >( r ) < filter.size() )
		{
			return filter[static_cast< unsigned >( r )];
		}
	}

	return INVALID_STATUS;
}
