#include "branchesmodel.h"

#include <QFont>

#include "repository.h"

#include "settings.h"

BranchesModel::BranchesModel()
	: repo_(), hashlen(global_settings().hashDisplayLength() ), sortfn([](const branch_info& l, const branch_info& r) {
	return l.name < r.name;
})
{
}

void BranchesModel::settings_updated()
{
	hashlen = global_settings().hashDisplayLength();

	if ( !branches.empty() )
	{
		emit dataChanged(index(0, COMMIT), index(branches.size() - 1, COMMIT) );
	}
}

void BranchesModel::setRepository(std::shared_ptr< Repository > r)
{
	beginResetModel();
	repo_ = r;

	rebuild();

	endResetModel();
}

void BranchesModel::refresh()
{
	beginResetModel();

	rebuild();

	endResetModel();
}

void BranchesModel::rebuild()
{
	branches.clear();

	if ( repo_ )
	{
		for ( const auto i : repo_->branch_ids() )
		{
			const auto& b  = repo_->branch(i);
			branch_info bi = {
				i,
				QString::fromStdString(b.name),
				QString::fromStdString(b.hash.to_string() )
			};
			branches.push_back(std::move(bi) );
		}

		std::sort(branches.begin(), branches.end(), sortfn);
	}
}

QVariant BranchesModel::data(
	const QModelIndex& idx,
	int                role
) const
{
	if ( repo_ && idx.isValid() )
	{
		const int r = idx.row();
		const int c = idx.column();

		if ( role == Qt::DisplayRole )
		{
			switch ( c )
			{
			case BRANCH:
				return branches[r].name;

			case COMMIT:
				return branches[r].hash.left(hashlen);
			}
		}
		else if ( role == Qt::FontRole )
		{
			if ( branches[r].hash.toStdString() == repo_->head().to_string() )
			{
				QFont font;
				font.setBold(true);
				return font;
			}
		}
	}

	return QVariant();
}

Qt::ItemFlags BranchesModel::flags(const QModelIndex& idx) const
{
	if ( idx.isValid() )
	{
		return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
	}

	return Qt::NoItemFlags;
}

QVariant BranchesModel::headerData(
	int             section,
	Qt::Orientation orientation,
	int             role
) const
{
	if ( orientation == Qt::Horizontal && role == Qt::DisplayRole )
	{
		switch ( section )
		{
		case BRANCH:
			return "Local Branches";

		case COMMIT:
			return "Commit";

		default:
			break;
		} // switch

	}

	return QVariant();
}

QModelIndex BranchesModel::index(
	int                row,
	int                col,
	const QModelIndex& idx
) const
{
	if ( repo_ && row >= 0 && col >= 0 && col < NUMBER_OF_COLUMNS )
	{
		if ( !idx.isValid() )
		{
			// Top level item
			if ( static_cast< unsigned >( row ) < branches.size() )
			{
				return createIndex(row, col);
			}
		}
	}

	return QModelIndex();
}

QModelIndex BranchesModel::parent(const QModelIndex&) const
{
	return QModelIndex();
}

int BranchesModel::rowCount(const QModelIndex& idx) const
{
	if ( idx.isValid() )
	{
		return 0;
	}

	return repo_ ? branches.size() : 0;
}

int BranchesModel::columnCount(const QModelIndex&) const
{
	return NUMBER_OF_COLUMNS;
}

branch_id BranchesModel::get_branch(const QModelIndex& idx) const
{
	return idx.isValid() ? branches[idx.row()].id : INVALID_BRANCH;
}

void BranchesModel::sort(
	int           c,
	Qt::SortOrder order
)
{
	switch ( c )
	{
	case BRANCH:

		if ( order == Qt::AscendingOrder )
		{
			sortfn = [](const branch_info& l, const branch_info& r) { return l.name < r.name; };
		}
		else
		{
			sortfn = [](const branch_info& l, const branch_info& r) { return !( l.name < r.name ); };
		}

		break;
	case COMMIT:

		if ( order == Qt::AscendingOrder )
		{
			sortfn = [](const branch_info& l, const branch_info& r) { return l.hash < r.hash; };
		}
		else
		{
			sortfn = [](const branch_info& l, const branch_info& r) { return !( l.hash < r.hash ); };
		}

		break;
		break;
	} // switch

	beginResetModel();
	std::sort(branches.begin(), branches.end(), sortfn);
	endResetModel();
}
