#ifndef TAGSMODEL_H
#define TAGSMODEL_H

#include <memory>

#include <QAbstractItemModel>

#include "repotypes.h"

class Repository;

class TagsModel
	: public QAbstractItemModel
{
public:
	enum {TAG, COMMIT, USER, EMAIL, MESSAGE, NUMBER_OF_COLUMNS};

	TagsModel();

	void setRepository(std::shared_ptr< Repository >);
	void refresh();

	QVariant data(const QModelIndex&, int = Qt::DisplayRole) const override;
	Qt::ItemFlags flags(const QModelIndex&) const override;
	QVariant headerData(int, Qt::Orientation, int  = Qt::DisplayRole) const override;
	QModelIndex index(int, int, const QModelIndex& = QModelIndex() ) const override;
	QModelIndex parent(const QModelIndex&) const override;
	int rowCount(const QModelIndex&    = QModelIndex() ) const override;
	int columnCount(const QModelIndex& = QModelIndex() ) const override;
	void sort(int, Qt::SortOrder       = Qt::AscendingOrder) override;

	tag_id get_tag(const QModelIndex&) const;

	void settings_updated();
private:
	struct tag_info
	{
		tag_id id;
		QString name;
		QString hash;
		QString user;
		QString email;
		QString message;
	};

	void rebuild();

	std::shared_ptr< Repository >                           repo_;
	std::vector< tag_info >                                 tags;
	std::size_t                                             hashlen;
	std::function< bool(const tag_info&, const tag_info&) > sortfn;
};

#endif // TAGSMODEL_H
