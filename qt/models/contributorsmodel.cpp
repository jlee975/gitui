#include "contributorsmodel.h"

#include <QDateTime>

#include "repository.h"

ContributorsModel::ContributorsModel(
	const std::shared_ptr< Repository >& repo_,
	std::size_t                          hashlen_,
	QObject*                             parent
)
	: QAbstractItemModel(parent), repo(repo_), hashlen(hashlen_)
{
}

QVariant ContributorsModel::headerData(
	int             section,
	Qt::Orientation orientation,
	int             role
) const
{
	if ( orientation == Qt::Horizontal && role == Qt::DisplayRole )
	{
		switch ( section )
		{
		case AUTHOR:
			return "Author";

		case EMAIL:
			return "E-mail";

		case NUM_AUTHORSHIPS:
			return "Number of Authorships";

		case NUM_COMMITS:
			return "Number of Commits";

		case FIRST_COMMIT:
			return "First Activity";

		case FIRST_HASH:
			return "First Hash";

		case LAST_COMMIT:
			return "Last Activity";

		case LAST_HASH:
			return "Last Hash";

		default:
			break;
		} // switch

	}

	return QVariant();
}

QModelIndex ContributorsModel::index(
	int                row,
	int                col,
	const QModelIndex& parent
) const
{
	if ( row >= 0 && col >= 0 && col < NUM_COLUMNS )
	{
		if ( !parent.isValid() )
		{
			if ( static_cast< unsigned >( row ) < repo->number_of_contributors() )
			{
				return createIndex(row, col);
			}
		}
	}

	return QModelIndex();
}

QModelIndex ContributorsModel::parent(const QModelIndex& index) const
{
	return QModelIndex();
}

int ContributorsModel::rowCount(const QModelIndex& parent) const
{
	if ( !parent.isValid() )
	{
		return repo->number_of_contributors();
	}

	return 0;
}

int ContributorsModel::columnCount(const QModelIndex& parent) const
{
	if ( !parent.isValid() )
	{
		return NUM_COLUMNS;
	}

	return 0;
}

QVariant ContributorsModel::data(
	const QModelIndex& index,
	int                role
) const
{
	if ( !index.isValid() )
	{
		return QVariant();
	}

	if ( role == Qt::DisplayRole )
	{
		const int row = index.row();
		const int col = index.column();

		const auto& c = repo->contributor(contributor_id(row) );

		switch ( col )
		{
		case AUTHOR:
			return QString::fromStdString(c.name);

		case EMAIL:
			return QString::fromStdString(c.email);

		case NUM_AUTHORSHIPS:
			return qulonglong(c.num_authorships);

		case NUM_COMMITS:
			return qulonglong(c.num_commits);

		case FIRST_COMMIT:
		{
			QDateTime d;
			d.setTime_t(repo->commit(c.first_activity).actual_time);
			return d;
		}
		case FIRST_HASH:
			return QString::fromStdString(repo->commit(c.first_activity).hashxxx.to_string(hashlen) );

		case LAST_COMMIT:
		{
			QDateTime d;
			d.setTime_t(repo->commit(c.last_activity).actual_time);
			return d;
		}
		case LAST_HASH:
			return QString::fromStdString(repo->commit(c.last_activity).hashxxx.to_string(hashlen) );
		} // switch

	}

	return QVariant();
}
