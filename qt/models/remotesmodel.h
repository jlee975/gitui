#ifndef REMOTESMODEL_H
#define REMOTESMODEL_H

#include <memory>

#include <QAbstractItemModel>

#include "repotypes.h"

class Repository;

class RemotesModel
	: public QAbstractItemModel
{
public:
	enum {REMOTE, URL, NUMBER_OF_COLUMNS};

	RemotesModel();

	void setRepository(std::shared_ptr< Repository >);

	QVariant data(const QModelIndex&, int = Qt::DisplayRole) const override;
	Qt::ItemFlags flags(const QModelIndex&) const override;
	QVariant headerData(int, Qt::Orientation, int  = Qt::DisplayRole) const override;
	QModelIndex index(int, int, const QModelIndex& = QModelIndex() ) const override;
	QModelIndex parent(const QModelIndex&) const override;
	int rowCount(const QModelIndex&    = QModelIndex() ) const override;
	int columnCount(const QModelIndex& = QModelIndex() ) const override;

	remote_id get_remote(const QModelIndex&) const;
	commit_id get_commit(const QModelIndex&) const;

	void settings_updated();
private:
	struct branch_info
	{
		std::size_t id;
		QString name;
		QString hash;
	};

	struct remote_info
	{
		remote_id id;
		QString name;
		QString url;

		std::vector< branch_info > branches;
	};

	static bool sort_remotes_by_name(const remote_info&, const remote_info&);
	static bool sort_branches_by_name(const branch_info&, const branch_info&);

	void rebuild();

	std::shared_ptr< Repository > repo_;
	std::vector< remote_info >    remotes;
	std::size_t                   hashlen;
};

#endif // REMOTESMODEL_H
