#include "stashesmodel.h"

#include "repository.h"

StashesModel::StashesModel()
	: repo_()
{
}

void StashesModel::setRepository(std::shared_ptr< Repository > r)
{
	beginResetModel();
	repo_ = r;

	rebuild();

	endResetModel();
}

void StashesModel::refresh()
{
	beginResetModel();

	rebuild();

	endResetModel();
}

void StashesModel::rebuild()
{
	if ( repo_ )
	{
		stashes = repo_->stash_ids();
	}
	else
	{
		stashes.clear();
	}
}

QVariant StashesModel::data(
	const QModelIndex& idx,
	int                role
) const
{
	if ( repo_ && idx.isValid() && role == Qt::DisplayRole )
	{
		const int r = idx.row();
		const int c = idx.column();

		switch ( c )
		{
		case STASH:
			return QString::fromStdString(repo_->stash(stashes[r]).message);
		}
	}

	return QVariant();
}

Qt::ItemFlags StashesModel::flags(const QModelIndex& idx) const
{
	if ( idx.isValid() )
	{
		return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
	}

	return Qt::NoItemFlags;
}

QVariant StashesModel::headerData(
	int             section,
	Qt::Orientation orientation,
	int             role
) const
{
	if ( orientation == Qt::Horizontal && role == Qt::DisplayRole )
	{
		switch ( section )
		{
		case STASH:
			return "Stash";

		default:
			break;
		} // switch

	}

	return QVariant();
}

QModelIndex StashesModel::index(
	int                row,
	int                col,
	const QModelIndex& idx
) const
{
	if ( repo_ && row >= 0 && col >= 0 && col < NUMBER_OF_COLUMNS )
	{
		if ( !idx.isValid() )
		{
			// Top level item
			if ( static_cast< unsigned >( row ) < stashes.size() )
			{
				return createIndex(row, col);
			}
		}
	}

	return QModelIndex();
}

QModelIndex StashesModel::parent(const QModelIndex&) const
{
	return QModelIndex();
}

int StashesModel::rowCount(const QModelIndex& idx) const
{
	if ( idx.isValid() )
	{
		return 0;
	}

	return stashes.size();
}

int StashesModel::columnCount(const QModelIndex&) const
{
	return NUMBER_OF_COLUMNS;
}

stash_id StashesModel::get_stash(const QModelIndex& idx)
{
	if ( idx.isValid() )
	{
		return stashes[idx.row()];
	}

	return INVALID_STASH;
}
