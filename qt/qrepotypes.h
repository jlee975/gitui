#ifndef QREPOTYPES_H
#define QREPOTYPES_H

#include <QMetaType>

#include "repotypes.h"

Q_DECLARE_METATYPE(hash_type)
Q_DECLARE_METATYPE(branch_id)
Q_DECLARE_METATYPE(remote_id)
Q_DECLARE_METATYPE(commit_id)

#endif // QREPOTYPES_H
