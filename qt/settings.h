#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>

class GitUISettings
{
public:
	GitUISettings();

	QStringList recentDocs() const;
	void recentDocs(const QStringList& recentdocs);

	/// @todo QList< QColor >
	QVariantList defaultColourList() const;
	QVariantList getColourList() const;
	void setColourList(const QVariantList&);

	int hashDisplayLength() const;
	void hashDisplayLength(int);
private:
	QSettings settings;
};

GitUISettings& global_settings();

#endif // SETTINGS_H
