#-------------------------------------------------
#
# Project created by QtCreator 2018-07-14T20:17:48
#
#-------------------------------------------------
include(../vars.pri)

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qt
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    forms/reflogform.cpp \
        main.cpp \
        mainwindow.cpp \
    dialogs/addremotedialog.cpp \
    dialogs/clonedialog.cpp \
    dialogs/createtagdialog.cpp \
    dialogs/globalconfigurationdialog.cpp \
    dialogs/preferencesdialog.cpp \
    dialogs/rebasedialog.cpp \
    dialogs/repopropertiesdialog.cpp \
    dialogs/selectcommitdialog.cpp \
    forms/commitdialog.cpp \
    forms/commitviewer.cpp \
    forms/filehistory.cpp \
    forms/filesdiffform.cpp \
    forms/historyviewer.cpp \
    forms/logview.cpp \
    forms/overviewform.cpp \
    models/branchesmodel.cpp \
    models/diffmodel.cpp \
    models/logmodel.cpp \
    models/reflogmodel.cpp \
    models/remotesmodel.cpp \
    models/stashesmodel.cpp \
    models/statusmodel.cpp \
    models/tagsmodel.cpp \
    qutility/widgets/patchviewer.cpp \
    qutility/widgets/patchviewerinner.cpp \
    qutility/patch.cpp \
    settings.cpp \
    forms/blameform.cpp \
    models/contributorsmodel.cpp

HEADERS += \
    forms/reflogform.h \
        mainwindow.h \
    dialogs/addremotedialog.h \
    dialogs/clonedialog.h \
    dialogs/createtagdialog.h \
    dialogs/globalconfigurationdialog.h \
    dialogs/preferencesdialog.h \
    dialogs/rebasedialog.h \
    dialogs/repopropertiesdialog.h \
    dialogs/selectcommitdialog.h \
    forms/commitdialog.h \
    forms/commitviewer.h \
    forms/filehistory.h \
    forms/filesdiffform.h \
    forms/historyviewer.h \
    forms/logview.h \
    forms/overviewform.h \
    models/branchesmodel.h \
    models/diffmodel.h \
    models/logmodel.h \
    models/reflogmodel.h \
    models/remotesmodel.h \
    models/stashesmodel.h \
    models/statusmodel.h \
    models/tagsmodel.h \
    qutility/widgets/patchviewer.h \
    qutility/widgets/patchviewerinner.h \
    qutility/patch.h \
    settings.h \
    qrepotypes.h \
    forms/blameform.h \
    models/contributorsmodel.h

FORMS += \
    forms/reflogform.ui \
        mainwindow.ui \
    dialogs/addremotedialog.ui \
    dialogs/clonedialog.ui \
    dialogs/createtagdialog.ui \
    dialogs/globalconfigurationdialog.ui \
    dialogs/preferencesdialog.ui \
    dialogs/rebasedialog.ui \
    dialogs/repopropertiesdialog.ui \
    dialogs/selectcommitdialog.ui \
    forms/commitdialog.ui \
    forms/commitviewer.ui \
    forms/filehistory.ui \
    forms/filesdiffform.ui \
    forms/historyviewer.ui \
    forms/overviewform.ui \
    qutility/widgets/patchviewer.ui \
    forms/blameform.ui

unix|win32: LIBS += -lgit2

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../qtility/release/ -lqtility
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../qtility/debug/ -lqtility
else:unix: LIBS += -L$$OUT_PWD/../qtility/ -lqtility

INCLUDEPATH += $$PWD/../qtility
DEPENDPATH += $$PWD/../qtility

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../qtility/release/libqtility.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../qtility/debug/libqtility.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../qtility/release/qtility.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../qtility/debug/qtility.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../qtility/libqtility.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../repo/release/ -lrepo
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../repo/debug/ -lrepo
else:unix: LIBS += -L$$OUT_PWD/../repo/ -lrepo

INCLUDEPATH += $$PWD/../repo
DEPENDPATH += $$PWD/../repo

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../repo/release/librepo.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../repo/debug/librepo.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../repo/release/repo.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../repo/debug/repo.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../repo/librepo.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../jlee/release/ -ljlee
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../jlee/debug/ -ljlee
else:unix: LIBS += -L$$OUT_PWD/../jlee/ -ljlee

INCLUDEPATH += $$PWD/../jlee
DEPENDPATH += $$PWD/../jlee

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../jlee/release/libjlee.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../jlee/debug/libjlee.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../jlee/release/jlee.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../jlee/debug/jlee.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../jlee/libjlee.a

