QMAKE_CXXFLAGS = -std=c++17 -pipe -pedantic
QMAKE_CXXFLAGS_DEBUG = -O0 -ggdb3
QMAKE_CXXFLAGS_RELEASE = -O2
QMAKE_CXXFLAGS_WARN_OFF = warnoff

QMAKE_CXXFLAGS_WARN_ON = -Wall -Wextra -Wshadow -Wsign-compare -Wpointer-arith -Winit-self \
    -Wcast-qual -Wredundant-decls -Wcast-align -Wwrite-strings \
    -Woverloaded-virtual -Wformat -Wno-unknown-pragmas -Wnon-virtual-dtor \
    -Wold-style-cast -Wzero-as-null-pointer-constant -Wsuggest-override

QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-parameter -Wno-missing-field-initializers -Wno-unused-but-set-variable \
    -Wno-unused-variable -Wno-unused-function

QMAKE_CXXFLAGS_WARN_ON += -Werror -Wno-error=switch -Wno-error=suggest-attribute=const -Wno-error=suggest-final-types \
    -Wno-error=suggest-final-methods -Wno-error=implicit-fallthrough -Wno-error=type-limits

#QMAKE_CXXFLAGS_DEBUG += -fsanitize=address -fsanitize=pointer-compare -fsanitize=leak -fsanitize=undefined
#QMAKE_LFLAGS += -fsanitize=address -fsanitize=pointer-compare -fsanitize=leak -fsanitize=undefined
#DEFINES += _GLIBCXX_DEBUG _GLIBCXX_DEBUG_PEDANTIC

#profiling
#QMAKE_CXXFLAGS_DEBUG += --coverage
#QMAKE_LFLAGS += --coverage
