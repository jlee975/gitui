#ifndef INDEX_TYPE_H
#define INDEX_TYPE_H

#include <functional>

template< typename U, typename = void >
class index_type
{
public:
	index_type()
		: x()
	{
	}

	explicit index_type(U x_)
		: x(x_)
	{
	}

	friend bool operator==(
		const index_type& l,
		const index_type& r
	)
	{
		return l.x == r.x;
	}

	friend bool operator!=(
		const index_type& l,
		const index_type& r
	)
	{
		return l.x != r.x;
	}

	U get() const
	{
		return x;
	}

	index_type& operator++()
	{
		++x;
		return *this;
	}

	index_type operator++(int)
	{
		return index_type(x++);
	}

private:
	U x;
};

namespace std
{
template< typename U, typename X >
struct hash< index_type< U, X > >
{
	std::size_t operator()(const index_type< U, X >& i) const noexcept
	{
		hash< U > h;

		return h(i.get() );
	}

};
}

#endif // INDEX_TYPE_H
