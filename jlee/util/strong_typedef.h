#ifndef STRONG_TYPEDEF_H
#define STRONG_TYPEDEF_H

enum strong_typedef_options {op_arrow, op_dereference, op_eq, op_ne, op_equal_comparable, op_lt, op_gt, op_lte, op_gte,
	                         op_ordered, op_inc};

/** @todo Would be nice to auto-determine all the ops.
 * @todo Separate const and non-const specializations
 * @todo Handle an op that is inherited twice
 */
template< typename T, strong_typedef_options... >
class strong_typedef;

// Base case stores the value
template< typename T >
class strong_typedef< T >
{
public:
	strong_typedef() = default;
	explicit strong_typedef(const T& value_)
		: value(value_)
	{
	}

	const T& get() const
	{
		return value;
	}

protected: /// @todo Make private and friend strong_typedef< T, ... >
	T value;
};

template< typename T, strong_typedef_options... opts >
class strong_typedef< T, op_arrow, opts... >
	: public strong_typedef< T, opts... >
{
public:
	using strong_typedef< T, opts... >::strong_typedef;

	auto operator->()
	{
		return ( this->value ).operator->();
	}

	auto operator->() const
	{
		return ( this->value ).operator->();
	}

};

template< typename T, strong_typedef_options... opts >
class strong_typedef< T, op_dereference, opts... >
	: public strong_typedef< T, opts... >
{
public:
	using strong_typedef< T, opts... >::strong_typedef;

	auto operator*()
	{
		return *( this->value );
	}

	auto operator*() const
	{
		return *( this->value );
	}

};

template< typename T, strong_typedef_options... opts >
class strong_typedef< T, op_eq, opts... >
	: public strong_typedef< T, opts... >
{
public:
	using strong_typedef< T, opts... >::strong_typedef;

	friend bool operator==(
		const strong_typedef& l,
		const strong_typedef& r
	)
	{
		return l.value == r.value;
	}

};

template< typename T, strong_typedef_options... opts >
class strong_typedef< T, op_ne, opts... >
	: public strong_typedef< T, opts... >
{
public:
	using strong_typedef< T, opts... >::strong_typedef;

	friend bool operator!=(
		const strong_typedef& l,
		const strong_typedef& r
	)
	{
		return l.value != r.value;
	}

};

template< typename T, strong_typedef_options... opts >
class strong_typedef< T, op_equal_comparable, opts... >
	: public strong_typedef< T, op_eq, op_ne, opts... >
{
	using strong_typedef< T, op_eq, op_ne, opts... >::strong_typedef;
};

template< typename T, strong_typedef_options... opts >
class strong_typedef< T, op_lt, opts... >
	: public strong_typedef< T, opts... >
{
public:
	using strong_typedef< T, opts... >::strong_typedef;

	friend bool operator<(
		const strong_typedef& l,
		const strong_typedef& r
	)
	{
		return l.value < r.value;
	}

};

template< typename T, strong_typedef_options... opts >
class strong_typedef< T, op_gt, opts... >
	: public strong_typedef< T, opts... >
{
public:
	using strong_typedef< T, opts... >::strong_typedef;

	friend bool operator>(
		const strong_typedef& l,
		const strong_typedef& r
	)
	{
		return l.value > r.value;
	}

};

template< typename T, strong_typedef_options... opts >
class strong_typedef< T, op_lte, opts... >
	: public strong_typedef< T, opts... >
{
public:
	using strong_typedef< T, opts... >::strong_typedef;

	friend bool operator<=(
		const strong_typedef& l,
		const strong_typedef& r
	)
	{
		return l.value <= r.value;
	}

};

template< typename T, strong_typedef_options... opts >
class strong_typedef< T, op_gte, opts... >
	: public strong_typedef< T, opts... >
{
public:
	using strong_typedef< T, opts... >::strong_typedef;

	friend bool operator>=(
		const strong_typedef& l,
		const strong_typedef& r
	)
	{
		return l.value >= r.value;
	}

};

/// @todo should also expand to op_lt, op_le, op_gt, op_ge
template< typename T, strong_typedef_options... opts >
class strong_typedef< T, op_ordered, opts... >
	: public strong_typedef< T, op_eq, op_ne, op_lt, op_gt, op_lte, op_gte, opts... >
{
public:
	using strong_typedef< T, op_eq, op_ne, op_lt, op_gt, op_lte, op_gte, opts... >::strong_typedef;
};

template< typename T, strong_typedef_options... opts >
class strong_typedef< T, op_inc, opts... >
	: public strong_typedef< T, opts... >
{
public:
	using strong_typedef< T, opts... >::strong_typedef;

	auto operator++()
	{
		return ( this->value ).operator++();
	}

};

#endif // STRONG_TYPEDEF_H
