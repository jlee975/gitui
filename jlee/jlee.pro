#-------------------------------------------------
#
# Project created by QtCreator 2019-01-07T09:37:40
#
#-------------------------------------------------
include(../vars.pri)

CONFIG -= qt c++11 c++14

TARGET = jlee
TEMPLATE = lib
CONFIG += staticlib

SOURCES +=

HEADERS += \
    containers/iterator_wrapper.h \
    idmap/idmap.h \
    util/index_type.h \
    util/strong_typedef.h \
    graph/dag.h \
    graph/directed.h \
    bits/hex.h \
    hash/hash.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
