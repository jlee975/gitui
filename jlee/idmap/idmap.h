#ifndef IDMAP_H
#define IDMAP_H

#include <unordered_map>

#include "containers/iterator_wrapper.h"

template< typename T, typename K = std::size_t >
class idmap
{
public:
	typedef iterator_wrapper< typename std::unordered_map< K, T >::iterator, idmap > iterator;
	typedef iterator_wrapper< typename std::unordered_map< K, T >::const_iterator, idmap > const_iterator;
	typedef K id_type;

	idmap()
		: next()
	{
	}

	template< typename Iterator >
	idmap(
		Iterator first,
		Iterator last
	)
		: next(), data(first, last)
	{
		for ( const auto& x : data )
		{
			if ( x.first >= next )
			{
				next = x.first + 1;
			}
		}
	}

	id_type insert(const T& value)
	{
		data.emplace(next, value);
		return next++;
	}

	id_type insert(T&& value)
	{
		data.emplace(next, std::move(value) );
		return next++;
	}

	template< typename... Args >
	id_type emplace(Args&& ... args)
	{
		data.emplace(next, T{ std::forward< Args >(args) ... });
		return next++;
	}

	T& operator[](id_type x)
	{
		return data.at(x);
	}

	const T& operator[](id_type x) const
	{
		return data.at(x);
	}

	std::size_t size() const
	{
		return data.size();
	}

	void clear()
	{
		return data.clear();
	}

	iterator begin()
	{
		return iterator(data.begin() );
	}

	const_iterator begin() const
	{
		return const_iterator(data.begin() );
	}

	iterator end()
	{
		return iterator(data.end() );
	}

	const_iterator end() const
	{
		return const_iterator(data.end() );
	}

	bool has(K k) const
	{
		return data.count(k) != 0;
	}

	iterator find(K k)
	{
		return iterator(data.find(k) );
	}

	const_iterator find(K k) const
	{
		return const_iterator(data.find(k) );
	}

	void erase(K k)
	{
		data.erase(k);
	}

	void erase(iterator it)
	{
		data.erase(it.get() );
	}

	void swap(idmap& o)
	{
		K t = next;

		next   = o.next;
		o.next = t;
		data.swap(o.data);
	}

private:
	K                          next;
	std::unordered_map< K, T > data;
};

#endif // IDMAP_H
