#ifndef DIRECTED_H
#define DIRECTED_H

#include <vector>
#include <unordered_map>
#include <algorithm>
#include "idmap/idmap.h"

/// @todo Node -> Vertices
template< typename Node >
class directed_graph
{
public:
	using value_type          = Node;
	using node_id             = typename idmap< value_type >::id_type;
	using node_iterator       = typename idmap< value_type >::iterator;
	using node_const_iterator = typename idmap< value_type >::const_iterator;

	template< typename... Args >
	node_id emplace(Args&& ... args)
	{
		return nodes.emplace(std::forward< Args >(args)...);
	}

	const value_type& operator[](node_id i) const
	{
		return nodes[i];
	}

	value_type& operator[](node_id i)
	{
		return nodes[i];
	}

	node_iterator find(node_id i)
	{
		return nodes.find(i);
	}

	node_const_iterator find(node_id i) const
	{
		return nodes.find(i);
	}

	node_iterator begin()
	{
		return nodes.begin();
	}

	node_const_iterator begin() const
	{
		return nodes.begin();
	}

	node_iterator end()
	{
		return nodes.end();
	}

	node_const_iterator end() const
	{
		return nodes.end();
	}

	const std::vector< node_id > parents(node_id i) const
	{
		const auto it = parents_.find(i);

		if ( it != parents_.end() )
		{
			return it->second;
		}

		static const std::vector< node_id > fallback;
		return fallback;
	}

	std::size_t order() const
	{
		return nodes.size();
	}

	void clear()
	{
		nodes.clear();
	}

	/** DERP
	 *
	 * A graph which has all the listed nodes, and then edges which reflect ancestor relationships.
	 * i.e., an edge exists in the resulting graph if u,v were pathwise connected
	 *
	 * @todo Appropriate return type. The return type is a graph with the same nodes, but different edges.
	 *   This is basically a derivative of graph< std::vector< node_id > >, i.e., the returned graph has
	 *   the edge list associated to each graph.
	 * @todo Rename. Seems to be some mixtures of induced subgraph and smoothed graph
	 * @todo Performance
	 */
	directed_graph< node_id > derp(const std::vector< node_id >& keep) const
	{
		directed_graph< node_id > xyzzy;

		std::unordered_map< node_id, typename directed_graph< node_id >::node_id > remap;

		for ( const auto i : keep )
		{
			remap[i] = xyzzy.emplace(i);
		}

		for ( const auto i : keep )
		{
			std::vector< typename directed_graph< node_id >::node_id > y;

			/// @todo This copy could be avoided
			std::vector< node_id > x = parents(i);

			/// @todo switched from vector< bool > to map< commit_id, bool > when commit_id was introduced
			std::unordered_map< node_id, bool > z;

			for ( const auto& p : nodes )
			{
				z[p.first] = false;
			}

			while ( !x.empty() )
			{
				const node_id p = x.back();
				x.pop_back();

				if ( !z[p] )
				{
					z[p] = true;

					bool has = false;

					for ( std::size_t j = 0; j < keep.size(); ++j )
					{
						if ( keep[j] == p )
						{
							has = true;
							break;
						}
					}

					if ( has )
					{
						y.push_back(remap[p]);
					}
					else
					{
						const auto& t = parents(p);
						x.insert(x.end(), t.begin(), t.end() );
					}
				}
			}

			xyzzy.connect(y, remap[i]);
		}

		return xyzzy;
	}

	/** Description
	 *
	 * Returns u, v, and ancestors of each up to first common ancestor(s).
	 *
	 * @todo Performance
	 * @todo We probably don't need to go over EVERY node.
	 */
	std::vector< node_id > common(
		node_id u,
		node_id v
	) const
	{
		std::vector< node_id > keep = { u, v };

		/// @todo sets would probably be better than maps
		std::unordered_map< node_id, bool > reachable_from;
		std::unordered_map< node_id, bool > reachable_to;

		{
			std::vector< node_id > queue = { u };

			for ( std::size_t i = 0; i < queue.size(); ++i )
			{
				if ( !reachable_from[queue[i]] )
				{
					reachable_from[queue[i]] = true;
					const auto& p = parents(queue[i]);
					queue.insert(queue.end(), p.begin(), p.end() );
				}
			}
		}

		{
			std::vector< node_id > queue = { v };

			for ( std::size_t i = 0; i < queue.size(); ++i )
			{
				if ( !reachable_to[queue[i]] )
				{
					reachable_to[queue[i]] = true;
					const auto& ps = parents(queue[i]);
					queue.insert(queue.end(), ps.begin(), ps.end() );
				}
			}
		}

		std::unordered_map< node_id, bool > reachable_common;

		for ( const auto&[i, c] : nodes )
		{
			if ( reachable_from[i] != reachable_to[i] )
			{
				for ( const auto j : parents(i) )
				{
					reachable_common[j] = true;
				}
			}
		}

		// for all reachable with child_count > 0 keep
		for ( const auto&[i, c] : nodes )
		{
			if ( i != u && i != v && reachable_common[i] && reachable_from[i] && reachable_to[i] )
			{
				keep.push_back(i);
			}
		}

		return keep;
	}

	void connect(
		node_id from,
		node_id to
	)
	{
		parents_[to].push_back(from);
	}

	void connect(
		const std::vector< node_id >& from,
		node_id                       to
	)
	{
		auto it = parents_.find(to);

		if ( it != parents_.end() )
		{
			it->second.insert(it->second.end(), from.begin(), from.end() );
		}
		else
		{
			parents_.emplace(to, from);
		}
	}

private:
	idmap< value_type >                                   nodes;
	std::unordered_map< node_id, std::vector< node_id > > parents_;
};

/** Get a topological order of nodes, optionally sorted with the given comparison function
 * @todo Rename
 * @todo Performance
 * @todo Should take a graph view instead of the unordered_map business
 */
template< typename Node, typename Compare >
std::vector< typename directed_graph< Node >::node_id > derp_topo_sort(
	const directed_graph< Node >& g,
	const Compare&                compare
)
{
	using node_id = typename directed_graph< Node >::node_id;

	const std::size_t n = g.order();

	// For each commit, number of children
	std::unordered_map< node_id, std::size_t > child_counts;

	for ( const auto& p : g )
	{
		for ( const node_id j : g.parents(p.first) )
		{
			++child_counts[j];
		}
	}

	// Start listing commits with no children...
	std::vector< node_id > ready;
	ready.reserve(n);

	for ( const auto& p : g )
	{
		const auto i = p.first;

		if ( child_counts[i] == 0 )
		{
			ready.push_back(i);
		}
	}

	// sort by date
	std::sort(ready.begin(), ready.end(), compare);

	/// @todo v0 seems redundant. Exact copy of ready
	std::vector< node_id > v0;
	v0.reserve(n);

	for ( std::size_t i = 0; i < ready.size(); ++i )
	{
		// Add childless nodes to end of list. Update list of childless nodes
		const node_id k = ready[i];
		v0.push_back(k);

		for ( const node_id j : g.parents(k) )
		{
			if ( --child_counts[j] == 0 )
			{
				// insert by date
				/// @todo Performance: Append all nodes and then sort, or merge into tail
				auto it = std::lower_bound(ready.begin() + ( i + 1 ), ready.end(), j, compare);
				ready.insert(it, j);
			}
		}
	}

	return v0;
}

#endif // DIRECTED_H
